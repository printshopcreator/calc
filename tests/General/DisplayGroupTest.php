<?php
namespace PSC\Library\Calc\Tests\General;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class DisplayGroupTest extends TestCase
{
    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/Complex/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);

        $this->engine->loadString(file_get_contents(__DIR__ .'/../TestFiles/SampleArticle/article1.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }
    
    public function testDisplayGroup()
    {
      
        $article = $this->engine->getArticle();

        $this->assertEquals(2, $article->getDisplayGroups()->count());

        $group = $article->getDisplayGroups()[0];

        $this->assertEquals("1", $group->getId());

        $this->assertEquals("2", $article->getOptionById("produktion")->getDisplayGroup());
    }

}
