<?php
namespace PSC\Library\Calc\Tests\General;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer\Container;

class LoadTest extends TestCase
{
    public function testSimpleLoadXmlFromString()
    {
        $parser = new Engine();
        $this->assertTrue($parser->loadString(file_get_contents(__DIR__ .'/../TestFiles/General/simple.xml')));
    }

    public function testWrongEval()
    {
        $parser = new Engine();
        $parser->loadString(file_get_contents(__DIR__ .'/../TestFiles/General/simple.xml'));
        $this->assertEquals(0, $parser->getPrice());
    }

}
