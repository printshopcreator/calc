<?php
namespace PSC\Library\Calc\Tests\Recursiv;



use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class FirstTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/Recursiv/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__.'/../TestFiles/Recursiv/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__.'/../TestFiles/Recursiv/parameters.txt'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/Recursiv/1.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfArticleCountIsCorrect()
    {
        $this->assertEquals(1, $this->engine->getArticles()->Count());
        $this->engine->getPrice();
    }

}
