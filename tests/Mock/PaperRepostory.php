<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace PSC\Library\Calc\Tests\Mock;

use Doctrine\Persistence\ObjectRepository;

class PaperRepostory implements ObjectRepository
{

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param mixed $id The identifier.
     *
     * @return object The object.
     */
    public function find($id)
    {
        // TODO: Implement find() method.
    }

    /**
     * Finds all objects in the repository.
     *
     * @return array The objects.
     */
    public function findAll()
    {
        // TODO: Implement findAll() method.
    }

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array The objects.
     *
     * @throws \UnexpectedValueException
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        // TODO: Implement findBy() method.
    }

    /**
     * Finds a single object by a set of criteria.
     *
     * @param array $criteria The criteria.
     *
     * @return object The object.
     */
    public function findOneBy(array $criteria)
    {
        $papier = array();
        $papier['bdm135'] = new Paper();
        $papier['bdm135']->setArtNr('bdm135');
        $papier['bdm135']->setVolume(0);
        $papier['bdm135']->setPreis(0.1);
        $papier['bdm135']->setDescription1('Bilderdruck matt 135 gr');

        $papier['bdg135'] = new Paper();
        $papier['bdg135']->setArtNr('bdg135');
        $papier['bdg135']->setDescription1('Bilderdruck glänzend 135 gr');

        $papier['bdm170'] = new Paper();
        $papier['bdm170']->setArtNr('bdm170');
        $papier['bdm170']->setDescription1('Bilderdruck matt 170 gr');

        $papier['bdg170'] = new Paper();
        $papier['bdg170']->setArtNr('bdg170');
        $papier['bdg170']->setDescription1('Bilderdruck glänzend 170 gr');

        $papier['bdm250'] = new Paper();
        $papier['bdm250']->setArtNr('bdm250');
        $papier['bdm250']->setDescription1('Bilderdruck matt 250 gr');

        $papier['bdg250'] = new Paper();
        $papier['bdg250']->setArtNr('bdg250');
        $papier['bdg250']->setDescription1('Bilderdruck glänzend 250 gr');
        $papier['bdg250']->setPreis(84);
        $papier['bdg250']->setGrammatur(250);
        $papier['bdg250']->setPapierTyp1(1);


        $papier['bdm300'] = new Paper();
        $papier['bdm300']->setArtNr('bdm300');
        $papier['bdm300']->setDescription1('Bilderdruck matt 300 gr');

        $papier['bdg300'] = new Paper();
        $papier['bdg300']->setArtNr('bdg300');
        $papier['bdg300']->setDescription1('Bilderdruck glänzend 300 gr');


        $papier['INM115'] = new Paper();
        $papier['INM115']->setArtNr('INM115');
        $papier['INM115']->setGrammatur('115');
        $papier['INM115']->setDigitalVar('495');
        $papier['INM115']->setDigitalFix('345');
        $papier['INM115']->setPreis(24);
        $papier['INM115']->setVolume(0.104);
        $papier['INM115']->setDescription1('115 g/m² Bilderdruck matt gestrichen');
        $papier['INM115']->setDescription2('115 g/m² Inapa Infinity silk, seidenmatt');

        $papier['INM135'] = new Paper();
        $papier['INM135']->setArtNr('INM135');
        $papier['INM135']->setGrammatur('135');
        $papier['INM135']->setOffsetVar('495');
        $papier['INM135']->setOffsetFix('345');
        $papier['INM135']->setDigitalVar('495');
        $papier['INM135']->setDigitalFix('345');
        $papier['INM135']->setPreis(28);
        $papier['INM135']->setVolume(0.118);
        $papier['INM135']->setDescription1('135 g/m² Bilderdruck matt gestrichen');
        $papier['INM135']->setDescription2('135 g/m² Inapa Infinity silk, seidenmatt');

        $papier['INM13522'] = new Paper();
        $papier['INM13522']->setArtNr('INM13522');
        $papier['INM13522']->setGrammatur('135');
        $papier['INM13522']->setOffsetVar('495');
        $papier['INM13522']->setOffsetFix('345');
        $papier['INM13522']->setDigitalVar('495');
        $papier['INM13522']->setDigitalFix('345');
        $papier['INM13522']->setPreis(50.5);
        $papier['INM13522']->setVolume(0.118);
        $papier['INM13522']->setDescription1('135 g/m² Bilderdruck matt gestrichen');
        $papier['INM13522']->setDescription2('135 g/m² Inapa Infinity silk, seidenmatt');

        $papier['INM135ND'] = new Paper();
        $papier['INM135ND']->setArtNr('INM135ND');
        $papier['INM135ND']->setGrammatur('135');
        $papier['INM135ND']->setDigitalVar('495');
        $papier['INM135ND']->setOffsetVar('495');
        $papier['INM135ND']->setDigitalFix('345');
        $papier['INM135ND']->setOffsetFix('345');
        $papier['INM135ND']->setPreis(50.5);
        $papier['INM135ND']->setVolume(0.118);
        $papier['INM135ND']->setDescription1('135 g/m² Bilderdruck matt gestrichen');
        $papier['INM135ND']->setDescription2('135 g/m² Bilderdruck matt gestrichen');

        $papier['GP100'] = new Paper();
        $papier['GP100']->setArtNr('GP100');
        $papier['GP100']->setGrammatur('100');
        $papier['GP100']->setDigitalVar('495');
        $papier['GP100']->setDigitalFix('345');
        $papier['GP100']->setPreis(74.5);
        $papier['GP100']->setVolume(0.126);
        $papier['GP100']->setDescription1('100 g/m² Creapaper GRASPAP® Graspapier');
        $papier['GP100']->setDescription2('100 g/m² CreaPaper Graspapier');

        $papier['GP150'] = new Paper();
        $papier['GP150']->setArtNr('GP150');
        $papier['GP150']->setGrammatur('150');
        $papier['GP150']->setDigitalVar('495');
        $papier['GP150']->setDigitalFix('345');
        $papier['GP150']->setPreis(102.5);
        $papier['GP150']->setVolume(0.18);
        $papier['GP150']->setDescription1('150 g/m² Creapaper GRASPAP® Graspapier');
        $papier['GP150']->setDescription2('150 g/m² CreaPaper Graspapier');

        $papier['yes-s-80-a6'] = new Paper();
        $papier['yes-s-80-a6']->setArtNr('yes-s-80-a6');
        $papier['yes-s-80-a6']->setPreis(0);
        $papier['yes-s-80-a6']->setDescription1('YES Silver DIN A6');
        $papier['yes-s-80-a6']->setDescription2('YES Silver DIN A6');

        $papier['INM250'] = new Paper();
        $papier['INM250']->setArtNr('INM170');
        $papier['INM250']->setGrammatur('170');
        $papier['INM250']->setDigitalVar('495');
        $papier['INM250']->setDigitalFix('345');
        $papier['INM250']->setPreis(63.6);
        $papier['INM250']->setVolume(0.156);
        $papier['INM250']->setDescription1('170 g/m² Bilderdruck matt gestrichen');
        $papier['INM250']->setDescription2('170 g/m² Bilderdruck matt gestrichen');

        $papier['INM300'] = new Paper();
        $papier['INM300']->setArtNr('INM300');
        $papier['INM300']->setGrammatur('300');
        $papier['INM300']->setDigitalVar('495');
        $papier['INM300']->setDigitalFix('345');
        $papier['INM300']->setPreis(58);
        $papier['INM300']->setVolume(0.303);
        $papier['INM300']->setDescription1('115 g/m² Bilderdruck matt gestrichen');
        $papier['INM300']->setDescription2('115 g/m² Inapa Infinity silk, seidenmatt');

        $papier['INM170'] = new Paper();
        $papier['INM170']->setArtNr('INM170');
        $papier['INM170']->setGrammatur('170');
        $papier['INM170']->setDigitalVar('495');
        $papier['INM170']->setDigitalFix('345');
        $papier['INM170']->setPreis(63.6);
        $papier['INM170']->setVolume(0.156);
        $papier['INM170']->setDescription1('170 g/m² Bilderdruck matt gestrichen');
        $papier['INM170']->setDescription2('Stand: 22.04.2022');

        $papier['INM200'] = new Paper();
        $papier['INM200']->setArtNr('INM200');
        $papier['INM200']->setGrammatur('200');
        $papier['INM200']->setDigitalVar('495');
        $papier['INM200']->setDigitalFix('345');
        $papier['INM200']->setPreis(76);
        $papier['INM200']->setVolume(0.188);
        $papier['INM200']->setDescription1('200 g/m² Bilderdruck matt gestrichen');
        $papier['INM200']->setDescription2('Stand: 22.04.2022');

        $papier['KEIN'] = new Paper();
        $papier['KEIN']->setArtNr('KEIN');

        $papier['INM300ND'] = new Paper();
        $papier['INM300ND']->setArtNr('INM300ND');
        $papier['INM300ND']->setGrammatur('300');
        $papier['INM300ND']->setDigitalVar('495');
        $papier['INM300ND']->setDigitalFix('345');
        $papier['INM300ND']->setPreis(115.7);
        $papier['INM300ND']->setVolume(0.303);
        $papier['INM300ND']->setDescription1('300 g/m² Bilderdruck matt gestrichen');
        $papier['INM300ND']->setDescription2('300 g/m² Bilderdruck matt gestrichen');

        $papier['INA90'] = new Paper();
        $papier['INA90']->setArtNr('INA90');
        $papier['INA90']->setArtNr('90');
        $papier['INA90']->setDigitalVar('495');
        $papier['INA90']->setDigitalFix('345');
        $papier['INA90']->setPreis(28.3);
        $papier['INA90']->setVolume(0.119);
        $papier['INA90']->setDescription1('	90 g/m² Offsetpapier hochweiß, natur');
        $papier['INA90']->setDescription2('90 g/m² Multi Business, 1,3-faches Volumen');

        $papier['INA400'] = new Paper();
        $papier['INA400']->setArtNr('INA400');
        $papier['INA400']->setGrammatur('400');
        $papier['INA400']->setDigitalVar('495');
        $papier['INA400']->setDigitalFix('345');
        $papier['INA400']->setPreis(154);
        $papier['INA400']->setVolume(0.548);
        $papier['INA400']->setDescription1('400 g/m² Offsetpapier hochweiß, natur');
        $papier['INA400']->setDescription2('400 g/m² Maestro extra, 1,3-faches Volumen');

        $papier['ZETL260'] = new Paper();
        $papier['ZETL260']->setArtNr('ZETL260');
        $papier['ZETL260']->setGrammatur('260');
        $papier['ZETL260']->setDigitalVar('495');
        $papier['ZETL260']->setDigitalFix('345');
        $papier['ZETL260']->setPreis(220);
        $papier['ZETL260']->setVolume(0.280);
        $papier['ZETL260']->setDescription1('260 g/m² Zanders ZETA leinen brilliantweiß');
        $papier['ZETL260']->setDescription2('260 g/m² Naturkarton leinengeprägt');

        $papier['CONG300_1'] = new Paper();
        $papier['CONG300_1']->setArtNr('CONG300_1');
        $papier['CONG300_1']->setGrammatur('300');
        $papier['CONG300_1']->setDescription1('300 g/m² Naturkarton gerippt hochweiß');
        $papier['CONG300_1']->setDescription2('300 g/m² Conqueror gerippt diamantweiß');

        $papier['grasnatur85'] = new Paper();
        $papier['grasnatur85']->setArtNr('grasnatur85');
        $papier['grasnatur85']->setGrammatur('85');
        $papier['grasnatur85']->setPreis('160');
        $papier['grasnatur85']->setVolume('1.2');
        $papier['grasnatur85']->setDescription1('Graspapier 50% Faseranteil (85 g/m²)');
        $papier['grasnatur85']->setDescription2('Graspapier 50% Faseranteil (85 g/m²)');

        $papier['grasnatur205'] = new Paper();
        $papier['grasnatur205']->setArtNr('grasnatur205');
        $papier['grasnatur205']->setGrammatur('205');
        $papier['grasnatur205']->setPreis('160');
        $papier['grasnatur205']->setVolume('1.2');
        $papier['grasnatur205']->setDescription1('Graspapier 50% Faseranteil (205 g/m²)');
        $papier['grasnatur205']->setDescription2('Graspapier 50% Faseranteil (205 g/m²)');

        $papier['sm150g'] = new Paper();
        $papier['sm150g']->setArtNr('sm150g');
        $papier['sm150g']->setGrammatur('150');
        $papier['sm150g']->setPreis('100');
        $papier['sm150g']->setVolume('1.2');
        $papier['sm150g']->setDescription1('Bilderdruck seidenmatt, aus zert. nachhaltiger Forstwirtschaft');
        $papier['sm150g']->setDescription2('Bilderdruck seidenmatt, aus zert. nachhaltiger Forstwirtschaft');
        $papier['sm150g']->setPapierTyp1(1);
        $papier['sm150g']->setPapierTyp2(1);
        $papier['sm150g']->setPapierTyp3(1);
        $papier['sm150g']->setPapierTyp11(1);
        $papier['sm150g']->setPapierTyp14(1);
        $papier['sm150g']->setUmschlagen(1);
        $papier['sm150g']->setHappy(1);

        $papier['sm300g'] = new Paper();
        $papier['sm300g']->setArtNr('sm300g');
        $papier['sm300g']->setGrammatur('300');
        $papier['sm300g']->setPreis('175');
        $papier['sm300g']->setDescription1('Bilderdruck seidenmatt 300g  *');
        $papier['sm300g']->setDescription2('Bilderdruck seidenmatt, aus zert. nachhaltiger Forstwirtschaft');
        $papier['sm300g']->setPapierTyp1(1);
        $papier['sm300g']->setPapierTyp2(1);
        $papier['sm300g']->setPapierTyp3(1);
        $papier['sm300g']->setPapierTyp11(1);
        $papier['sm300g']->setPapierTyp14(1);
        $papier['sm300g']->setPapierTyp13(1);
        $papier['sm300g']->setUmschlagen(1);
        $papier['sm300g']->setHappy(1);

        $papier['lwc65'] = new Paper();
        $papier['lwc65']->setArtNr('lwc65');
        $papier['lwc65']->setGrammatur('65');
        $papier['lwc65']->setPreis('73');
        $papier['lwc65']->setVolume('1');
        $papier['lwc65']->setDescription1('65g LWC');
        $papier['lwc65']->setDescription2('65g LWC');
        $papier['lwc65']->setPapierTyp1(1);

        $papier['50135'] = new Paper();
        $papier['50135']->setArtNr('50135');
        $papier['50135']->setGrammatur('135');
        $papier['50135']->setPreis('0.0275');
        $papier['50135']->setDescription1('135 g/m² Bilderdruck glänzend (FSC-zertifiziert)');
        $papier['50135']->setDescription2('135 gloss');

        $papier['100'] = new Paper();
        $papier['100']->setArtNr('100');
        $papier['100']->setGrammatur('0');
        $papier['100']->setPreis('0');
        $papier['100']->setDescription1('ohne Deckblatt');
        $papier['100']->setDescription2('ohne Deckblatt');

        $papier['101'] = new Paper();
        $papier['101']->setArtNr('101');
        $papier['101']->setGrammatur('0');
        $papier['101']->setPreis('0');
        $papier['101']->setDescription1('ohne Deckblatt');
        $papier['101']->setDescription2('ohne Deckblatt');

        $papier['10080'] = new Paper();
        $papier['10080']->setArtNr('10080');
        $papier['10080']->setGrammatur('80');
        $papier['10080']->setPreis('0.0173');
        $papier['10080']->setDescription1('80 g/m² Naturpapier (Oberfläche wie Kopierpapier, hochweiß, FSC-zert.)');
        $papier['10080']->setDescription2('080 Prep');

        $papier['10090'] = new Paper();
        $papier['10090']->setArtNr('10090');
        $papier['10090']->setGrammatur('90');
        $papier['10090']->setPreis('0.0196');
        $papier['10090']->setDescription1('90 g/m² Naturpapier (Oberfläche wie Kopierpapier, hochweiß, FSC-zert.)');
        $papier['10090']->setDescription2('090 Prep');

        $papier['sm150g'] = new Paper();
        $papier['sm150g']->setArtNr('sm150g');
        $papier['sm150g']->setGrammatur('150');
        $papier['sm150g']->setPreis('100');
        $papier['sm150g']->setDescription1('Bilderdruck seidenmatt 150g *');
        $papier['sm150g']->setDescription2('Bilderdruck seidenmatt, aus zert. nachhaltiger Forstwirtschaft');
        $papier['sm150g']->setUmschlagen(1);
        $papier['sm150g']->setHappy(1);
        $papier['sm150g']->setPapierTyp1(1);
        $papier['sm150g']->setPapierTyp2(1);
        $papier['sm150g']->setPapierTyp3(1);
        $papier['sm150g']->setPapierTyp11(1);
        $papier['sm150g']->setPapierTyp14(1);

        $papier['931001'] = new Paper();
        $papier['931001']->setArtNr('931001');
        $papier['931001']->setGrammatur('200');
        $papier['931001']->setPreis('300');
        $papier['931001']->setVolume(1);
        $papier['931001']->setPapierTyp3(1);
        $papier['931001']->setDescription1('Haftpapier hochglänzend weiß, permanent 80g');
        $papier['931001']->setDescription2('Michaelis - High Gloss White permanent geschlitzt');


        return $papier[$criteria['artNr']];

    }

    /**
     * Returns the class name of the object managed by the repository.
     *
     * @return string
     */
    public function getClassName()
    {
        // TODO: Implement getClassName() method.
    }

    public function getNewObject()
    {
        return new Paper();
    }
}
