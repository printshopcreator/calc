<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace PSC\Library\Calc\Tests\Mock;

class Paper
{
    private $id;
    private $install;
    private $artNr;
    private $auslauf;
    private $description1;
    private $description2;
    private $grammatur;
    private $staerke;
    private $breite;
    private $hoehe;
    private $laenge;
    protected $papierTyp1;
    protected $papierTyp2;
    protected $papierTyp3;
    protected $papierTyp4;
    protected $papierTyp5;
    protected $papierTyp6;
    protected $papierTyp7;
    protected $papierTyp8;
    protected $papierTyp9;
    protected $papierTyp10;
    protected $papierTyp11;
    protected $papierTyp12;
    protected $papierTyp13;
    protected $papierTyp14;
    protected $umschlagen;
    protected $happy;
    protected $eq;
    protected $sense;
    protected $sky;
    protected $glam;
    protected $post;

    private $rollenLaenge;

    /**
     * @var integer
     *
     * @ORM\Column(name="gewicht", type="integer", nullable=false)
     */
    private $gewicht;

    /**
     * @var string
     *
     * @ORM\Column(name="papierausruestung", type="string", length=60, nullable=false)
     */
    private $papierAusruestung;

    /**
     * @var string
     *
     * @ORM\Column(name="farbnummer", type="string", length=60, nullable=false)
     */
    private $farbNummer;

    /**
     * @var string
     *
     * @ORM\Column(name="farbbezeichnung", type="string", length=60, nullable=false)
     */
    private $farbBezeichnung;

    /**
     * @var string
     *
     * @ORM\Column(name="huelsendurchmesser", type="string", length=60, nullable=false)
     */
    private $huelsenDurchmesser;

    /**
     * @var string
     *
     * @ORM\Column(name="kleber", type="string", length=60, nullable=false)
     */
    private $kleber;

    /**
     * @var string
     *
     * @ORM\Column(name="abdeckpapier", type="string", length=60, nullable=false)
     */
    private $abdeckPapier;

    /**
     * @var string
     *
     * @ORM\Column(name="laufrichtung", type="string", length=2, nullable=false)
     */
    private $laufRichtung;

    /**
     * @var integer
     *
     * @ORM\Column(name="mengenangabe", type="integer", nullable=false)
     */
    private $mengenAngabe;

    /**
     * @var integer
     *
     * @ORM\Column(name="mengenangabe_palette", type="integer", nullable=false)
     */
    private $mengenangabePalette;

    /**
     * @var string
     *
     * @ORM\Column(name="inhalt", type="string", length=60, nullable=false)
     */
    private $inhalt;

    /**
     * @var string
     *
     * @ORM\Column(name="etiketten_je_blatt", type="string", length=60, nullable=false)
     */
    private $etikettenJeBlatt;

    /**
     * @var string
     *
     * @ORM\Column(name="mengeneinheit", type="string", length=30, nullable=false)
     */
    private $mengenEinheit;

    /**
     * @var integer
     *
     * @ORM\Column(name="staffelmenge_1", type="integer", nullable=false)
     */
    private $staffelmenge1;

    /**
     * @var float
     *
     * @ORM\Column(name="staffelpreis_1", type="float", precision=10, scale=0, nullable=false)
     */
    private $staffelpreis1;

    /**
     * @var integer
     *
     * @ORM\Column(name="staffelmenge_2", type="integer", nullable=false)
     */
    private $staffelmenge2;

    /**
     * @var float
     *
     * @ORM\Column(name="staffelpreis_2", type="float", precision=10, scale=0, nullable=false)
     */
    private $staffelpreis2;

    /**
     * @var integer
     *
     * @ORM\Column(name="staffelmenge_3", type="integer", nullable=false)
     */
    private $staffelmenge3;

    /**
     * @var float
     *
     * @ORM\Column(name="staffelpreis_3", type="float", precision=10, scale=0, nullable=false)
     */
    private $staffelpreis3;

    /**
     * @var integer
     *
     * @ORM\Column(name="staffelmenge_4", type="integer", nullable=false)
     */
    private $staffelmenge4;

    /**
     * @var float
     *
     * @ORM\Column(name="staffelpreis_4", type="float", precision=10, scale=0, nullable=false)
     */
    private $staffelpreis4;

    /**
     * @var integer
     *
     * @ORM\Column(name="staffelmenge_5", type="integer", nullable=false)
     */
    private $staffelmenge5;

    /**
     * @var float
     *
     * @ORM\Column(name="staffelpreis_5", type="float", precision=10, scale=0, nullable=false)
     */
    private $staffelpreis5;

    /**
     * @var string
     *
     * @ORM\Column(name="lagerort", type="string", length=7, nullable=false)
     */
    private $lagerOrt;

    /**
     * @var string
     *
     * @ORM\Column(name="verkaufshinweise", type="text", nullable=false)
     */
    private $verkaufshinweise;

    /**
     * @var string
     *
     * @ORM\Column(name="abnahmeinfo", type="string", length=60, nullable=false)
     */
    private $abnahmeInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="produkt_beschreibung", type="text", nullable=false)
     */
    private $produktBeschreibung;

    private $produktEigenschaften;

    private $produktVorteile;

    private $produktNutzen;

    private $produktAnwendungen;

    private $produktBesonderheit;

    private $musterbuch;

    private $zurAufnahmeVon;

    private $eigenschaften;

    private $preis;

    private $uuid;

    private $offsetFix;

    private $offsetVar;

    private $digitalFix;

    private $digitalVar;

    private $volume;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getInstall()
    {
        return $this->install;
    }

    /**
     * @param mixed $install
     */
    public function setInstall($install)
    {
        $this->install = $install;
    }

    /**
     * @return mixed
     */
    public function getArtNr()
    {
        return $this->artNr;
    }

    /**
     * @param mixed $artNr
     */
    public function setArtNr($artNr)
    {
        $this->artNr = $artNr;
    }

    /**
     * @return mixed
     */
    public function getAuslauf()
    {
        return $this->auslauf;
    }

    /**
     * @param mixed $auslauf
     */
    public function setAuslauf($auslauf)
    {
        $this->auslauf = $auslauf;
    }

    /**
     * @return mixed
     */
    public function getDescription1()
    {
        return $this->description1;
    }

    /**
     * @param mixed $description1
     */
    public function setDescription1($description1)
    {
        $this->description1 = $description1;
    }

    /**
     * @return mixed
     */
    public function getDescription2()
    {
        return $this->description2;
    }

    /**
     * @param mixed $description2
     */
    public function setDescription2($description2)
    {
        $this->description2 = $description2;
    }

    /**
     * @return mixed
     */
    public function getGrammatur()
    {
        return $this->grammatur;
    }

    /**
     * @param mixed $grammatur
     */
    public function setGrammatur($grammatur)
    {
        $this->grammatur = $grammatur;
    }

    /**
     * @return mixed
     */
    public function getStaerke()
    {
        return $this->staerke;
    }

    /**
     * @param mixed $staerke
     */
    public function setStaerke($staerke)
    {
        $this->staerke = $staerke;
    }

    /**
     * @return mixed
     */
    public function getBreite()
    {
        return $this->breite;
    }

    /**
     * @param mixed $breite
     */
    public function setBreite($breite)
    {
        $this->breite = $breite;
    }

    /**
     * @return mixed
     */
    public function getHoehe()
    {
        return $this->hoehe;
    }

    /**
     * @param mixed $hoehe
     */
    public function setHoehe($hoehe)
    {
        $this->hoehe = $hoehe;
    }

    /**
     * @return mixed
     */
    public function getLaenge()
    {
        return $this->laenge;
    }

    /**
     * @param mixed $laenge
     */
    public function setLaenge($laenge)
    {
        $this->laenge = $laenge;
    }

    /**
     * @return mixed
     */
    public function getRollenLaenge()
    {
        return $this->rollenLaenge;
    }

    /**
     * @param mixed $rollenLaenge
     */
    public function setRollenLaenge($rollenLaenge)
    {
        $this->rollenLaenge = $rollenLaenge;
    }

    /**
     * @return int
     */
    public function getGewicht()
    {
        return $this->gewicht;
    }

    /**
     * @param int $gewicht
     */
    public function setGewicht($gewicht)
    {
        $this->gewicht = $gewicht;
    }

    /**
     * @return string
     */
    public function getPapierAusruestung()
    {
        return $this->papierAusruestung;
    }

    /**
     * @param string $papierAusruestung
     */
    public function setPapierAusruestung($papierAusruestung)
    {
        $this->papierAusruestung = $papierAusruestung;
    }

    /**
     * @return string
     */
    public function getFarbNummer()
    {
        return $this->farbNummer;
    }

    /**
     * @param string $farbNummer
     */
    public function setFarbNummer($farbNummer)
    {
        $this->farbNummer = $farbNummer;
    }

    /**
     * @return string
     */
    public function getFarbBezeichnung()
    {
        return $this->farbBezeichnung;
    }

    /**
     * @param string $farbBezeichnung
     */
    public function setFarbBezeichnung($farbBezeichnung)
    {
        $this->farbBezeichnung = $farbBezeichnung;
    }

    /**
     * @return string
     */
    public function getHuelsenDurchmesser()
    {
        return $this->huelsenDurchmesser;
    }

    /**
     * @param string $huelsenDurchmesser
     */
    public function setHuelsenDurchmesser($huelsenDurchmesser)
    {
        $this->huelsenDurchmesser = $huelsenDurchmesser;
    }

    /**
     * @return string
     */
    public function getKleber()
    {
        return $this->kleber;
    }

    /**
     * @param string $kleber
     */
    public function setKleber($kleber)
    {
        $this->kleber = $kleber;
    }

    /**
     * @return string
     */
    public function getAbdeckPapier()
    {
        return $this->abdeckPapier;
    }

    /**
     * @param string $abdeckPapier
     */
    public function setAbdeckPapier($abdeckPapier)
    {
        $this->abdeckPapier = $abdeckPapier;
    }

    /**
     * @return string
     */
    public function getLaufRichtung()
    {
        return $this->laufRichtung;
    }

    /**
     * @param string $laufRichtung
     */
    public function setLaufRichtung($laufRichtung)
    {
        $this->laufRichtung = $laufRichtung;
    }

    /**
     * @return int
     */
    public function getMengenAngabe()
    {
        return $this->mengenAngabe;
    }

    /**
     * @param int $mengenAngabe
     */
    public function setMengenAngabe($mengenAngabe)
    {
        $this->mengenAngabe = $mengenAngabe;
    }

    /**
     * @return int
     */
    public function getMengenangabePalette()
    {
        return $this->mengenangabePalette;
    }

    /**
     * @param int $mengenangabePalette
     */
    public function setMengenangabePalette($mengenangabePalette)
    {
        $this->mengenangabePalette = $mengenangabePalette;
    }

    /**
     * @return string
     */
    public function getInhalt()
    {
        return $this->inhalt;
    }

    /**
     * @param string $inhalt
     */
    public function setInhalt($inhalt)
    {
        $this->inhalt = $inhalt;
    }

    /**
     * @return string
     */
    public function getEtikettenJeBlatt()
    {
        return $this->etikettenJeBlatt;
    }

    /**
     * @param string $etikettenJeBlatt
     */
    public function setEtikettenJeBlatt($etikettenJeBlatt)
    {
        $this->etikettenJeBlatt = $etikettenJeBlatt;
    }

    /**
     * @return string
     */
    public function getMengenEinheit()
    {
        return $this->mengenEinheit;
    }

    /**
     * @param string $mengenEinheit
     */
    public function setMengenEinheit($mengenEinheit)
    {
        $this->mengenEinheit = $mengenEinheit;
    }

    /**
     * @return int
     */
    public function getStaffelmenge1()
    {
        return $this->staffelmenge1;
    }

    /**
     * @param int $staffelmenge1
     */
    public function setStaffelmenge1($staffelmenge1)
    {
        $this->staffelmenge1 = $staffelmenge1;
    }

    /**
     * @return float
     */
    public function getStaffelpreis1()
    {
        return $this->staffelpreis1;
    }

    /**
     * @param float $staffelpreis1
     */
    public function setStaffelpreis1($staffelpreis1)
    {
        $this->staffelpreis1 = $staffelpreis1;
    }

    /**
     * @return int
     */
    public function getStaffelmenge2()
    {
        return $this->staffelmenge2;
    }

    /**
     * @param int $staffelmenge2
     */
    public function setStaffelmenge2($staffelmenge2)
    {
        $this->staffelmenge2 = $staffelmenge2;
    }

    /**
     * @return float
     */
    public function getStaffelpreis2()
    {
        return $this->staffelpreis2;
    }

    /**
     * @param float $staffelpreis2
     */
    public function setStaffelpreis2($staffelpreis2)
    {
        $this->staffelpreis2 = $staffelpreis2;
    }

    /**
     * @return int
     */
    public function getStaffelmenge3()
    {
        return $this->staffelmenge3;
    }

    /**
     * @param int $staffelmenge3
     */
    public function setStaffelmenge3($staffelmenge3)
    {
        $this->staffelmenge3 = $staffelmenge3;
    }

    /**
     * @return float
     */
    public function getStaffelpreis3()
    {
        return $this->staffelpreis3;
    }

    /**
     * @param float $staffelpreis3
     */
    public function setStaffelpreis3($staffelpreis3)
    {
        $this->staffelpreis3 = $staffelpreis3;
    }

    /**
     * @return int
     */
    public function getStaffelmenge4()
    {
        return $this->staffelmenge4;
    }

    /**
     * @param int $staffelmenge4
     */
    public function setStaffelmenge4($staffelmenge4)
    {
        $this->staffelmenge4 = $staffelmenge4;
    }

    /**
     * @return float
     */
    public function getStaffelpreis4()
    {
        return $this->staffelpreis4;
    }

    /**
     * @param float $staffelpreis4
     */
    public function setStaffelpreis4($staffelpreis4)
    {
        $this->staffelpreis4 = $staffelpreis4;
    }

    /**
     * @return int
     */
    public function getStaffelmenge5()
    {
        return $this->staffelmenge5;
    }

    /**
     * @param int $staffelmenge5
     */
    public function setStaffelmenge5($staffelmenge5)
    {
        $this->staffelmenge5 = $staffelmenge5;
    }

    /**
     * @return float
     */
    public function getStaffelpreis5()
    {
        return $this->staffelpreis5;
    }

    /**
     * @param float $staffelpreis5
     */
    public function setStaffelpreis5($staffelpreis5)
    {
        $this->staffelpreis5 = $staffelpreis5;
    }

    /**
     * @return string
     */
    public function getLagerOrt()
    {
        return $this->lagerOrt;
    }

    /**
     * @param string $lagerOrt
     */
    public function setLagerOrt($lagerOrt)
    {
        $this->lagerOrt = $lagerOrt;
    }

    /**
     * @return string
     */
    public function getVerkaufshinweise()
    {
        return $this->verkaufshinweise;
    }

    /**
     * @param string $verkaufshinweise
     */
    public function setVerkaufshinweise($verkaufshinweise)
    {
        $this->verkaufshinweise = $verkaufshinweise;
    }

    /**
     * @return string
     */
    public function getAbnahmeInfo()
    {
        return $this->abnahmeInfo;
    }

    /**
     * @param string $abnahmeInfo
     */
    public function setAbnahmeInfo($abnahmeInfo)
    {
        $this->abnahmeInfo = $abnahmeInfo;
    }

    /**
     * @return string
     */
    public function getProduktBeschreibung()
    {
        return $this->produktBeschreibung;
    }

    /**
     * @param string $produktBeschreibung
     */
    public function setProduktBeschreibung($produktBeschreibung)
    {
        $this->produktBeschreibung = $produktBeschreibung;
    }

    /**
     * @return mixed
     */
    public function getProduktEigenschaften()
    {
        return $this->produktEigenschaften;
    }

    /**
     * @param mixed $produktEigenschaften
     */
    public function setProduktEigenschaften($produktEigenschaften)
    {
        $this->produktEigenschaften = $produktEigenschaften;
    }

    /**
     * @return mixed
     */
    public function getProduktVorteile()
    {
        return $this->produktVorteile;
    }

    /**
     * @param mixed $produktVorteile
     */
    public function setProduktVorteile($produktVorteile)
    {
        $this->produktVorteile = $produktVorteile;
    }

    /**
     * @return mixed
     */
    public function getProduktNutzen()
    {
        return $this->produktNutzen;
    }

    /**
     * @param mixed $produktNutzen
     */
    public function setProduktNutzen($produktNutzen)
    {
        $this->produktNutzen = $produktNutzen;
    }

    /**
     * @return mixed
     */
    public function getProduktAnwendungen()
    {
        return $this->produktAnwendungen;
    }

    /**
     * @param mixed $produktAnwendungen
     */
    public function setProduktAnwendungen($produktAnwendungen)
    {
        $this->produktAnwendungen = $produktAnwendungen;
    }

    /**
     * @return mixed
     */
    public function getProduktBesonderheit()
    {
        return $this->produktBesonderheit;
    }

    /**
     * @param mixed $produktBesonderheit
     */
    public function setProduktBesonderheit($produktBesonderheit)
    {
        $this->produktBesonderheit = $produktBesonderheit;
    }

    /**
     * @return mixed
     */
    public function getMusterbuch()
    {
        return $this->musterbuch;
    }

    /**
     * @param mixed $musterbuch
     */
    public function setMusterbuch($musterbuch)
    {
        $this->musterbuch = $musterbuch;
    }

    /**
     * @return mixed
     */
    public function getZurAufnahmeVon()
    {
        return $this->zurAufnahmeVon;
    }

    /**
     * @param mixed $zurAufnahmeVon
     */
    public function setZurAufnahmeVon($zurAufnahmeVon)
    {
        $this->zurAufnahmeVon = $zurAufnahmeVon;
    }

    /**
     * @return mixed
     */
    public function getEigenschaften()
    {
        return $this->eigenschaften;
    }

    /**
     * @param mixed $eigenschaften
     */
    public function setEigenschaften($eigenschaften)
    {
        $this->eigenschaften = $eigenschaften;
    }

    /**
     * @return mixed
     */
    public function getPreis()
    {
        return $this->preis;
    }

    /**
     * @param mixed $preis
     */
    public function setPreis($preis)
    {
        $this->preis = $preis;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getOffsetFix()
    {
        return $this->offsetFix;
    }

    /**
     * @param mixed $offsetFix
     */
    public function setOffsetFix($offsetFix)
    {
        $this->offsetFix = $offsetFix;
    }

    /**
     * @return mixed
     */
    public function getOffsetVar()
    {
        return $this->offsetVar;
    }

    /**
     * @param mixed $offsetVar
     */
    public function setOffsetVar($offsetVar)
    {
        $this->offsetVar = $offsetVar;
    }

    /**
     * @return mixed
     */
    public function getDigitalFix()
    {
        return $this->digitalFix;
    }

    /**
     * @param mixed $digitalFix
     */
    public function setDigitalFix($digitalFix)
    {
        $this->digitalFix = $digitalFix;
    }

    /**
     * @return mixed
     */
    public function getDigitalVar()
    {
        return $this->digitalVar;
    }

    /**
     * @param mixed $digitalVar
     */
    public function setDigitalVar($digitalVar)
    {
        $this->digitalVar = $digitalVar;
    }

    /**
     * @return mixed
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param mixed $volume
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp1()
    {
        return $this->papierTyp1;
    }

    /**
     * @param mixed $papierTyp1
     */
    public function setPapierTyp1($papierTyp1): void
    {
        $this->papierTyp1 = $papierTyp1;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp2()
    {
        return $this->papierTyp2;
    }

    /**
     * @param mixed $papierTyp2
     */
    public function setPapierTyp2($papierTyp2): void
    {
        $this->papierTyp2 = $papierTyp2;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp3()
    {
        return $this->papierTyp3;
    }

    /**
     * @param mixed $papierTyp3
     */
    public function setPapierTyp3($papierTyp3): void
    {
        $this->papierTyp3 = $papierTyp3;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp4()
    {
        return $this->papierTyp4;
    }

    /**
     * @param mixed $papierTyp4
     */
    public function setPapierTyp4($papierTyp4): void
    {
        $this->papierTyp4 = $papierTyp4;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp5()
    {
        return $this->papierTyp5;
    }

    /**
     * @param mixed $papierTyp5
     */
    public function setPapierTyp5($papierTyp5): void
    {
        $this->papierTyp5 = $papierTyp5;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp6()
    {
        return $this->papierTyp6;
    }

    /**
     * @param mixed $papierTyp6
     */
    public function setPapierTyp6($papierTyp6): void
    {
        $this->papierTyp6 = $papierTyp6;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp7()
    {
        return $this->papierTyp7;
    }

    /**
     * @param mixed $papierTyp7
     */
    public function setPapierTyp7($papierTyp7): void
    {
        $this->papierTyp7 = $papierTyp7;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp8()
    {
        return $this->papierTyp8;
    }

    /**
     * @param mixed $papierTyp8
     */
    public function setPapierTyp8($papierTyp8): void
    {
        $this->papierTyp8 = $papierTyp8;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp9()
    {
        return $this->papierTyp9;
    }

    /**
     * @param mixed $papierTyp9
     */
    public function setPapierTyp9($papierTyp9): void
    {
        $this->papierTyp9 = $papierTyp9;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp10()
    {
        return $this->papierTyp10;
    }

    /**
     * @param mixed $papierTyp10
     */
    public function setPapierTyp10($papierTyp10): void
    {
        $this->papierTyp10 = $papierTyp10;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp11()
    {
        return $this->papierTyp11;
    }

    /**
     * @param mixed $papierTyp11
     */
    public function setPapierTyp11($papierTyp11): void
    {
        $this->papierTyp11 = $papierTyp11;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp12()
    {
        return $this->papierTyp12;
    }

    /**
     * @param mixed $papierTyp12
     */
    public function setPapierTyp12($papierTyp12): void
    {
        $this->papierTyp12 = $papierTyp12;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp13()
    {
        return $this->papierTyp13;
    }

    /**
     * @param mixed $papierTyp13
     */
    public function setPapierTyp13($papierTyp13): void
    {
        $this->papierTyp13 = $papierTyp13;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp14()
    {
        return $this->papierTyp14;
    }

    /**
     * @param mixed $papierTyp14
     */
    public function setPapierTyp14($papierTyp14): void
    {
        $this->papierTyp14 = $papierTyp14;
    }

    /**
     * @return mixed
     */
    public function getUmschlagen()
    {
        return $this->umschlagen;
    }

    /**
     * @param mixed $umschlagen
     */
    public function setUmschlagen($umschlagen): void
    {
        $this->umschlagen = $umschlagen;
    }

    /**
     * @return mixed
     */
    public function getHappy()
    {
        return $this->happy;
    }

    /**
     * @param mixed $happy
     */
    public function setHappy($happy): void
    {
        $this->happy = $happy;
    }

    /**
     * @return mixed
     */
    public function getEq()
    {
        return $this->eq;
    }

    /**
     * @param mixed $eq
     */
    public function setEq($eq): void
    {
        $this->eq = $eq;
    }

    /**
     * @return mixed
     */
    public function getSense()
    {
        return $this->sense;
    }

    /**
     * @param mixed $sense
     */
    public function setSense($sense): void
    {
        $this->sense = $sense;
    }

    /**
     * @return mixed
     */
    public function getSky()
    {
        return $this->sky;
    }

    /**
     * @param mixed $sky
     */
    public function setSky($sky): void
    {
        $this->sky = $sky;
    }

    /**
     * @return mixed
     */
    public function getGlam()
    {
        return $this->glam;
    }

    /**
     * @param mixed $glam
     */
    public function setGlam($glam): void
    {
        $this->glam = $glam;
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param mixed $post
     */
    public function setPost($post): void
    {
        $this->post = $post;
    }

    public function getSammelform() {
        if($this->getHappy()) {
            return 2;
        }
        if($this->getEq()) {
            return 3;
        }
        if($this->getSense()) {
            return 4;
        }
        if($this->getSky()) {
            return 5;
        }
        if($this->getGlam()) {
            return 6;
        }
        if($this->getPost()) {
            return 7;
        }
        return 1;
    }
}
