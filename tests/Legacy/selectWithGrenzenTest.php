<?php
namespace PSC\Library\Calc\Tests\Article;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class selectWithGrenzenTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/Option/Select/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);

        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/Legacy/testSelectWithGrenzen.xml'));

    }

    public function testSelectWithGrenzen()
    {
        $this->engine->calc("Test25");
        /** @var Article $article */
        $article = $this->engine->getArticle();

        $this->assertCount(2, $article->getValidOptions());

        $this->assertCount(2, $article->getOptionsAsArray());
    }

    public function testSelectWithGrenzenParams()
    {
        $this->engine->setVariable('umschlag', 4);
        $this->engine->calc("Test25");
        /** @var Article $article */
        $article = $this->engine->getArticle();

        $this->assertCount(6, $article->getValidOptions());

        $this->assertCount(6, $article->getOptionsAsArray());

        $this->assertInstanceOf('\PSC\Library\Calc\Option\Type\Select\Opt', $article->getOptionById('umschlag')->getSelectedOption());
    }

}
