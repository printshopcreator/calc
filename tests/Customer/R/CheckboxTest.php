<?php
namespace PSC\Library\Calc\Tests\Customer\R;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Error\Validation\Input\Max;
use PSC\Library\Calc\Error\Validation\Input\Min;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\PreCalc\PreCalc;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CheckboxTest extends TestCase
{

    protected ?Engine $engine;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc_checkbox.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testCalcDefault(): void
    {
        self::assertSame(26.0, $this->engine->getPrice());
        self::assertTrue($this->engine->getArticle()->getOptionById('checkbox')->getOptions()[0]->isSelected());
        self::assertTrue($this->engine->getArticle()->getOptionById('checkbox')->getOptions()[1]->isSelected());
    }

    public function testCalcOnly2(): void
    {
        $this->engine->setVariable('checkbox', [2]);
        self::assertSame(14.0, $this->engine->getPrice());
        self::assertFalse($this->engine->getArticle()->getOptionById('checkbox')->getOptions()[0]->isSelected());
        self::assertTrue($this->engine->getArticle()->getOptionById('checkbox')->getOptions()[1]->isSelected());
    }
}
