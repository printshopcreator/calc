<?php

namespace PSC\Library\Calc\Tests\Customer\R;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class RadioboxCalcTest extends TestCase
{
    protected ?Engine $engine;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc_radiobox_with_calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testCalcDefault(): void
    {
        $this->assertSame(3.22, $this->engine->getPrice());
    }

    public function testCalcZweiseitig(): void
    {
        $this->engine->setVariable('bedruckung', 'beidseitig');
        $this->assertSame(5.97, $this->engine->getPrice());
    }

    public function testCalcZweiseitig20(): void
    {
        $this->engine->setVariable('auflage', '20');
        $this->engine->setVariable('bedruckung', 'beidseitig');
        $this->assertSame(62.86, $this->engine->getPrice());
    }

}
