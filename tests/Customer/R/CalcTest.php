<?php
namespace PSC\Library\Calc\Tests\Customer\R;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Error\Validation\Input\Max;
use PSC\Library\Calc\Error\Validation\Input\Min;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\PreCalc\PreCalc;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    protected ?Engine $engine;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testOptionAuswahl330(): void
    {
        $this->assertSame('30', $this->engine->getArticle()->getOptionById('auswahl3')->getRawValue());
    }

    public function testOptionAuswahl331(): void
    {
        $this->engine->setVariable('auswahl2', 21);
        $this->engine->calc();
        $this->assertSame('31', $this->engine->getArticle()->getOptionById('auswahl3')->getRawValue());
    }

    public function testOptionCheckbox(): void
    {
        self::assertTrue($this->engine->getArticle()->getOptionById('checkboxen2')->getOptions()[0]->isSelected());
        self::assertTrue($this->engine->getArticle()->getOptionById('checkboxen2')->getOptions()[1]->isSelected());

        self::assertFalse($this->engine->getArticle()->getOptionById('checkboxen3')->getOptions()[0]->isSelected());
        self::assertTrue($this->engine->getArticle()->getOptionById('checkboxen3')->getOptions()[1]->isSelected());

    }

    public function testOptionDefaultCheckbox(): void
    {
        $this->engine->setVariable('checkboxen1', [1,2]);
        $this->engine->calc();
        self::assertTrue($this->engine->getArticle()->getOptionById('checkboxen1')->getOptions()[0]->isSelected());
        self::assertTrue($this->engine->getArticle()->getOptionById('checkboxen1')->getOptions()[1]->isSelected());
        $this->engine->setVariable('checkboxen1', [2]);
        self::assertFalse($this->engine->getArticle()->getOptionById('checkboxen1')->getOptions()[0]->isSelected());
        self::assertTrue($this->engine->getArticle()->getOptionById('checkboxen1')->getOptions()[1]->isSelected());

    }

    public function testOptionCalcCheckbox(): void
    {
        $this->assertEquals(1048, $this->engine->getPrice());
        $this->engine->setVariable('radio1', 1);
        $this->assertEquals(1044, $this->engine->getPrice());
    }
}
