<?php
namespace PSC\Library\Calc\Tests\Customer\W;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfDefaultPriceIsOk(): void
    {
        $dateTime = new \DateTime('13:12:12 03.02.2023');
        $this->engine->getArticle()->getOptionById('versand')->getSelectedOption()->setCurDate($dateTime);
        self::assertSame('', $this->engine->getArticle()->getOptionById('versand')->getSelectedOption()->getInfo());

        self::assertSame('Germany', $this->engine->getArticle()->getOptionById('versand')->getSelectedOption()->getCountry());

        self::assertSame("08.02.2023", $this->engine->getArticle()->getOptionById('versand')->getSelectedOption()->getDeliveryDateAsString(),
            'Act: ' . $this->engine->getArticle()->getOptionById('versand')->getSelectedOption()->getDeliveryDateAsString() . ' Exp: '.date('d.m.Y'));
    }

    public function testMvWestpomPriceIsOk(): void
    {
        $dateTime = new \DateTime('13:12:12 03.02.2023');
        $this->engine->setVariable('versand', 1);
        $this->engine->calc();
        $this->engine->getArticle()->getOptionById('versand')->getSelectedOption()->setCurDate($dateTime);

        self::assertSame('Sofortiger Produktionsbeginn bei Dateneingang und Zahlung bis 18 Uhr.', $this->engine->getArticle()->getOptionById('versand')->getSelectedOption()->getInfo());

        self::assertSame('Germany/MecklenburgWesternPomerania', $this->engine->getArticle()->getOptionById('versand')->getSelectedOption()->getCountry());

        self::assertSame("17.02.2023", $this->engine->getArticle()->getOptionById('versand')->getSelectedOption()->getDeliveryDateAsString(),
            'Act: ' . $this->engine->getArticle()->getOptionById('versand')->getSelectedOption()->getDeliveryDateAsString() . ' Exp: '.date('d.m.Y'));
    }
}
