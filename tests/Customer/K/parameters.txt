// AEB Exportfilling

//Tairfe pro Monat Export
$tarif_starter=33;
$tarif_basic=92;
$tarif_business=356;
$tarif_enterprise=1843;

//Tairfe pro Monat Export Brexit
$tarif_starter_brexit=33;
$tarif_basic_brexit=92;
$tarif_business_brexit=356;
$tarif_enterprise_brexit=1843;

//Schwellwerte für Tarifwechsel Export
//bis jetzt nich verwendbar in Grenzen
$schwellwert_starter=158;
$schwellwert_basic=1374;
$schwellwert_business=13749;
$schwellwert_enterprise_ab=13750;

//enthaltene Ausfuhren Export
$enthaltene_ausfuhren_starter=25;
$enthaltene_ausfuhren_basic=250;
$enthaltene_ausfuhren_business=2500;
$enthaltene_ausfuhren_enterprise=25000;

//enthaltene Ausfuhren Export Brexit
$enthaltene_ausfuhren_starter_brexit=50;
$enthaltene_ausfuhren_basic_brexit=250;
$enthaltene_ausfuhren_business_brexit=2500;
$enthaltene_ausfuhren_enterprise_brexit=25000;

// Setup-Kosten
$setup_exportfilling=600;

//SAP Setup einmalig Export filing Chief
$sap_export_filing_chief_ecc=5700;
$sap_export_filing_chief_hana=7600;

//SAP monatlich Export filing Chief
$sap_export_filing_chief_monatlich=225;

// Setup-Kosten Export Brexit
$setup_export_starter_brexit=0;
$setup_export_basic_brexit=0;
$setup_export_business_brexit=0;
$setup_export_enterprise_brexit=600;

//Kosten zusätzliche Ausfuhren Export
$kosten_ausfuhren_starter=3.60;
$kosten_ausfuhren_basic=2.82;
$kosten_ausfuhren_business=1.59;
$kosten_ausfuhren_enterprise=0.88;

//Kosten zusätzliche Ausfuhren Export Brexit
$kosten_ausfuhren_starter_brexit=3.60;
$kosten_ausfuhren_basic_brexit=2.82;
$kosten_ausfuhren_business_brexit=1.59;
$kosten_ausfuhren_enterprise_brexit=0.88;

//AEB Importfilling
//Tarife pro Monat Import
$tarif_basic_import=92;
$tarif_business_import=626;
$tarif_enterprise_import=1843;

//enthaltene Ausfuhren Import
$enthaltene_ausfuhren_basic_import=100;
$enthaltene_ausfuhren_business_import=2500;
$enthaltene_ausfuhren_enterprise_import=10000;

//Kosten zusätzliche Ausfuhren Import
$kosten_ausfuhren_basic_import=5.34;
$kosten_ausfuhren_business_import=3.89;
$kosten_ausfuhren_enterprise_import=2.21;

// Lizenzkosten CSP Badge Licence
$csp_badge_licence=127.5;
$csp_per_declaration=1;