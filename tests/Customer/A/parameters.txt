$onlinerabatt_flaschenanhaenger_p = 0;
$onlinerabatt_flaschenanhaenger_f = 10;

$onlinerabatt_digitalplot_p = 0;
$onlinerabatt_digitalplot_f = 5;

$onlinerabatt_ausdruck_p = 0;
$onlinerabatt_ausdruck_f = 10;

$onlinerabatt_digitalproof_p = 0;
$onlinerabatt_digitalproof_f = 0;
$medienkeil_aufpreis_f = 8;

$onlinerabatt_farbscan_p = 0;
$onlinerabatt_farbscan_f = 5;

$onlinerabatt_strichscan_p = 0;
$onlinerabatt_strichscan_f = 5;

$hour_druckvorstufe = 60;
$geiferRand = 2.3;
$bogenRaender = 1.5;

$onlinerabatt_broschuere_p = 0;
$onlinerabatt_broschuere_f = 30;
$aktionsrabatt_broschuere_p = 3;
$aktionsrabatt_broschuere_f = 0;

$onlinerabatt_plano_p = 0;
$onlinerabatt_plano_f = 10;

$onlinerabatt_privatdrucksachen_p = 0;
$onlinerabatt_privatdrucksachen_f = 5;

$aktionsrabatt_plano_p = 6;
$aktionsrabatt_plano_p_vk = 6;
$aktionsrabatt_plano_f = 0;
$aktionsrabatt_plano_f_vk = 0;

$papierzuschlag_broschuere = 1.3;
$papierzuschlag_plano = 1.3;

$onlinerabatt_20 = -20;
$onlinerabatt_10 = -10;

$onlinerabatt_etikett = -10;

$kostenProDruckplatte = 15;
$stundenSatzDruck = 140;
$stundenSatzDruckGTO = 40;
$ruestZeitJeDruckplatte = 5;
$ruestZeitJeDruckplatteGTO = 15;
$kostenProZusatzdruckplatteGTO = 35;

$farbwechselZeit = 45;
$farbwechselZeitGTO = 20;

$stundensatz_wtv_maschinen = 50;
$stundensatz_wtv = 30;
$stundensatz_vorstufe = 60;
$stundensatz_vorstufe_daten_huellen = 60;
$vorstufe_fixkosten = 20;

$papieraufschlag_huellen = 1.3;

$durckfarbe_preis_je_bogen = 1;

$buchrueckenstaerke = 0.36;

// WTV
$ringoesen = 60;
$oesen_stkpreis = 0.20;
$oesen_fixpreis = 45;
$beilage_einlegen_bestimmte = 45;
$beilage_einlegen_unbestimmte = 35;
$buendeln = 0.15;
$perforation = 25;
$laserstanzung = 0.50;
$folieeinschweissen = 0.13;

$ruestkosten_perforation = 35;
$ruestkosten_lochung = 15;
$ruestkosten_umlaufender_karton = 35;

$fixkosten_nummerierung = 15;
$fixkosten_blockleimung = 25;
$fixkosten_umlaufender_karton = 0.6;

$umschlag_kaschierung_mk = 0.8;
$umschlag_kaschierung_gl = 0.6;

$faktor_klein_gross_falzung = 3;

$plano_fa_aufpreis_anderes_dateiformat = 25;
$plano_fa_fixpreis_buendeln = 15;
$plano_fa_preis_je_buendel = 0.15;
$plano_fa_fixpreis_perforation = 35;
$plano_fa_tsdpreis_perforation = 35;
$plano_fa_tsdpreis_ecken_runden = 16;
$plano_fa_tsdpreis_ecken_runden_nassleimetik = 1.50;
$plano_fa_fixpreis_ecken_runden = 15;
$plano_fa_fixpreis_bohren = 25;
$plano_fa_tsdpreis_bohren = 5;
$plano_fa_stckpreis_laserstanzung = 0.50;
$plano_fa_fixpreis_laserstanzung = 150;
$plano_fa_fixpreis_cito = 260;
$plano_fa_tsdpreis_cito = 35;
$plano_fa_tsdpreis_cito_nassleimetik = 2;

// Sammelform
$sammelform_brosch_inhalt_happy = -60;
$sammelform_brosch_inhalt_eq = -45;
$sammelform_brosch_inhalt_sense = -45;
$sammelform_brosch_inhalt_sky = -30;
$sammelform_brosch_inhalt_glam = -60;
$sammelform_brosch_inhalt_post = 0;

$sammelform_brosch_umschlag_happy = -60;
$sammelform_brosch_umschlag_eq = -40;
$sammelform_brosch_umschlag_sense = -20;
$sammelform_brosch_umschlag_sky = -10;
$sammelform_brosch_umschlag_glam = -40;
$sammelform_brosch_umschlag_post = 0;

$sammelform_plano_fa_happy = -180;
$sammelform_plano_fa_eq = -160;
$sammelform_plano_fa_sense = -130;
$sammelform_plano_fa_sky = -125;
$sammelform_plano_fa_glam = -90;
$sammelform_plano_fa_post = -90;
$sammelform_plano_fa_happy_rapetik = - 280;
$sammelform_plano_fa_happy_aufkleber = - 280;
$sammelform_plano_fa_eq_aufkleber = - 250;
$sammelform_plano_fa_stdformatabschlag = 30;

// Banderolen Verschluss
$klebepunkte_lose_fix = 15;
$klebepunkte_lose_pro_tausend = 12;
$klebepunkte_vorkonf_fix = 15;
$klebepunkte_vorkonf_pro_tausend = 47;
$haftstreifen_lose_fix = 15;
$haftstreifen_lose_pro_tausend = 15;
$haftstreifen_vorkonf_fix = 45;
$haftstreifen_vorkonf_pro_tausend = 85;

// Digitaldruck
$standard_bogenbreite = 45;
$standard_bogenhoehe = 32;
$stundensatz_vorstufe_digitaldruck = 90;
$stundensatz_nuten_digitaldruck = 15;
$stundensatz_klebebinder = 200;
$papierzuschlag_digitaldruck = 1.3;
$klick_digitaldruck_a4fbg = 0.17;
$klick_digitaldruck_a4sw = 0.07;


$plano_fa_maxabschlag_proz_ek = 90;
$plano_fa_maxabschlag_abs = 7;

$plano_stanzen_fix1 = 35;
$plano_stanzen_fix1_vkru7 = 15;
$plano_stanzen_fix2 = 85;
$plano_stanzen_fix3_mappen = 260;

$plano_stanzen_var1 = 13;
$plano_stanzen_var2 = 33;
$plano_stanzen_var3_mappen = 40;


$plano_praegen_fix1 = 60;
$plano_praegen_fix2 = 110;
$plano_praegen_var1 = 25;
$plano_praegen_var2 = 45;

$plano_digitalproof_minimal = 15;


$plano_express_f = 45;
$plano_express_p = 13;
$plano_relax_f = 0;
$plano_relax_p = 15;
$plano_max_highend_f = 0;
$plano_max_highend_p = 10;
$plano_easy_value_f = 0;
$plano_easy_value_p = 10;

$rdh_aufschlag_sonderformate = 1.7;
$bro_aufschlag_pur = 1.22;
$bro_aufschlag_wire_o = 1.40;
$bro_aufschlag_hdcover = 1.20;

$bro_weight_faktor = 1.05;

$brosch_express_f = 45;
$brosch_express_p = 13;
$brosch_relax_f = 0;
$brosch_relax_p = 8;
$brosch_max_highend_f = 60;
$brosch_max_highend_p = 10;
$brosch_easy_value_f = 0;
$brosch_easy_value_p = 10;