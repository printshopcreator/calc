<?php
namespace PSC\Library\Calc\Tests\Customer\M;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testDefaultOption(): void
    {
        $article = $this->engine->getArticle();
        $option = $article->getOptionById('lochbohrung');
        $this->assertEquals("ohne Lochbohrung", $option->getValue());
    }

    public function testCalcValues(): void
    {
        $this->engine->setVariable('lochbohrung', 1);
        $article = $this->engine->getArticle();
        $option = $article->getOptionById('lochbohrung');
        $this->assertEquals("mit Lochbohrung (+2,50 Euro/Stück)", $option->getValue());
    }
}
