<?php
namespace PSC\Library\Calc\Tests\Customer\P;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Error\Validation\Input\Max;
use PSC\Library\Calc\Error\Validation\Input\Min;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\PreCalc\PreCalc;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    protected ?Engine $engine;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testCalcPrice(): void
    {
        $article = $this->engine->getArticle();
    
        $this->assertSame(92.16, $this->engine->getPrice());

    }

    public function testHelpTextAndLink(): void
    {
        $article = $this->engine->getArticle();
        $this->assertSame("Hello World", $article->getOptionById('breite_frei')->getHelp());
        $this->assertSame("cms/test", $article->getOptionById('breite_frei')->getHelpLink());
    }

    public function testMaxBreite(): void
    {
        $article = $this->engine->getArticle();

        $this->engine->setVariable('breite_frei', 600);
        $this->engine->calc();
        $this->assertTrue($article->getOptionById('breite_frei')->isValid());
        $this->assertEquals(1, count($article->getOptionById('breite_frei')->getValidationErrors()));
        $this->assertInstanceOf(Max::class, $article->getOptionById('breite_frei')->getValidationErrors()->first());
    }

    public function testMinBreite(): void
    {
        $article = $this->engine->getArticle();

        $this->engine->setVariable('breite_frei', 10);
        $this->engine->calc();
        $this->assertTrue($article->getOptionById('breite_frei')->isValid());
        $this->assertEquals(1, count($article->getOptionById('breite_frei')->getValidationErrors()));
        $this->assertInstanceOf(Min::class, $article->getOptionById('breite_frei')->getValidationErrors()->first());
    }


}
