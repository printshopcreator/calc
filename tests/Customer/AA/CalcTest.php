<?php
namespace PSC\Library\Calc\Tests\Customer\AA;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testAuflageBelow100(): void
    {
        $this->assertEquals("ohne" , $this->engine->getArticle()->getOptionById('druckfarbe')->getRawValue());
        $this->assertEquals("ohne" , $this->engine->getArticle()->getOptionById('druckart')->getRawValue());
    }

    public function testAuflageBelow100AndMaterial160(): void
    {
        $this->engine->setVariable('auflage', 99);
        $this->engine->setVariable('material', 160);
        $this->engine->calc();
        $this->assertEquals("schwarz" , $this->engine->getArticle()->getOptionById('druckfarbe')->getRawValue());

        /** @var Select $option */
        $option = $this->engine->getArticle()->getOptionById('druckart');

        $this->assertEquals("einseitig" , $option->getRawValue());
        $this->assertCount(2, $option->getValidOptions());
    }

    public function testAuflageBelow100AndMaterial250(): void
    {
        $this->engine->setVariable('auflage', 140);
        $this->engine->calc();
        $this->assertEquals("ohne" , $this->engine->getArticle()->getOptionById('druckfarbe')->getRawValue());

        /** @var Select $option */
        $option = $this->engine->getArticle()->getOptionById('druckart');

        $this->assertEquals("ohne" , $option->getRawValue());
        $this->assertCount(1, $option->getValidOptions());
    }

}
