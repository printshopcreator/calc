<?php
namespace PSC\Library\Calc\Tests\Customer\N;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\PreCalc\PreCalc;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    protected ?Engine $engine;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testPreCalcOption()
    {
        $article = $this->engine->getArticle();
    
        $this->assertInstanceOf(PreCalc::class, $article->getPreCalc());
        $this->assertSame(5.00, $this->engine->getPrice());

        $values = $article->getPreCalc()->getGroups()[0]->getVariants()[3]->getValues();
        foreach($values as $value) {
            $this->engine->setVariable($value->getKey(), $value->getValue());
        }
        $this->assertSame(2000.00, $this->engine->getPrice());
    }

}
