<?php

namespace PSC\Library\Calc\Tests\Customer\DD;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{
    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));


    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testPrice(): void
    {
        $this->engine->calc();
        $this->assertSame(32.22, $this->engine->getPrice());
    }

    public function testPricePaper(): void
    {
        $this->engine->setVariable('papierauswahl', 'papier5_1');
        $this->engine->calc();
        $this->assertSame(34.77, $this->engine->getPrice());
    }

    public function testPricePaper1(): void
    {
        $this->engine->setVariables([
          'preisart_nopresentationall' => '1',
          'festpreis_nopresentationall' => '1',
          'produktart_nopresentationpdf' => '10',
          'kalkparameterprodukt_nopresentationpdf' => '10',
          'datacheck' => '1',
          'korrektur' => '1',
          'produktion' => '1',
          'eformat' => '62370H',
          'nutzenrechner_nopresentationpdf' => '1',
          'plano' => '0',
          'oformatU' => 'XXXXXXXX',
          'formatpapierlaufrichtungU_nopresentationpdf' => '0',
          'oformat' => '02100297',
          'formatpapierlaufrichtung_nopresentationpdf' => '0',
          'broschuereklammer500' => '0',
          'broschuereklammer500param' => '0',
          'druckenU_nopresentationpdf' => 'X',
          'drucken1xxU_nopresentationpdf' => 'X',
          'farbeU' => 'X',
          'schneiden200U_nopresentationpdf' => 'X',
          'multifinisher400U_nopresentationpdf' => '0',
          'multifinisher400Uparam1_nopresentationpdf' => '0',
          'falzen302U_nopresentationpdf' => 'X',
          'drucken_nopresentationpdf' => '1',
          'drucken1xx_nopresentationpdf' => '1a',
          'farbe' => '4f4f',
          'schneiden200_nopresentationpdf' => '1',
          'multifinisher400_nopresentationpdf' => '0',
          'multifinisher400param1_nopresentationpdf' => '0',
          'falzen302_nopresentationpdf' => '0',
          'stanzen600_nopresentationpdf' => '0',
          'stanzen600param_nopresentationpdf' => '2',
          'stanzen600param2_nopresentationpdf' => '2',
          'papierauswahlU' => 'papier1_1',
          'papierU' => 'INM300ND',
          'papierlaufrichtungU_nopresentationpdf' => '1',
          'seitenU' => '0',
          'papierauswahl' => 'papier5_1',
          'papier' => 'GP150',
          'papierlaufrichtung_nopresentationpdf' => '1',
          'seiten' => '0',
          'bohrmaschine' => '0',
          'stdl701pselect' => '0',
          'eckenstanze' => '0',
          'stdl702pselect' => '0',
          'codierung' => '0',
          'codeart' => '0',
          'falzen302Uopt1' => '0',
          'falzen302Uopt1param' => '0',
          'kaschieren800U' => '0',
          'kaschieren800Uparam' => '0',
          'falzen302opt1' => '0',
          'falzen302opt1param' => '0',
          'kaschieren800' => '0',
          'kaschieren800param' => '0',
          'zusatzarbeit1select_nopresentation' => '00',
          'zusatzarbeit2select_nopresentation' => '00',
          'offline_nopresentation' => '0',
          'angebotstyp_nopresentation' => '0',
          'produktname_nopresentationpdf' => '',
          'auflage' => '100',
          'versionen' => '1',
          'eformatbreite_nopresentationpdf' => '210',
          'eformathoehe_nopresentationpdf' => '297',
          'oformatbreiteU_nopresentationpdf' => '210',
          'oformathoeheU_nopresentationpdf' => '297',
          'nutzenU_nopresentationpdf' => '2',
          'show_bernutzenU_max_nopresentationpdf' => '2',
          'show_bernutzenU_min_nopresentationpdf' => '1',
          'show_bernutzenU_nopresentationpdf' => '2',
          'oformatbreite_nopresentationpdf' => '210',
          'oformathoehe_nopresentationpdf' => '297',
          'nutzen_nopresentationpdf' => '2',
          'show_bernutzen_max_nopresentationpdf' => '2',
          'show_bernutzen_min_nopresentationpdf' => '1',
          'show_bernutzen_nopresentationpdf' => '2',
          'falzen302Uparam1input_nopresentationpdf' => '30',
          'falzen302Uparam2input_nopresentationpdf' => '10',
          'falzen302Uparam3input_nopresentationpdf' => '0',
          'falzen302param1input_nopresentationpdf' => '30',
          'falzen302param2input_nopresentationpdf' => '10',
          'falzen302param3input_nopresentationpdf' => '0',
          'stanzen600bogennutzen_nopresentationpdf' => '1',
          'stanzen600nutzen_nopresentationpdf' => '1',
          'stanzen600hub_nopresentationpdf' => '2',
          'stanzen600paraminput_nopresentationpdf' => '3000',
          'stanzen600param2input_nopresentationpdf' => '15',
          'papiernameU_nopresentationpdf' => '',
          'papiergrammaturU_nopresentationpdf' => '300',
          'papierstaerkeU_nopresentationpdf' => '0.303',
          'papierpreisU_nopresentationpdf' => '76.8',
          'papierbreiteU_nopresentationpdf' => '345',
          'papierhoeheU_nopresentationpdf' => '495',
          'papiername_nopresentationpdf' => '',
          'papiergrammatur_nopresentationpdf' => '135',
          'papierstaerke_nopresentationpdf' => '0.118',
          'papierpreis_nopresentationpdf' => '0',
          'papierbreite_nopresentationpdf' => '345',
          'papierhoehe_nopresentationpdf' => '495',
          'produktname_nopresentation' => '',
          'satzarbeitinput_nopresentation' => '',
          'satzarbeit_nopresentation' => '0',
          'zusatzarbeit1_nopresentation' => '0',
          'zusatzarbeit2_nopresentation' => '0',
          'zusatzarbeit3input_nopresentation' => '',
          'zusatzarbeit3_nopresentation' => '0',
          'zusatzarbeit4input_nopresentation' => '',
          'zusatzarbeit4_nopresentation' => '0',
          'rabattkalk_nopresentation' => '0',
          'angebotsadresse_nopresentation' => '',
          'eckenstanzecheckbox1' =>
          array(
              0 => '1',
          ),
          'eckenstanzecheckbox2' =>
          array(
              0 => '1',
          ),
          'eckenstanzecheckbox3' =>
          array(
              0 => '1',
          ),
          'eckenstanzecheckbox4' =>
          array(
              0 => '1',
          ),
        ]);
        $this->assertSame(37.74, $this->engine->getPrice());
    }

}
