<?php
namespace PSC\Library\Calc\Tests\Customer\FF;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));


    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testPrice(): void
    {
        $this->engine->calc();
        $this->assertSame(3000.0, $this->engine->getPrice());
    }

    public function testPriceExpress(): void
    {
        $this->engine->setVariable('produktion', 'digital_express');
        $this->engine->calc();
        $this->assertSame(130.0, $this->engine->getPrice());
    }

    public function testPriceFlexo(): void
    {
        $this->engine->setVariable('produktion', 'flexo');
        $this->engine->calc();
        $this->assertSame(175.0, $this->engine->getPrice());
        $this->assertCount(3, $this->engine->getArticle()->getPreCalc()->getGroups());
    }

}
