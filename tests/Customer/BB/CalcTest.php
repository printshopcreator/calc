<?php
namespace PSC\Library\Calc\Tests\Customer\BB;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testPattern(): void
    {
        $this->engine->calc();
        $this->assertEquals("[0-9]+", $this->engine->getArticle()->getOptionById('format1')->getPattern());
    }

    public function testPlaceHolder(): void
    {
        $this->engine->calc();
        $this->assertEquals("Platzhalter", $this->engine->getArticle()->getOptionById('betreff')->getPlaceHolder());
        $this->assertTrue($this->engine->getArticle()->getOptionById('betreff')->isRequire());
    }

    public function testFormat1(): void
    {
        $this->engine->calc();
        $this->assertEquals(5 , $this->engine->getArticle()->getOptionById('format1')->getRawValue());
        $this->assertTrue($this->engine->getArticle()->getOptionById('format1')->isValid());
    }

    public function testFormat2(): void
    {
        $this->engine->setVariable('auflage', 210);
        $this->engine->calc();
        $this->assertTrue($this->engine->getArticle()->getOptionById('format1')->isValid());
        $this->assertEquals(50, $this->engine->getArticle()->getOptionById('format1')->getRawValue());
        $this->assertEquals("format2" , $this->engine->getArticle()->getOptionById('format1')->getName());
    }

    public function testFormat3(): void
    {
        $this->engine->setVariable('auflage', 412);
        $this->engine->calc();
        $this->assertTrue($this->engine->getArticle()->getOptionById('format1')->isValid());
        $this->assertEquals(150, $this->engine->getArticle()->getOptionById('format1')->getRawValue());
        $this->assertEquals("format3" , $this->engine->getArticle()->getOptionById('format1')->getName());
    }

}
