<?php
namespace PSC\Library\Calc\Tests\Customer\CC;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testContainerSelectFalse(): void
    {
        $this->engine->calc();
        $this->assertFalse($this->engine->getArticle()->getOptionById('dd')->isValid());
        $this->assertFalse($this->engine->getArticle()->getOptionById('defa')->isValid());
    }

    public function testContainerSelectBinden(): void
    {
        $this->engine->setVariable('binden', 'leimes');
        $this->engine->calc();
        $this->assertTrue($this->engine->getArticle()->getOptionById('dd')->isValid());
        $this->assertTrue($this->engine->getArticle()->getOptionById('defa')->isValid());
    }

    public function testContainerSelectIgnoreDefaultBinden(): void
    {
        $this->engine->setVariable('binden', 'leimes');
        $this->engine->calc();
        $this->assertTrue($this->engine->getArticle()->getOptionById('dd')->isValid());
        $this->assertTrue($this->engine->getArticle()->getOptionById('defa')->isValid());

        $this->engine->setVariable('dd', 'ddleim');
        $this->engine->calc();

        $this->assertTrue($this->engine->getArticle()->getOptionById('dd')->isValid());
        $this->assertSame('ddleim', $this->engine->getArticle()->getOptionById('dd')->getRawValue());
        $this->assertTrue($this->engine->getArticle()->getOptionById('defa')->isValid());
    }

}
