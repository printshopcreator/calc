<?php
namespace PSC\Library\Calc\Tests\Customer\Q;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Error\Validation\Input\Max;
use PSC\Library\Calc\Error\Validation\Input\Min;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\PreCalc\PreCalc;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    protected ?Engine $engine;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testCalcPrice(): void
    {
        $article = $this->engine->getArticle();
    
        $this->assertSame(11.22, $this->engine->getPrice());

    }

    public function testCalcPriceDefaultChange(): void
    {
        $this->engine->setVariable('print', '885731');

        $this->assertSame(50.29, $this->engine->getPrice());

    }

    public function testCalcPriceDefaultChangeSecound(): void
    {
        $this->engine->setVariable('print', '885731');
        $this->assertSame(50.29, $this->engine->getPrice());
        $this->engine->setVariable('print', '885731');
        $this->engine->setVariable('auflage', '40329547');
        $this->assertSame(276.48, $this->engine->getPrice());
        $this->engine->setVariable('print', '885730');
        $this->assertSame(11.22, $this->engine->getPrice());
    }
}
