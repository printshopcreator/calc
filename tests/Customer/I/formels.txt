$hilfsvalue_abschlag_sammelform_plano = '((($CVhilfsvalue_sammelform_format_auflage$CV/100*75)+($CVhilfsvalue_sammelform_menge_auflage$CV/100*25))/(75+25)*100)';

$hilfsvalue_sammelform_format_plano = '(max(0,getSammelformFormatPlano($Vsortenzahl$V*$CVformat_offen_breite_auflage$CV*$CVformat_offen_hoehe_auflage$CV)-(5*abs(1.408-max($CVformat_offen_breite_auflage$CV,$CVformat_offen_hoehe_auflage$CV)/min($CVformat_offen_breite_auflage$CV,$CVformat_offen_hoehe_auflage$CV)))))';

if(!function_exists('getSammelformMengenAbschlagPlano')) {

    function getSammelformMengenAbschlagPlano($menge) {

        if($menge < 2500) return 100;
        if($menge < 7500) return 90;
        if($menge < 10000) return 80;
        if($menge < 15000) return 70;
        if($menge < 20000) return 60;
        if($menge < 35000) return 50;
        if($menge < 50000) return 25;
        return 1;
    }

    function getSammelformFormatPlano($value) {
        if($value < 56) return 150;
        if($value < 63) return 130;
        if($value < 649) return 100;
        if($value < 1299) return 90;
        if($value < 1999) return 80;
        if($value < 2499) return 60;
        return 0;
    }


    function getCalcTableValue($what, $papierType1,$papierType2,$papierType3,$papierType4,$papierType5,$papierType6,$papierType7,$papierType8,$papierType9,$papierType10,$papierType11,$papierType12,$papierType13,$papierType14, $kostenDruckplatte, $druckplatteOption, $gammatur, $papierKosten, $weiterVerarbeitung, $seiten, $auflage, $anzahlDruckfarben, $anzahlFarbwechsel, $anzahlDruckJeForm, $format_offen_breite, $format_offen_hoehe, $geiferRand, $bogenRaender) {

        $price = 9999999999;
        $nutzen = 9999999999;
        $nettobg = 9999999999;
        $zuschuss = 9999999999;
        $bogen_hoehe = 1000;
        $bogen_breite = 1000;
        $ctp = 999999999;

        $formate=array();
        if($papierType1) {
            $formate[] = array('type' => 1, 'breite' => 88, 'hoehe' => 63, 'seiten' => 4);
        }
        if($papierType1 && ($weiterVerarbeitung == 3)) {
            $formate[] = array('type' => 2, 'breite' => 88, 'hoehe' => 63, 'seiten' => 2);
        }

        if($papierType2) {
            $formate[] = array('type' => 5, 'breite' => 100, 'hoehe' => 70, 'seiten' => 4);
        }
        if($papierType2 && ($weiterVerarbeitung == 3)) {
            $formate[] = array('type' => 5, 'breite' => 100, 'hoehe' => 70, 'seiten' => 2);
        }

        if($papierType3) {
            //$formate[] = array('type' => 3, 'breite' => 70, 'hoehe' => 50, 'seiten' => 4);
        }
        if($papierType3 && ($weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 3, 'breite' => 70, 'hoehe' => 50, 'seiten' => 2);
        }

        if($papierType4) {
            //$formate[] = array('type' => 7, 'breite' => 61, 'hoehe' => 43, 'seiten' => 4);
        }
        if($papierType4 && ($weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 7, 'breite' => 61, 'hoehe' => 43, 'seiten' => 2);
        }

        if($papierType5) {
            //$formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 11.4, 'seiten' => 4);
        }
        if($papierType5 && ($weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 11.4, 'seiten' => 2);
        }

        if($papierType6) {
            //$formate[] = array('type' => 7, 'breite' => 32.4, 'hoehe' => 22.9, 'seiten' => 4);
        }
        if($papierType6 && ($weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 7, 'breite' => 32.4, 'hoehe' => 22.9, 'seiten' => 2);
        }

        if($papierType7) {
            //$formate[] = array('type' => 7, 'breite' => 35.3, 'hoehe' => 25, 'seiten' => 4);
        }
        if($papierType7 && ($weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 7, 'breite' => 35.3, 'hoehe' => 25, 'seiten' => 2);
        }

        if($papierType8) {
            //$formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 16.2, 'seiten' => 4);
        }
        if($papierType8 && ($weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 16.2, 'seiten' => 2);
        }

        if($papierType9) {
            //$formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 11, 'seiten' => 4);
        }
        if($papierType9 && ($weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 11, 'seiten' => 2);
        }

        if($papierType10) {
            //$formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 22, 'seiten' => 4);
        }
        if($papierType10 && ($weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 22, 'seiten' => 2);
        }

        if($papierType11) {
            $formate[] = array('type' => 7, 'breite' => 92, 'hoehe' => 65, 'seiten' => 4);
        }
        if($papierType11 && ($weiterVerarbeitung == 3)) {
            $formate[] = array('type' => 7, 'breite' => 92, 'hoehe' => 65, 'seiten' => 2);
        }

        if($papierType12) {
            //$formate[] = array('type' => 7, 'breite' => 64, 'hoehe' => 45, 'seiten' => 4);
        }
        if($papierType12 && ($weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 7, 'breite' => 64, 'hoehe' => 45, 'seiten' => 2);
        }

        if($papierType13) {
            //$formate[] = array('type' => 7, 'breite' => 44, 'hoehe' => 31.5, 'seiten' => 4);
        }
        if($papierType13 && ($weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 7, 'breite' => 44, 'hoehe' => 31.5, 'seiten' => 2);
        }
        if($papierType14) {
            $formate[] = array('type' => 7, 'breite' => 102, 'hoehe' => 72, 'seiten' => 4);
        }
        if($papierType14 && ($weiterVerarbeitung == 3)) {
            $formate[] = array('type' => 7, 'breite' => 102, 'hoehe' => 72, 'seiten' => 2);
        }

        //echo "<br/><br/>";

        foreach($formate as $key => $format) {

            if($format['seiten'] == 4) {
                $calc1=floor((($format['breite']-$bogenRaender)/($format_offen_breite)))*floor((($format['hoehe']-$geiferRand))/$format_offen_hoehe)*2;
                $calc2=floor((($format['hoehe']-$bogenRaender)/($format_offen_breite)))*floor((($format['breite']-$geiferRand))/$format_offen_hoehe)*2;
            }else{
                $calc1=floor((($format['breite']-$bogenRaender)/($format_offen_breite/2)))*floor((($format['hoehe']-$geiferRand))/$format_offen_hoehe);
                $calc2=floor((($format['hoehe']-$bogenRaender)/($format_offen_breite/2)))*floor((($format['breite']-$geiferRand))/$format_offen_hoehe);
            }

            $nutzen_calc = ($calc1 > $calc2)? $calc1: $calc2;

            $calc1 = 200;

            $calc2 = ($anzahlDruckJeForm*$anzahlDruckfarben*100*ceil($seiten/($nutzen_calc*2))+($anzahlFarbwechsel*100)+($seiten/($nutzen_calc*2)*$auflage*0.02));

            $zuschussCalc = ($calc1 > $calc2)? $calc1 : $calc2;

            $nettoBedruckGesamt = $seiten/$nutzen_calc*$auflage/2;
            $ctp1 = ceil($seiten/$nutzen_calc)*($anzahlDruckfarben)+$druckplatteOption;
            $price_calc = ((($nettoBedruckGesamt+$zuschussCalc)*$format['hoehe']*$format['breite']*$gammatur*$papierKosten/1000000000))+($kostenDruckplatte*$ctp1);
            //var_dump($nutzen_calc);
            //var_dump($format['hoehe'].'-'.$format['breite'].'-'.$ctp1.'-'.$nettoBedruckGesamt.'-'.$price_calc.'-'.$price);
            //echo "<br/>";

            if($price_calc < $price && $nutzen > 0) {
                $price = $price_calc;
                $nutzen = $nutzen_calc;
                $ctp = $ctp1;
                $zuschuss = $zuschussCalc;
                $nettobg = $nettoBedruckGesamt;
                $bogen_hoehe = $format['hoehe'];
                $bogen_breite = $format['breite'];
            }
        }




        if($what == 'nutzen') {
            if($seiten < $nutzen) {
                return $seiten;
            }
            return $nutzen;
        }
        if($what == 'nettobg') {
            return $nettobg;
        }
        if($what == 'zuschuss') {
            return $zuschuss;
        }

        if($what == 'bogen_hoehe') {
            return $bogen_hoehe;
        }

        if($what == 'ctp') {
            return $ctp;
        }

        if($what == 'bogen_breite') {
            return $bogen_breite;
        }
        return $price;

    }


    function getCalcTableValueUmschlag($what, $papierType1,$papierType2,$papierType3,$papierType4,$papierType5,$papierType6,$papierType7,$papierType8,$papierType9,$papierType10,$papierType11,$papierType12,$papierType13,$papierType14, $kostenDruckplatte, $druckplatteOption, $gammatur, $papierKosten, $umschlagen, $seiten, $auflage, $anzahlDruckfarben, $anzahlFarbwechsel, $anzahlDruckJeForm, $format_offen_breite, $format_offen_hoehe, $geiferRand, $bogenRaender, $umschlag_seiten_bedruckt) {

        $price = 9999999999;
        $nutzen = 9999999999;
        $nettobg = 9999999999;
        $zuschuss = 9999999999;
        $bogen_hoehe = 1000;
        $bogen_breite = 1000;

        $formate=array();
        if($papierType1) {
            $formate[] = array('type' => 1, 'breite' => 88, 'hoehe' => 63, 'umschlagen' => 0);
        }
        if($papierType1 && $umschlagen == 1) {
            $formate[] = array('type' => 2, 'breite' => 88, 'hoehe' => 63, 'umschlagen' => 1);
        }

        if($papierType2) {
            $formate[] = array('type' => 5, 'breite' => 100, 'hoehe' => 70, 'umschlagen' => 0);
        }
        if($papierType2 && $umschlagen == 1) {
            $formate[] = array('type' => 5, 'breite' => 100, 'hoehe' => 70, 'umschlagen' => 1);
        }

        if($papierType3) {
            $formate[] = array('type' => 3, 'breite' => 70, 'hoehe' => 50, 'umschlagen' => 0);
        }
        if($papierType3 && $umschlagen == 1) {
            $formate[] = array('type' => 3, 'breite' => 70, 'hoehe' => 50, 'umschlagen' => 1);
        }

        if($papierType4) {
            //$formate[] = array('type' => 7, 'breite' => 61, 'hoehe' => 43, 'umschlagen' => 0);
        }
        if($papierType4 && $umschlagen == 1) {
            //$formate[] = array('type' => 7, 'breite' => 61, 'hoehe' => 43, 'umschlagen' => 1);
        }

        if($papierType5) {
            //$formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 11.4, 'umschlagen' => 0);
        }
        if($papierType5 && $umschlagen == 1) {
            //$formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 11.4, 'umschlagen' => 1);
        }

        if($papierType6) {
            //$formate[] = array('type' => 7, 'breite' => 32.4, 'hoehe' => 22.9, 'umschlagen' => 0);
        }
        if($papierType6 && $umschlagen == 1) {
            //$formate[] = array('type' => 7, 'breite' => 32.4, 'hoehe' => 22.9, 'umschlagen' => 1);
        }

        if($papierType7) {
            //$formate[] = array('type' => 7, 'breite' => 35.3, 'hoehe' => 25, 'umschlagen' => 0);
        }
        if($papierType7 && $umschlagen == 1) {
            //$formate[] = array('type' => 7, 'breite' => 35.3, 'hoehe' => 25, 'umschlagen' => 1);
        }

        if($papierType8) {
            //$formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 16.2, 'umschlagen' => 0);
        }
        if($papierType8 && $umschlagen == 1) {
            //$formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 16.2, 'umschlagen' => 1);
        }

        if($papierType9) {
            //$formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 11, 'umschlagen' => 0);
        }
        if($papierType9 && $umschlagen == 1) {
            //$formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 11, 'umschlagen' => 1);
        }

        if($papierType10) {
            //$formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 22, 'umschlagen' => 0);
        }
        if($papierType10 && $umschlagen == 1) {
            //$formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 22, 'umschlagen' => 1);
        }

        if($papierType11) {
            //$formate[] = array('type' => 7, 'breite' => 92, 'hoehe' => 65, 'umschlagen' => 0);
        }
        if($papierType11 && $umschlagen == 1) {
            //$formate[] = array('type' => 7, 'breite' => 92, 'hoehe' => 65, 'umschlagen' => 1);
        }

        if($papierType12) {
            //$formate[] = array('type' => 7, 'breite' => 64, 'hoehe' => 45, 'umschlagen' => 0);
        }
        if($papierType12 && $umschlagen == 1) {
            //$formate[] = array('type' => 7, 'breite' => 64, 'hoehe' => 45, 'umschlagen' => 1);
        }

        if($papierType13) {
            //$formate[] = array('type' => 7, 'breite' => 44, 'hoehe' => 31.5, 'umschlagen' => 0);
        }
        if($papierType13 && ($weiterVerarbeitung == 2 || $weiterVerarbeitung == 3)) {
            //$formate[] = array('type' => 7, 'breite' => 44, 'hoehe' => 31.5, 'umschlagen' => 1);
        }
        if($papierType14) {
            $formate[] = array('type' => 7, 'breite' => 102, 'hoehe' => 72, 'umschlagen' => 0);
        }
        if($papierType14 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 102, 'hoehe' => 72, 'umschlagen' => 1);
        }

        //echo "<br/><br/>";

        foreach($formate as $key => $format) {


            if($format['umschlagen'] == 0) {
                $calc1=floor((($format['breite']-$bogenRaender)/($format_offen_breite)))*floor((($format['hoehe']-$geiferRand))/$format_offen_hoehe);
                $calc2=floor((($format['hoehe']-$bogenRaender)/($format_offen_breite)))*floor((($format['breite']-$geiferRand))/$format_offen_hoehe);

                $ctp1 = $umschlag_seiten_bedruckt*$anzahlDruckfarben+$druckplatteOption;

            }else{
                $calc1=floor((($format['breite']-$bogenRaender)/($format_offen_breite)))*floor((($format['hoehe']-$geiferRand))/$format_offen_hoehe);
                $calc2=floor((($format['hoehe']-$bogenRaender)/($format_offen_breite)))*floor((($format['breite']-$geiferRand))/$format_offen_hoehe);

                $ctp1 = $anzahlDruckfarben+$druckplatteOption;
            }

            $nutzen_calc = ($calc1 > $calc2)? $calc1: $calc2;

            if($format['umschlagen'] == 1) {
                $nutzen_calc = floor($nutzen_calc/2)*2;
            }

            if($nutzen_calc == 0) {
                continue;
            }

            $calc1 = 150;

            $calc2 = ($anzahlDruckJeForm*$anzahlDruckfarben*100)+($auflage/$nutzen_calc*0.02);

            $zuschussCalc = ($calc1 > $calc2)? $calc1 : $calc2;

            //var_dump($nutzen_calc);

            $nettoBedruckGesamt = $auflage/$nutzen_calc;

            $gramGesch = (100-sqrt((150-$gammatur)*(150-$gammatur))/5)/100;

            $fortdruckGeschwindigkeit = 2500+$gramGesch*(log10($auflage*$auflage)*700);

            $kba1 = 140*($zuschussCalc+$nettoBedruckGesamt)/$fortdruckGeschwindigkeit*$umschlag_seiten_bedruckt*(ceil($anzahlDruckfarben/5));
            //var_dump($zuschussCalc.' - '.$nettoBedruckGesamt.' - '.$fortdruckGeschwindigkeit.' - '.$umschlag_seiten_bedruckt.' - '.$anzahlDruckfarben);
            $price_calc = ($format['hoehe']*$format['breite']*($nettoBedruckGesamt+$zuschussCalc)*$gammatur*$papierKosten/1000000000)+($kostenDruckplatte*$ctp1)+$kba1;

            //var_dump($format['hoehe'].' - '.$format['breite'].' - '.$ctp1.' - '.$kba1.' - '.$price_calc.' - '.$price);
            //echo "<br/>";


            if($price_calc < $price) {
                $price = $price_calc;
                $nutzen = $nutzen_calc;
                $zuschuss = $zuschussCalc;
                $nettobg = $nettoBedruckGesamt;
                $bogen_hoehe = $format['hoehe'];
                $bogen_breite = $format['breite'];
            }
        }




        if($what == 'nutzen') {
            return $nutzen;
        }
        if($what == 'nettobg') {
            return $nettobg;
        }
        if($what == 'zuschuss') {
            return $zuschuss;
        }

        if($what == 'bogen_hoehe') {
            return $bogen_hoehe;
        }

        if($what == 'bogen_breite') {
            return $bogen_breite;
        }
        return $price;

    }

    function getPurHelpKlebe($what, $var) {

        $fix = 222;
        $per = 89;

        if($var > 7) {
            $fix = 233;
            $per = 102;
        }
        if($var > 12) {
            $fix = 244;
            $per = 114;
        }
        if($var > 16) {
            $fix = 366;
            $per = 228;
        }

        if($what=='fix') {
            return $fix;
        }
        return $per;
    }

    function getPurHelpFalz($what, $var) {

        $fix = 10.30;
        $per = 7.05;

        if($var == 6) {
            $fix = 15.40;
            $per = 8.20;
        }
        if($var == 8) {
            $fix = 15.40;
            $per = 8.40;
        }
        if($var == 12) {
            $fix = 20.50;
            $per = 9.70;
        }
        if($var == 16) {
            $fix = 31.50;
            $per = 9.50;
        }
        if($var == 24) {
            $fix = 41.00;
            $per = 10.70;
        }
        if($var == 32) {
            $fix = 51.20;
            $per = 11.90;
        }
        if($var == 48) {
            $fix = 102.30;
            $per = 12.90;
        }

        if($what=='fix') {
            return $fix;
        }
        return $per;
    }


    function getCalcTableValueOffsetPlano($what, $papierType1,$papierType2,$papierType3,$papierType4,$papierType5,$papierType6,$papierType7,$papierType8,$papierType9,$papierType10,$papierType11,$papierType12,$papierType13,$papierType14, $kostenDruckplatte, $druckplatteOption, $gammatur, $papierKosten, $umschlagen, $seiten, $auflage, $anzahlDruckfarben, $anzahlFarbwechsel, $anzahlSeiten, $format_offen_breite, $format_offen_hoehe, $drucklack, $papier_vs_rs_unterschiedlich) {
        //var_dump( func_get_args());
        $price = 9999999999;
        $nutzen = 9999999999;
        $nettobg = 9999999999;
        $zuschuss = 9999999999;
        $bogen_hoehe = 1000;
        $bogen_breite = 1000;
        $ctp = 999999999;
        $druckgaenge = 99999999;
        $fortdruckmaschine = 0;
        $stundensatz = 99999999;
        $maschine = 'kba';

        $formate=array();
        if($papierType1) {
            $formate[] = array('type' => 1, 'breite' => 88, 'hoehe' => 63, 'umschlagen' => 0);
        }
        if($papierType1 && $umschlagen == 1) {
            $formate[] = array('type' => 2, 'breite' => 88, 'hoehe' => 63, 'umschlagen' => 1);
        }

        if($papierType2) {
            $formate[] = array('type' => 5, 'breite' => 100, 'hoehe' => 70, 'umschlagen' => 0);
        }
        if($papierType2 && $umschlagen == 1) {
            $formate[] = array('type' => 5, 'breite' => 100, 'hoehe' => 70, 'umschlagen' => 1);
        }

        if($papierType3) {
            $formate[] = array('type' => 3, 'breite' => 70, 'hoehe' => 50, 'umschlagen' => 0);
        }
        if($papierType3 && $umschlagen == 1) {
            $formate[] = array('type' => 3, 'breite' => 70, 'hoehe' => 50, 'umschlagen' => 1);
        }

        if($papierType4) {
            $formate[] = array('type' => 7, 'breite' => 61, 'hoehe' => 43, 'umschlagen' => 0);
        }
        if($papierType4 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 61, 'hoehe' => 43, 'umschlagen' => 1);
        }

        if($papierType5) {
            $formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 11.4, 'umschlagen' => 0);
        }
        if($papierType5 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 11.4, 'umschlagen' => 1);
        }

        if($papierType6) {
            $formate[] = array('type' => 7, 'breite' => 32.4, 'hoehe' => 22.9, 'umschlagen' => 0);
        }
        if($papierType6 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 32.4, 'hoehe' => 22.9, 'umschlagen' => 1);
        }

        if($papierType7) {
            $formate[] = array('type' => 7, 'breite' => 35.3, 'hoehe' => 25, 'umschlagen' => 0);
        }
        if($papierType7 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 35.3, 'hoehe' => 25, 'umschlagen' => 1);
        }

        if($papierType8) {
            $formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 16.2, 'umschlagen' => 0);
        }
        if($papierType8 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 22.9, 'hoehe' => 16.2, 'umschlagen' => 1);
        }

        if($papierType9) {
            $formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 11, 'umschlagen' => 0);
        }
        if($papierType9 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 11, 'umschlagen' => 1);
        }

        if($papierType10) {
            $formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 22, 'umschlagen' => 0);
        }
        if($papierType10 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 22, 'hoehe' => 22, 'umschlagen' => 1);
        }

        if($papierType11) {
            $formate[] = array('type' => 7, 'breite' => 92, 'hoehe' => 65, 'umschlagen' => 0);
        }
        if($papierType11 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 92, 'hoehe' => 65, 'umschlagen' => 1);
        }

        if($papierType12) {
            $formate[] = array('type' => 7, 'breite' => 64, 'hoehe' => 45, 'umschlagen' => 0);
        }
        if($papierType12 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 64, 'hoehe' => 45, 'umschlagen' => 1);
        }

        if($papierType13) {
            $formate[] = array('type' => 7, 'breite' => 44, 'hoehe' => 31.5, 'umschlagen' => 0);
        }
        if($papierType13 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 44, 'hoehe' => 31.5, 'umschlagen' => 1);
        }

        if($papierType14) {
            $formate[] = array('type' => 7, 'breite' => 102, 'hoehe' => 72, 'umschlagen' => 0);
        }
        if($papierType14 && $umschlagen == 1) {
            $formate[] = array('type' => 7, 'breite' => 102, 'hoehe' => 72, 'umschlagen' => 1);
        }

        //echo "<br/><br/>";

        foreach($formate as $key => $format) {

            $geiferRand = getGeiferRand($format);
            $bogenRaender = getBogenRaender($format);


            if($format['umschlagen'] == 0) {

                $nutzen_calc = max(
                                ((floor(($format['breite']-$bogenRaender)/$format_offen_breite))*(floor(($format['hoehe']-$geiferRand)/$format_offen_hoehe))),
                                ((floor(($format['breite']-$bogenRaender)/$format_offen_hoehe)*(floor(($format['hoehe']-$geiferRand)/$format_offen_breite))))
                                );

            }else{
                $nutzen_calc= 2*floor((max(
                                        ((floor(($format['breite']-$bogenRaender)/$format_offen_breite))*(floor(($format['hoehe']-$geiferRand)/$format_offen_hoehe))),
                                        ((floor(($format['breite']-$bogenRaender)/$format_offen_hoehe)*(floor(($format['hoehe']-$geiferRand)/$format_offen_breite))))
                                ))/2);


            }


            if($anzahlSeiten==2 && $papier_vs_rs_unterschiedlich==0 && $nutzen_calc > 1 && $format['umschlagen'] == 1) {
                $ctp1 = ($anzahlDruckfarben+$drucklack)*$anzahlSeiten/2;
            }else{
                $ctp1 = ($anzahlDruckfarben+$drucklack)*$anzahlSeiten;
            }

            $maschine1 = getMaschine($format['breite'], $format['hoehe']);

            if(getMaschine($format['breite'], $format['hoehe']) == "gto") {
                $druckgaenge1 = $anzahlSeiten*($anzahlDruckfarben+$drucklack);
                $fortdruckmaschine1 = 0;
                $stundensatz1 = 40;
                $anzahlFarb1 = max($anzahlFarbwechsel, $anzahlDruckfarben-1);
            }else{
                $druckgaenge1 = ceil(($anzahlDruckfarben+$drucklack)/5)*$anzahlSeiten;
                $fortdruckmaschine1 = 4500;
                $stundensatz1 = 140;
                $anzahlFarb1 = $anzahlFarbwechsel;
            }

            $zuschussCalc = intval(max(200,floor(($anzahlDruckfarben*100)+($auflage/$nutzen_calc*0.02))));

            $fortdruckGeschwindigkeit = (log10($auflage*$auflage)*900)*(100-sqrt((150-$gammatur)*(150-$gammatur))/5)/100;

            $nettoBedruckGesamt = (round($auflage/$nutzen_calc,0));
            $druckkosten = $stundensatz1*((($zuschussCalc+$nettoBedruckGesamt)/$fortdruckGeschwindigkeit*$druckgaenge1)+($ctp1*7/60)+($anzahlFarb1*45/60));

            //var_dump($maschine1.'-'.$stundensatz1.'-'.$zuschussCalc.'-'.$nettoBedruckGesamt.'-'.$fortdruckGeschwindigkeit.'-'.$druckgaenge1.'-'.$ctp1.'-'.$anzahlFarb1);
            //echo "<br/>";

            $price_calc = (($nettoBedruckGesamt+$zuschussCalc)*($format['hoehe']*$format['breite']*$gammatur*$papierKosten/1000000000)+($kostenDruckplatte*$ctp1))+$druckkosten;

            if($price_calc < $price && $nettoBedruckGesamt != 0) {
                $price = $price_calc;
                $nutzen = $nutzen_calc;
                $ctp = $ctp1;
                $zuschuss = $zuschussCalc;
                $nettobg = $nettoBedruckGesamt;
                $bogen_hoehe = $format['hoehe'];
                $bogen_breite = $format['breite'];
                $druckgaenge = $druckgaenge1;
                $fortdruckmaschine = $fortdruckmaschine1;
                $stundensatz = $stundensatz1;
                $maschine = $maschine1;
            }
            //var_dump($format['hoehe'].'-'.$format['breite'].'-'.$ctp1.'-'.($zuschussCalc+$nettoBedruckGesamt).'-'.$druckkosten.'-'.$price_calc.'-'.$price);
            //echo "<br/>";
        }




        if($what == 'nutzen') {
            return $nutzen;
        }
        if($what == 'nettobg') {
            return $nettobg;
        }
        if($what == 'maschine') {
            return $maschine;
        }
        if($what == 'zuschuss') {
            return $zuschuss;
        }
        if($what == 'fortdruckmaschine') {
            return $fortdruckmaschine;
        }

        if($what == 'maschine_stundensatz') {
            return $stundensatz;
        }

        if($what == 'bogen_hoehe') {
            return $bogen_hoehe;
        }

        if($what == 'ctp') {
            return $ctp;
        }

        if($what == 'druckgaenge') {
            return $druckgaenge;
        }

        if($what == 'bogen_breite') {
            return $bogen_breite;
        }
        return $price;

    }

    function getMaschine($breite, $hoehe) {
        if($breite > 49 || $hoehe > 49) {

            return 'kba';
        }

        return 'gto';
    }

    function getGeiferRand($format) {
        $maschine = getMaschine($format['breite'], $format['hoehe']);

        if($maschine == 'kba') {
            return 2.3;
        }

        return 1.4;
    }

    function getBogenRaender($format) {
        $maschine = getMaschine($format['breite'], $format['hoehe']);

        if($maschine == 'kba') {
            return 1.5;
        }

        return 0.8;
    }

    function getMaschineKosten($format) {
        $maschine = getMaschine($format['breite'], $format['hoehe']);

        if($maschine == 'kba') {
            return 140;
        }

        return 40;
    }


}