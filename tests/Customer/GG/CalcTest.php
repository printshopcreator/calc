<?php
namespace PSC\Library\Calc\Tests\Customer\GG;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));


    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testPrice(): void
    {
        $this->engine->calc();
        $this->assertSame(163.75, $this->engine->getPrice());
    }

    public function testColorSelect(): void
    {
        $this->engine->setVariable('farbe_1_v', '3');
        $this->engine->setVariable('farbe_1_v_wert_pantone', '128');
        $this->engine->setVariable('farbe_2_v', '3');
        $this->engine->setVariable('farbe_2_v_wert_pantone', '7555');
        $this->engine->calc();
        $this->assertSame(175.75, $this->engine->getPrice());
        $this->assertSame($this->engine->getArticle()->getOptionById('farbe_1_v_wert_pantone')->getRawValue(), '128');
        $this->assertSame($this->engine->getArticle()->getOptionById('farbe_2_v_wert_pantone')->getRawValue(), '7555');
    }

}
