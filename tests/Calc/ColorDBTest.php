<?php
namespace PSC\Library\Calc\Tests\Calc;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Base;
use PSC\Library\Calc\PaperContainer\Container;

class ColorDBTest extends TestCase
{
    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $this->engine = new Engine(new Container());
        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/Calc/colors.xml'));
    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfOptionsNotValid()
    {
        $this->engine->calc();

        /** @var Article $article */
        $article = $this->engine->getArticle();

        /** @var Base $option */
        $option = $article->getOptionById('colorpantone');

        $this->assertTrue($option->isValid());
        $this->assertSame('142', $option->getSelectedOption()->getId());

        $option = $article->getOptionById('colorhks');

        $this->assertTrue($option->isValid());
        $this->assertSame('78', $option->getSelectedOption()->getId());
    }

}
