<?php
namespace PSC\Library\Calc\Tests\Calc;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Base;
use PSC\Library\Calc\PaperContainer\Container;

class PriceMinTest extends TestCase
{
    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $this->engine = new Engine(new Container());
        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/Calc/price_min.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfArticleCountIsCorrect()
    {
        $this->assertEquals(1, $this->engine->getArticles()->Count());
    }

    public function testIfParserGetMinPrice()
    {
        $this->engine->calc();

        $this->assertEquals(2500, $this->engine->getPrice());
        $this->engine->setTax(19);
        $this->assertEquals(475, $this->engine->getTaxPrice());
        $this->assertEquals(2975, $this->engine->getCompletePrice());
    }

    public function testIfCalcReturnsGrenzeWithFormular()
    {
        $this->engine->setVariables([
            'gebwert' => 1000000,
            'maschwertjuengerfuenf' => 400000,
            'maschwertaelterfuenf' => 400000,
            'allegereate' => 400000,
            'betriebsunterbrechung' => 400000,
            'umsatzjahrtransport' => 300000,
            'umsatzjahrbh' => 400000]);
        $this->engine->calc();

        $this->assertEquals(2721.40, $this->engine->getPrice());
        $this->engine->setTax(19);
        $this->assertEquals(517.07, $this->engine->getTaxPrice());
        $this->assertEquals(3238.47, $this->engine->getCompletePrice());
    }

}
