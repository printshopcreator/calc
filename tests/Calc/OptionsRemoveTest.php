<?php
namespace PSC\Library\Calc\Tests\Calc;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Base;
use PSC\Library\Calc\PaperContainer\Container;

class OptionsRemoveTest extends TestCase
{
    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $this->engine = new Engine(new Container());
        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/Calc/option_remove.xml'));
    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfArticleCountIsCorrect()
    {
        $this->assertEquals(1, $this->engine->getArticles()->Count());
    }

    public function testIfParserGetArticleCorrect()
    {
        $article = $this->engine->getArticle();
        $this->assertEquals('test1', $article->getName());

        $this->assertEquals(2, count($article->getOptions()));
    }

    public function testIfOptionsNotValid()
    {
        $this->engine->setParameters("");
        $this->engine->setFormulas("");
        $this->engine->setVariables([]);
        $this->engine->calc('test1');

        /** @var Article $article */
        $article = $this->engine->getArticle();

        /** @var Base $option */
        $option = $article->getOptionById('gebwert');

        $this->assertTrue($option->isValid());

        $this->engine->setVariables(['gebyn' => 2]);
        $this->engine->calc('test1');

        $this->assertEquals("Nein", $article->getOptionById('gebyn')->getValue());

        $option = $article->getOptionById('gebwert');

        $this->assertFalse($option->isValid());
    }

}
