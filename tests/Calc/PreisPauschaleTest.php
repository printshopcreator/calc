<?php
namespace PSC\Library\Calc\Tests\Calc;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Base;
use PSC\Library\Calc\PaperContainer\Container;

class PreisPauschaleTest extends TestCase
{
    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $this->engine = new Engine(new Container());
        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/Calc/pauschale_preis.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfArticleCountIsCorrect()
    {
        $this->assertEquals(1, $this->engine->getArticles()->Count());
    }

    public function testPreisPauschaleCalc()
    {
        $this->engine->calc();

        $this->assertEquals(110, $this->engine->getPrice());
        $this->engine->setVariables(['auflage' => 50]);
        $this->assertEquals(270, $this->engine->getPrice());
        $this->engine->setVariables(['auflage' => 250]);
        $this->assertEquals(0, $this->engine->getPrice());

    }

}
