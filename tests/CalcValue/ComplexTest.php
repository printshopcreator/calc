<?php
namespace PSC\Library\Calc\Tests\CalcValue;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\PaperDbSelect;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\PaperContainer\Container;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class ComplexTest extends TestCase
{
    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ .'/../TestFiles/CalcValue/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperRepository($repository);
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/CalcValue/complex.xml'));
        $this->engine->setFormulas(file_get_contents(__DIR__.'/../TestFiles/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__.'/../TestFiles/parameters.txt'));
    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfParseValue(): void
    {
        $this->assertEquals(28.54, $this->engine->getPrice());
    }

    public function testIfPaperGrammaturValue(): void
    {
        $this->assertInstanceOf(PaperDbSelect::Class, $this->engine->getArticle()->getOptionById('papier'));

        $this->assertEquals("INM115", $this->engine->getArticle()->getOptionById('papier')->getSelectedOption()->getId());
        $this->assertEquals(0.104, $this->engine->getArticle()->getOptionById('papier')->getSelectedOption()->getPaper()->getVolume());
    }
}
