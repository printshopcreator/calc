<?php
namespace PSC\Library\Calc\Tests\CalcValue;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer\Container;

class SimpleTest extends TestCase
{
    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $this->engine = new Engine(new Container());
        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/CalcValue/simple.xml'));
    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfParseValue(): void
    {
        $this->assertEquals(20, $this->engine->getPrice());
    }
}
