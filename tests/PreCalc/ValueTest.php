<?php
namespace PSC\Library\Calc\Tests\PreCalc;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\PreCalc\Parser\Value;
use PSC\Library\Calc\PreCalc\Value as PSCValue;

class ValueTest extends TestCase
{
    public function testIfCorrectType()
    {
        $parser = new Value(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/PreCalc/value.xml')));
        
        $element = $parser->parse();

        $this->assertInstanceOf(PSCValue::class, $element);
    }

    public function testIfCorret()
    {
        $parser = new Value(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/PreCalc/value.xml')));
        
        $element = $parser->parse();
        $this->assertSame("auflage", $element->getKey());
        $this->assertSame("100", $element->getValue());
    }
    
}
