<?php
namespace PSC\Library\Calc\Tests\PreCalc;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\PreCalc\Parser\Variant;
use PSC\Library\Calc\PreCalc\Variant as PSCVariant;

class ParseVariantTest extends TestCase
{
    public function testIfCorrectType()
    {
        $parser = new Variant(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/PreCalc/variant.xml')));
        
        $element = $parser->parse();

        $this->assertInstanceOf(PSCVariant::class, $element);
    }
    
    public function testIfNameIsCorrect()
    {
        $parser = new Variant(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/PreCalc/variant.xml')));
        
        $element = $parser->parse();

        $this->assertSame("100 stk", $element->getName());
    }

}
