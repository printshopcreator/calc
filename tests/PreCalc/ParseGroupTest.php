<?php
namespace PSC\Library\Calc\Tests\PreCalc;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\PreCalc\Group as PSCGroup;
use PSC\Library\Calc\PreCalc\Parser\Group;

class ParseGroupTest extends TestCase
{
    public function testIfCorrectType()
    {
        $parser = new Group(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/PreCalc/group.xml')));
        
        $element = $parser->parse();

        $this->assertInstanceOf(PSCGroup::class, $element);
    }
 
    public function testIfCountCorrect()
    {
        $parser = new Group(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/PreCalc/group.xml')));
        
        $element = $parser->parse();

        $this->assertSame(4, count($element->getVariants()));
        $this->assertSame("ohne Lochbohrung", $element->getName());
    }
    
}
