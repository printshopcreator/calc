<?php
namespace PSC\Library\Calc\Tests\PreCalc;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\PreCalc\Parser\PreCalc;
use PSC\Library\Calc\PreCalc\PreCalc as PSCPreCalc;

class ParseTest extends TestCase
{
    public function testIfCorrectType()
    {
        $parser = new PreCalc(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/PreCalc/precalc.xml')));
        
        $element = $parser->parse();

        $this->assertInstanceOf(PSCPreCalc::class, $element);
    }
        
    public function testIfCountIsCorrect()
    {
        $parser = new PreCalc(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/PreCalc/precalc.xml')));
        
        $element = $parser->parse();

        $this->assertSame(2, count($element->getGroups()));
        $this->assertSame(4, count($element->getGroups()[0]->getVariants()));
    }

}
