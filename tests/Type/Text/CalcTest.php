<?php
namespace PSC\Library\Calc\Tests\Type\Text;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;
use function Symfony\Component\String\s;

class CalcTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testTextGrenzen(): void
    {
        $this->engine->calc();
        self::assertFalse($this->engine->getArticle()->getOptionById('text_1')->isValid());
        self::assertTrue($this->engine->getArticle()->getOptionById('text_2')->isValid());
    }

    public function testTextGrenzenChanges(): void
    {
        $this->engine->setVariable('auflage', 1000);
        $this->engine->calc();
        self::assertTrue($this->engine->getArticle()->getOptionById('text_1')->isValid());
        self::assertFalse($this->engine->getArticle()->getOptionById('text_2')->isValid());
    }
}
