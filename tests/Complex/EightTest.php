<?php
namespace PSC\Library\Calc\Tests\Complex;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class EightTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/Complex/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__.'/../TestFiles/Complex/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__.'/../TestFiles/Complex/parameters.txt'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/Complex/weight.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfWeightIsCorrect(): void
    {
        $this->assertEquals(25, $this->engine->getWeight());
    }

    public function testIfSingleWeightIsCorrect(): void
    {
        $this->assertEquals(2.5, $this->engine->getWeightSingle());
    }

    public function testIfDefaultPriceIsOk(): void
    {
        $this->assertEquals(20, $this->engine->getPrice());
    }
}
