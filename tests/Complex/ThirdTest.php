<?php
namespace PSC\Library\Calc\Tests\Complex;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class ThirdTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/Complex2/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__.'/../TestFiles/Complex2/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__.'/../TestFiles/Complex2/parameters.txt'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/Complex2/2.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfArticleCountIsCorrect(): void
    {
        $this->assertEquals(1, $this->engine->getArticles()->Count());
    }

    public function testIfDefaultPriceIsOk(): void
    {
        $this->engine->setVariable('auflage', 1500);
        $this->engine->setVariable('druckfarben_inhalt', 2);
        $this->assertEquals(152.00 , $this->engine->getPrice());

    }
}
