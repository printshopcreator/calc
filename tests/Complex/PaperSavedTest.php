<?php
namespace PSC\Library\Calc\Tests\Complex;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class PaperSavedTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/../TestFiles/Complex/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__.'/../TestFiles/Complex/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__.'/../TestFiles/Complex/parameters.txt'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/Complex/1.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfArticleCountIsCorrect(): void
    {
        $this->assertEquals(1, $this->engine->getArticles()->Count());
    }

    public function testIfDefaultPriceIsOk(): void
    {
        $this->engine->setVariable('papier', 'INM137');

        $this->engine->setSavedCalcReferences([ "papier" =>
            [ "id" => "5039", "install_id" => "1", "uuid" => "0001-00000000-4bfaf9c3-7158-bce189cb",
            "art_nr" => "INM137", "description_1" => "Bilderdruck matt 250 gr",
            "description_2" => "Bilderdruck matt 135 gr", "grammatur" => "135", "preis" => "28",
            "offset_fix" => "0", "offset_var" => "0", "digital_fix" => "0", "digital_var" => "0", "volume" => "0.118" ] ]);

        $this->assertEquals(162.19, $this->engine->getPrice());

        $this->assertEquals("Bilderdruck matt 250 gr", $this->engine->getArticle()->getOptionById('papier')->getValue());
    }
}
