<?php
namespace PSC\Library\Calc\Tests\Contact\AccountType;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testPriceWithout(): void
    {
        self::assertSame(0.0, $this->engine->getPrice());
    }

    public function testPrice(): void
    {
        $this->engine->setVariable('contact.accountType', 1);
        self::assertSame(100.0, $this->engine->getPrice());
    }

    public function testPriceCompany(): void
    {
        $this->engine->setVariable('contact.accountType', 2);
        self::assertSame(80.0, $this->engine->getPrice());
    }

    public function testPriceAsso(): void
    {
        $this->engine->setVariable('contact.accountType', 3);
        self::assertSame(50.0, $this->engine->getPrice());
    }
}
