<?php
namespace PSC\Library\Calc\Tests\Contact\Account;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalcTest extends TestCase
{

    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/papierContainer.xml')));

        $this->engine = new Engine();
        $this->engine->setPaperContainer($paperContainer);
        $this->engine->setPaperRepository($repository);
        $this->engine->setFormulas(file_get_contents(__DIR__ . '/formels.txt'));
        $this->engine->setParameters(file_get_contents(__DIR__ . '/parameters.txt'));
        $this->engine->setTemplates(file_get_contents(__DIR__ . '/calcTemplates.xml'));

        $this->engine->loadString(file_get_contents(__DIR__ . '/calc.xml'));

    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testPriceDefault(): void
    {
        self::assertSame(4.5, $this->engine->getPrice());
    }

    public function testPrice1(): void
    {
        $this->engine->setVariable('contact.account', 12);
        self::assertSame(4.5, $this->engine->getPrice());
    }

    public function testPriceAccountNotExists(): void
    {
        $this->engine->setVariable('contact.account', 3242);
        self::assertSame(4.5, $this->engine->getPrice());
    }

    public function testPrice123(): void
    {
        $this->engine->setVariable('contact.account', 123);
        self::assertSame(5.4, $this->engine->getPrice());
    }

    public function testPrice334(): void
    {
        $this->engine->setVariable('contact.account', 334);
        self::assertSame(2.25, $this->engine->getPrice());
    }
}
