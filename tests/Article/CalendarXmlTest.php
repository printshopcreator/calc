<?php
namespace PSC\Library\Calc\Tests\Article;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\PaperContainer\Container;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class CalendarXmlTest extends TestCase
{
    public function testIfArticleCountInXmlCorrect()
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ .'/../TestFiles/Option/Select/papierContainer.xml')));

        $parser = new Engine();
        $parser->setPaperRepository($repository);
        $parser->setPaperContainer($paperContainer);
        $this->assertTrue($parser->loadString(file_get_contents(__DIR__ .'/../TestFiles/General/calendar.xml')));

        $this->assertEquals(1, $parser->getArticles()->Count());
    }

    public function testGetArticleByName()
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ .'/../TestFiles/Option/Select/papierContainer.xml')));

        $parser = new Engine();
        $parser->setPaperRepository($repository);
        $parser->setPaperContainer($paperContainer);
        $parser->loadString(file_get_contents(__DIR__ .'/../TestFiles/General/calendar.xml'));

        /** @var Article $article */
        $article = $parser->getArticle();
        $this->assertInstanceOf('PSC\Library\Calc\Article', $article);
        $this->assertEquals('Kalender', $article->getName());
    }

    public function testGetOptionById()
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ .'/../TestFiles/Option/Select/papierContainer.xml')));

        $parser = new Engine();
        $parser->setPaperRepository($repository);
        $parser->setPaperContainer($paperContainer);
        $parser->loadString(file_get_contents(__DIR__ .'/../TestFiles/General/calendar.xml'));
        /** @var Article $article */
        $article = $parser->getArticle();

        /** @var Select $option */
        $option = $article->getOptionById('size');

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Select', $option);

    }

    public function testGetPrintableValuesFromSavedParamsWithCleaning()
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ .'/../TestFiles/Option/Select/papierContainer.xml')));

        $parser = new Engine();
        $parser->setPaperRepository($repository);
        $parser->setPaperContainer($paperContainer);
        $parser->loadString(file_get_contents(__DIR__ .'/../TestFiles/General/calendar.xml'));

        /** @var Article $article */
        $article = $parser->getArticle();

        $article->setParams(array(
            "motive" => 0,
            "motivepaid" => 0,
            "auflage" => "1",
            "size" => "a4h",
            "papier" => "bdm250",
            "seiten" => "13",
            "schutzfolie" => "glanz",
            "abschluss" => "mitkarton",
            "produktion" => "std",
            "hinweis" => "Produktionszeit + 3 Tage Versand",
            "datenformat" => "pdf",
            "musterdruck" => "keinmuster",
            "info" => "Textfeld zur freien Eingabe",
            "reload" => 0,
            "kalk_artikel" => "Kalender",
        ));

        /** @var Select $option */
        $option = $article->getOptionById('size');

        $this->assertEquals('a4h', $option->getRawValue());
        $this->assertEquals('DIN A4 Hochformat (297 x 210 mm)', $option->getValue());

        $option = $article->getOptionById('seiten');

        $this->assertEquals('13', $option->getRawValue());
        $this->assertEquals('13 Seiten Monatskalender mit Deckblatt', $option->getValue());

    }

    public function testGetOptionsForArticle()
    {
        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ .'/../TestFiles/Option/Select/papierContainer.xml')));

        $parser = new Engine();
        $parser->setPaperRepository($repository);
        $parser->setPaperContainer($paperContainer);
        $parser->loadString(file_get_contents(__DIR__ .'/../TestFiles/General/calendar.xml'));

        /** @var Article $article */
        $article = $parser->getArticle();

        $this->assertEquals(11, $article->getOptions()->count());
    }

}
