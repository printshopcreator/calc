<?php
namespace PSC\Library\Calc\Tests\Article;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer\Container;

class SimpleXmlTest extends TestCase
{
    public function testIfArticleCountInXmlCorrect()
    {
        $parser = new Engine(new Container());
        $this->assertTrue($parser->loadString(file_get_contents(__DIR__ .'/../TestFiles/General/simple.xml')));

        $this->assertEquals(1, $parser->getArticles()->Count());
    }

    public function testIfArticleCountInXmlCorrectWith2Article()
    {
        $parser = new Engine(new Container());
        $this->assertTrue($parser->loadString(file_get_contents(__DIR__ .'/../TestFiles/General/simple2articles.xml')));

        $this->assertEquals(2, $parser->getArticles()->Count());
    }

    public function testGetArticleByName()
    {
        $parser = new Engine(new Container());
        $parser->loadString(file_get_contents(__DIR__ .'/../TestFiles/General/simple2articles.xml'));

        $parser->setActiveArticle("Blocks A5 25blatt geleimt");
        $article = $parser->getArticle();
        $this->assertInstanceOf('PSC\Library\Calc\Article', $article);
        $this->assertEquals('Blocks A5 25blatt geleimt', $article->getName());
    }

}
