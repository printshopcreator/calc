<?php
namespace PSC\Library\Calc\Tests\Article;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer\Container;

class Complete1Test extends TestCase
{
    /** @var Engine */
    protected $engine = null;

    public function setUp(): void
    {
        $this->engine = new Engine(new Container());
        $this->engine->loadString(file_get_contents(__DIR__ . '/../TestFiles/General/complete1.xml'));
    }

    public function tearDown(): void
    {
        $this->engine = null;
    }

    public function testIfArticleCountIsCorrect()
    {
        $this->assertEquals(1, $this->engine->getArticles()->Count());
    }

    public function testIfParserGetArticleCorrect()
    {
        $article = $this->engine->getArticle();
        $this->assertEquals('test1', $article->getName());

        $this->assertEquals(17, count($article->getOptions()));
    }

    public function testIfCalcArticleCorrect()
    {
        $this->engine->setParameters("");
        $this->engine->setFormulas("");
        $this->engine->setVariables([]);
        $this->engine->calc('test1');
        $this->assertEquals(0, $this->engine->getPrice());

    }

    public function testIfCalcReturnsPrice()
    {
        $this->engine->setParameters("");
        $this->engine->setFormulas("");
        $this->engine->setVariables(['gebwert' => 20000]);
        $this->engine->calc('test1');
        $this->assertEquals(18.12, $this->engine->getPrice());
    }

    public function testIfCalcReturnsPriceWithEdge()
    {
        $this->engine->setParameters("");
        $this->engine->setFormulas("");
        $this->engine->setVariables(['gebyn' => 2, 'gebwert' => 20000]);
        $this->engine->calc('test1');
        $this->assertEquals(0, $this->engine->getPrice());
    }

    public function testIfCalcCompletePrice()
    {
        $this->engine->setParameters("");
        $this->engine->setFormulas("");
        $this->engine->setVariables([
            'gebwert' => 20000,
            'maschwertjuengerfuenf' => 12000,
            'maschwertaelterfuenf' => 13000,
            'allegereate' => 14000,
            'betriebsunterbrechung' => 15000,
            'umsatzjahrtransport' => 16000,
            'umsatzjahrbh' => 17000]);
        $this->engine->calc('test1');
        $this->assertEquals(82.09, $this->engine->getPrice());
    }
}
