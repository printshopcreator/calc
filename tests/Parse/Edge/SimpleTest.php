<?php
namespace PSC\Library\Calc\Tests\Parse\Edge;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\General\Parser\Edge;

class SimpleTest extends TestCase
{
    public function testIfOneValue()
    {
        $node = simplexml_load_string(file_get_contents(__DIR__ .'/../../TestFiles/Edges/simple.xml'));

        $edgeParser = new Edge($node->grenze[2]);

        /** @var \PSC\Library\Calc\General\Type\Edge $edge */
        $edge = $edgeParser->parse();
        $this->assertEquals(12, $edge->getPreis());
        $this->assertEquals(10, $edge->getPauschale());
        $this->assertEquals('$Vmaschwertjuengerfuenf$V/1000*0.900', $edge->getFormel());
        $this->assertEquals('$Vauflage$V', $edge->getCalcValue());
        $this->assertFalse($edge->isRegion());
        $this->assertEquals(34, $edge->getValues()[0]);

    }

    public function testIfRegionFrom()
    {
        $node = simplexml_load_string(file_get_contents(__DIR__ .'/../../TestFiles/Edges/simple.xml'));

        $edgeParser = new Edge($node->grenze[1]);

        /** @var \PSC\Library\Calc\General\Type\Edge $edge */
        $edge = $edgeParser->parse();
        $this->assertEquals(12, $edge->getPreis());
        $this->assertEquals(10, $edge->getPauschale());
        $this->assertEquals('$Vmaschwertjuengerfuenf$V/1000*0.900', $edge->getFormel());
        $this->assertEquals('$Vauflage$V', $edge->getCalcValue());
        $this->assertTrue($edge->isRegion());
        $this->assertEquals(101, $edge->getFrom());

    }

    public function testIfRegionFromTo()
    {
        $node = simplexml_load_string(file_get_contents(__DIR__ .'/../../TestFiles/Edges/simple.xml'));

        $edgeParser = new Edge($node->grenze[0]);

        /** @var \PSC\Library\Calc\General\Type\Edge $edge */
        $edge = $edgeParser->parse();
        $this->assertEquals(12, $edge->getPreis());
        $this->assertEquals(10, $edge->getPauschale());
        $this->assertEquals('$Vmaschwertjuengerfuenf$V/1000*0.900', $edge->getFormel());
        $this->assertEquals('$Vauflage$V', $edge->getCalcValue());
        $this->assertTrue($edge->isRegion());
        $this->assertEquals(1, $edge->getFrom());
        $this->assertEquals(100, $edge->getTo());

    }

    public function testIfCommaSeperated()
    {
        $node = simplexml_load_string(file_get_contents(__DIR__ .'/../../TestFiles/Edges/simple.xml'));

        $edgeParser = new Edge($node->grenze[3]);

        /** @var \PSC\Library\Calc\General\Type\Edge $edge */
        $edge = $edgeParser->parse();
        $this->assertEquals(12, $edge->getPreis());
        $this->assertEquals(10, $edge->getPauschale());
        $this->assertEquals('$Vmaschwertjuengerfuenf$V/1000*0.900', $edge->getFormel());
        $this->assertEquals('$Vauflage$V', $edge->getCalcValue());
        $this->assertFalse($edge->isRegion());
        $this->assertEquals([12,23], $edge->getValues());

    }

}
