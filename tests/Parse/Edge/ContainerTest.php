<?php
namespace PSC\Library\Calc\Tests\Parse\Edge;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\General\Parser\EdgeCollectionContainer;

class ContainerTest extends TestCase
{
    public function testIfLoadsCorrect()
    {
        $node = simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Edges/collections.xml'));

        $containerParser = new EdgeCollectionContainer($node);
        /** @var \PSC\Library\Calc\General\Type\EdgeCollectionContainer $container */
        $container = $containerParser->parse();

        $this->assertCount(2, $container);

        $this->assertCount(4, $container->getCollectionByName('gebyn'));
        $this->assertCount(2, $container->getCollectionByName('seiten'));

    }
}
