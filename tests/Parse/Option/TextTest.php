<?php
namespace PSC\Library\Calc\Tests\Option\Type;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Option\Parser;
use PSC\Library\Calc\Option\Type\Input;
use PSC\Library\Calc\PaperContainer\Container;

class TextTest extends TestCase
{
    public function testIfCorrectType()
    {
        $parser = new Parser();
        /** @var Parser\Input $obj */
        $obj = $parser->getOptByType(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/text.xml')));

        /** @var Input $element */
        $element = $obj->parse();

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Text', $element);
    }

    public function testIfCorrectAttributes()
    {
        $parser = new Parser();
        /** @var Parser\Input $obj */
        $obj = $parser->getOptByType(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/text.xml')));

        /** @var Input $element */
        $element = $obj->parse();

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Text', $element);
        $this->assertEquals('testtext', $element->getDefault());
        $this->assertEquals('t60', $element->getId());
        $this->assertEquals('', $element->getName());
    }

}
