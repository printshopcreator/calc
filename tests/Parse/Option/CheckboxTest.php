<?php
namespace PSC\Library\Calc\Tests\Option\Type;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Option\Parser;
use PSC\Library\Calc\Option\Type\Checkbox;
use PSC\Library\Calc\Option\Type\Input;
use PSC\Library\Calc\PaperContainer\Container;

class CheckboxTest extends TestCase
{
    public function testIfCorrectType()
    {
        $parser = new Parser();
        /** @var Parser\Checkbox $obj */
        $obj = $parser->getOptByType(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/checkbox.xml')));

        /** @var Checkbox $element */
        $element = $obj->parse();

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Checkbox', $element);
    }

}
