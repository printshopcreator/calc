<?php
namespace PSC\Library\Calc\Tests\Option\Type;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Option\Parser;
use PSC\Library\Calc\Option\Type\Input;
use PSC\Library\Calc\PaperContainer\Container;

class InputTest extends TestCase
{
    public function testIfCorrectType()
    {
        $parser = new Parser();
        /** @var Parser\Input $obj */
        $obj = $parser->getOptByType(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/input.xml')));

        /** @var Input $element */
        $element = $obj->parse();

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Input', $element);
    }

    public function testIfCorrectAttributes()
    {
        $parser = new Parser();
        /** @var Parser\Input $obj */
        $obj = $parser->getOptByType(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/input.xml')));

        /** @var Input $element */
        $element = $obj->parse();

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Input', $element);
        $this->assertTrue($element->isRequire());
        $this->assertEquals(5, $element->getDefault());
        $this->assertEquals('auflage', $element->getId());
        $this->assertEquals('Auflage', $element->getName());
    }

}
