<?php
namespace PSC\Library\Calc\Tests\Option\Type;

use PHPUnit\Framework\TestCase;
use PSC\Library\Calc\Option\Parser;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\PaperContainer\Container;
use PSC\Library\Calc\Tests\Mock\PaperRepostory;

class SelectTest extends TestCase
{
    public function testIfCorrectType()
    {
        $parser = new Parser();
        /** @var Parser\Select $obj */
        $obj = $parser->getOptByType(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/select.xml')));
        $obj->setPaperContainer(new PaperContainer());

        /** @var Select $element */
        $element = $obj->parse();

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Select', $element);
    }

    public function testIfCorrectAttributes()
    {
        $parser = new Parser();
        /** @var Select $obj */
        $obj = $parser->getOptByType(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/select.xml')));
        $obj->setPaperContainer(new PaperContainer());

        /** @var Select $element */
        $element = $obj->parse();

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Select', $element);
        $this->assertTrue($element->isRequire());
        $this->assertEquals('a4h', $element->getDefault());
        $this->assertEquals('size', $element->getId());
        $this->assertEquals('Wählen Sie eine Größe', $element->getName());
    }

    public function testIfPaperContainerReturnsCorrectItems()
    {
        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/Select/papierContainer.xml')));

        $this->assertEquals(2, $paperContainer->getContainer()->count());

        /** @var PaperContainer\Container $container */
        $container = $paperContainer->getContainerById('test1');

        $this->assertInstanceOf('PSC\Library\Calc\PaperContainer\Container', $container);

        $this->assertEquals(5, $container->getItems()->count());

        $container = $paperContainer->getContainerById('test2');

        $this->assertInstanceOf('PSC\Library\Calc\PaperContainer\Container', $container);

        $this->assertEquals(8, $container->getItems()->count());
    }

    public function testIfSelectWithPaperContainerModeReturnsCorrectOpt()
    {

        $repository = new PaperRepostory();

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/Select/papierContainer.xml')));

        $parser = new Parser();
        /** @var Parser\Select $obj */
        $obj = $parser->getOptByType(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/Select/selectPaperDB.xml')));
        $obj->setPaperContainer($paperContainer);
        $obj->setPaperRepository($repository);

        /** @var Select $element */
        $element = $obj->parse();

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Select', $element);

        $this->assertEquals(5, $element->getOptions()->count());
    }

    public function testIfSelectWithColorModePantoneReturnsCorrectOpt()
    {
        $parser = new Parser();
        /** @var Parser\Select $obj */
        $obj = $parser->getOptByType(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/Select/selectColorDBPantone.xml')));

        /** @var Select $element */
        $element = $obj->parse();

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Select', $element);
        $this->assertEquals(1354, $element->getOptions()->count());
    }

    public function testIfSelectWithColorModeReturnsCorrectOpt()
    {
        $parser = new Parser();
        /** @var Parser\Select $obj */
        $obj = $parser->getOptByType(simplexml_load_string(file_get_contents(__DIR__ . '/../../TestFiles/Option/Select/selectColorDBHKS.xml')));

        /** @var Select $element */
        $element = $obj->parse();

        $this->assertInstanceOf('PSC\Library\Calc\Option\Type\Select', $element);
        $this->assertEquals(86, $element->getOptions()->count());
    }

}
