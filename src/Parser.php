<?php
namespace PSC\Library\Calc;


use Doctrine\Persistence\ObjectRepository;
use PSC\Library\Calc\Option\Parser\Select;
use PSC\Library\Calc\Option\Parser\Template;
use PSC\Library\Calc\PreCalc\Parser\PreCalc;

class Parser
{
    protected $article;

    /** @var PaperContainer */
    protected $paperContainer;

    /** @var ObjectRepository */
    protected $paperRepository;

    /** @var \SimpleXMLElement $templates */
    protected $templates;

    public function __construct()
    {
    }

    public function parse(\SimpleXMLElement $node)
    {
        $optionParser = new \PSC\Library\Calc\Option\Parser();

        $this->article = new Article((string)$node->name);
    
        if(isset($node->kommentar)) {
            $this->article->setComment((string)$node->kommentar);
        }
        if(isset($node->precalc)) {
            if(isset($node->precalc['type']) && isset($node->precalc['select']) && strtolower($node->precalc['type']) == 'template') {
                $nodePreCalc = $this->templates->xpath('//precalc[@id="' . (string)$node->precalc['select'] . '"]');
                if(isset($nodePreCalc[0])) {
                    $parser = new PreCalc($nodePreCalc[0]);
                    $this->article->setPreCalc($parser->parse());
                }
            }else{
                $parser = new PreCalc($node->precalc);
                $this->article->setPreCalc($parser->parse());
            }
        }

        if(isset($node->displaygroups)) {
            
            foreach($node->displaygroups->group as $group) {
                $this->article->addDisplayGroup(new DisplayGroup((string)$group['id'], (string)$group['name']));
            }
        }

        foreach ($node->option as $option) {
            $obj = $optionParser->getOptByType($option);
            if($obj) {

                if($obj instanceof Select) {
                    $obj->setPaperContainer($this->getPaperContainer());
                    $obj->setPaperRepository($this->getPaperRepository());
                }
                if($obj instanceof Template) {
                    $element = $obj->parse();

                    $default = $element->getDefault();
                    $node = $this->templates->xpath('//option[@id="' . $element->getSelect() . '"]');
                    $obj = $optionParser->getOptByType($node[0]);
                    $element = $obj->parse();
                    if($default != "") {
                        $element->setDefault($default);
                    }
                }else{
                    $element = $obj->parse();
                }
                $this->article->addOption($element);
            }
        }

        return $this->article;
    }

    /**
     * @return PaperContainer
     */
    public function getPaperContainer()
    {
        return $this->paperContainer;
    }

    /**
     * @param PaperContainer $paperContainer
     */
    public function setPaperContainer($paperContainer)
    {
        $this->paperContainer = $paperContainer;
    }

    /**
     * @return ObjectRepository
     */
    public function getPaperRepository()
    {
        return $this->paperRepository;
    }

    /**
     * @param ObjectRepository $paperRepository
     */
    public function setPaperRepository($paperRepository)
    {
        $this->paperRepository = $paperRepository;
    }

    public function setTemplates(\SimpleXMLElement $templates)
    {
        $this->templates = $templates;
    }

}
