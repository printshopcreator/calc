<?php

namespace PSC\Library\Calc\Error\Validation\Input;

use PSC\Library\Calc\Error\Validation\Base;

class Max implements Base {

    protected int $currentValue = 0;

    protected int $maxValue = 0;

    private string $TYPE = "input::validation::max";

    public function getType(): string
    {
        return $this->TYPE;
    }

    public function __construct(int $currentValue, int $maxValue)
    {
        $this->currentValue = $currentValue;
        $this->maxValue = $maxValue;
    }

    public function getMessage(): string
    {
        return "value must be lower then " . $this->maxValue;
    }
}
