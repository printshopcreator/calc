<?php

namespace PSC\Library\Calc\Error\Validation\Input;

use PSC\Library\Calc\Error\Validation\Base;

class Min implements Base {

    protected int $currentValue = 0;

    protected int $minValue = 0;

    private string $TYPE = "input::validation::min";

    public function getType(): string
    {
        return $this->TYPE;
    }

    public function __construct(int $currentValue, int $minValue)
    {
        $this->minValue = $minValue;
        $this->currentValue = $currentValue;
    }

    public function getMessage(): string
    {
        return "value must be greater then " . $this->minValue;
    }
}
