<?php

namespace PSC\Library\Calc\Error\Validation;

interface Base {

    public function getType(): string;

    public function getMessage(): string;

}
