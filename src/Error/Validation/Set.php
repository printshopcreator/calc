<?php

namespace PSC\Library\Calc\Error\Validation;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;

class Set implements Countable, IteratorAggregate {

    private array $values = [];
    
    public function count(): int
    {
        return count($this->values);
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->values);
    }

    public function add(Base $base): Set
    {
        if($this->has($base)) {
            return $this;
        }
        
        $this->values[] = $base;
        return $this; 
    }
    
    private function has(Base $base): bool
    {
        return array_search($base, $this->values) !== false;
    }

    public function first(): ?Base
    {
        if($this->count() === 0) {
            return null;
        }
        return array_values($this->values)[0];
    }
}
