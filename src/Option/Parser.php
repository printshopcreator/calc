<?php
namespace PSC\Library\Calc\Option;

use PSC\Library\Calc\Option\Parser\Checkbox;
use PSC\Library\Calc\Option\Parser\Hidden;
use PSC\Library\Calc\Option\Parser\Input;
use PSC\Library\Calc\Option\Parser\Radio;
use PSC\Library\Calc\Option\Parser\Select;
use PSC\Library\Calc\Option\Parser\Template;
use PSC\Library\Calc\Option\Parser\Text;
use PSC\Library\Calc\Option\Parser\Textarea;
use PSC\Library\Calc\PaperContainer\Container;

class Parser
{
    protected $node;

    public function __construct()
    {
    }

    public function getOptByType(\SimpleXMLElement $node)
    {
        $this->node = $node;

        $obj = false;

        switch(strtolower((string)$node['type'])) {
            case 'input':
                $obj = new Input($node);
                break;
            case 'select':
                $obj = new Select($node);
                break;
            case 'radio':
                $obj = new Radio($node);
                break;
            case 'checkbox':
                $obj = new Checkbox($node);
                break;
            case 'text':
                $obj = new Text($node);
                break;
            case 'textarea':
                $obj = new Textarea($node);
                break;
            case 'hidden':
                $obj = new Hidden($node);
                break;
            case 'template':
                $obj = new Template($node);
                break;
        }

        return $obj;

    }
}