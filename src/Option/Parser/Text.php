<?php
namespace PSC\Library\Calc\Option\Parser;

use PSC\Library\Calc\General\Parser\EdgeCollectionContainer;

class Text extends Base
{

    protected $element;

    public function __construct(\SimpleXMLElement $node)
    {
        $this->element = new \PSC\Library\Calc\Option\Type\Text();
        parent::__construct($node);
    }

    public function parse()
    {
        parent::parse();

        if($this->node->children()) {
            $edgeCollectionContainerParser = new EdgeCollectionContainer($this->node);
            $this->element->setEdgesCollectionContainer($edgeCollectionContainerParser->parse());
        }

        return $this->element;
    }

}