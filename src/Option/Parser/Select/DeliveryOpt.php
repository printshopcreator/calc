<?php
namespace PSC\Library\Calc\Option\Parser\Select;

use PSC\Library\Calc\General\Parser\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Parser\Base;

class DeliveryOpt extends Opt
{

    public function __construct(\SimpleXMLElement $node)
    {
        $this->element = new \PSC\Library\Calc\Option\Type\Select\DeliveryOpt();
        $this->node = $node;
    }

    public function parse()
    {
        parent::parse();

        if(isset($this->node['info'])) {
            $this->element->setInfo((string)$this->node['info']);
        }
        if(isset($this->node['country'])) {
            $this->element->setCountry((string)$this->node['country']);
        }
        if(isset($this->node['dateFormat'])) {
            $this->element->setDateFormat((string)$this->node['dateFormat']);
        }
        if(isset($this->node['maxTime'])) {
            $this->element->setMaxTime((int)$this->node['maxTime']);
        }
        if(isset($this->node['workDays'])) {
            $this->element->setWorkDays((int)$this->node['workDays']);
        }

        $this->element->setLabel((string)$this->node['name']);

        return $this->element;
    }

}