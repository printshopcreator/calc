<?php
namespace PSC\Library\Calc\Option\Parser\Checkbox;

use PSC\Library\Calc\General\Parser\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Parser\Base;

class Opt
{
    protected $element;
    private \SimpleXMLElement $node;

    public function __construct(\SimpleXMLElement $node)
    {
        $this->element = new \PSC\Library\Calc\Option\Type\Checkbox\Opt();
        $this->node = $node;
    }

    public function parse()
    {
        $this->element->setId((string)$this->node['id']);
        $this->element->setLabel((string)$this->node['name']);

        if($this->node->children()) {
            $edgeCollectionContainerParser = new EdgeCollectionContainer($this->node);
            $this->element->setEdgesCollectionContainer($edgeCollectionContainerParser->parse());
        }

        return $this->element;
    }

}