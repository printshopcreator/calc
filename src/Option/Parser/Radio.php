<?php
namespace PSC\Library\Calc\Option\Parser;

use Doctrine\Persistence\ObjectRepository;
use PSC\Library\Calc\General\Parser\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Parser\Select\Opt;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\Paper;

class Radio extends Base
{

    protected $element;

    /** @var \SimpleXMLElement $node */
    protected $node;

    public function __construct(\SimpleXMLElement $node)
    {

        $this->element = new \PSC\Library\Calc\Option\Type\Radio();

        parent::__construct($node);
    }

    public function parse()
    {
        parent::parse();

        if(isset($this->node->grenzen) && $this->node->grenzen->children()) {
            $edgeCollectionContainerParser = new EdgeCollectionContainer($this->node->grenzen);
            $this->element->setEdgesCollectionContainer($edgeCollectionContainerParser->parse());
        }

        $this->parseModeNormal();

        return $this->element;
    }

    private function parseModeNormal()
    {
        foreach ($this->node->opt as $opt) {
            $optParser = new Opt($opt);
            $this->element->addOption($optParser->parse());
        }
    }

}