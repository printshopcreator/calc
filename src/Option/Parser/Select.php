<?php
namespace PSC\Library\Calc\Option\Parser;

use Color\System\HKS;
use Color\System\PANTONE;
use Doctrine\Persistence\ObjectRepository;
use PSC\Library\Calc\General\Parser\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Parser\Select\Opt;
use PSC\Library\Calc\Option\Parser\Select\DeliveryOpt;
use PSC\Library\Calc\PaperContainer;
use PSC\Library\Calc\Tests\Mock\Paper;

class Select extends Base
{

    protected $element;

    /** @var \SimpleXMLElement $node */
    protected $node;

    /** @var PaperContainer */
    protected $paperContainer;

    /** @var ObjectRepository */
    protected $paperRepository;

    public function __construct(\SimpleXMLElement $node)
    {
        if(isset($node['mode']) && (string)$node['mode'] == \PSC\Library\Calc\Option\Type\Select::$modePaperDb) {
            $this->element = new \PSC\Library\Calc\Option\Type\PaperDbSelect();
        }elseif(isset($node['mode']) && (string)$node['mode'] == \PSC\Library\Calc\Option\Type\Select::$modeDelivery) {
            $this->element = new \PSC\Library\Calc\Option\Type\DeliverySelect();
        }elseif(isset($node['mode']) && (string)$node['mode'] == \PSC\Library\Calc\Option\Type\Select::$modeColorDb) {
            $this->element = new \PSC\Library\Calc\Option\Type\ColorDBSelect();
        }else{
            $this->element = new \PSC\Library\Calc\Option\Type\Select();
        }

        parent::__construct($node);
    }

    public function parse()
    {
        parent::parse();

        if(isset($this->node->grenzen) && $this->node->grenzen->children()) {
            $edgeCollectionContainerParser = new EdgeCollectionContainer($this->node->grenzen);
            $this->element->setEdgesCollectionContainer($edgeCollectionContainerParser->parse());
        }

        if(isset($this->node['mode']) && (string)$this->node['mode'] == \PSC\Library\Calc\Option\Type\Select::$modePaperDb) {
            $this->parseModePapierDb();
        }elseif(isset($this->node['mode']) && (string)$this->node['mode'] == \PSC\Library\Calc\Option\Type\Select::$modeDelivery) {
            $this->parseModeDelivery();
        }elseif(isset($this->node['mode']) && (string)$this->node['mode'] == \PSC\Library\Calc\Option\Type\Select::$modeColorDb) {
            $this->parseModeColorDb();
        }else{
            $this->parseModeNormal();
        }

        return $this->element;
    }

    private function parseModeDelivery()
    {
        foreach ($this->node->opt as $opt) {
            $optParser = new DeliveryOpt($opt);
            $this->element->addOption($optParser->parse());
        }

    }

    private function parseModePapierDb()
    {
        $this->element->setNewPaperObject($this->getPaperRepository()->getNewObject());
        /** @var PaperContainer\Container $container */
        $container = $this->getPaperContainer()->getContainerById((string)$this->node['container']);

        if($container) {
            /** @var PaperContainer\Item $papier */
            
            foreach ($container->getItems() as $papier) {
                /** @var Paper $paper */
                $paper = $this->getPaperRepository()->findOneBy(array('artNr' => $papier->getId()));

                if($paper) {
                    $optPapier = new \PSC\Library\Calc\Option\Type\Select\PaperOpt();
                    $optPapier->setId($paper->getArtNr());
                    $optPapier->setLabel($paper->getDescription1());
                    $optPapier->setPaper($paper);
                    $optPapier->setValue($papier->getValue());
                    $optPapier->setEdgesCollectionContainer($papier->getEdgesCollectionContainer());
                    $this->element->addOption($optPapier);
                }
            }
        }
    }

    private function parseModeColorDb(): void
    {
        $colorSystem = (string)$this->node['container'];
        if(str_contains($colorSystem, "panton")) {
            $system = new PANTONE(\Color\System\Enum\PANTONE::from($colorSystem));
        }
        if(str_contains($colorSystem, "hks")) {
            $system = new HKS(\Color\System\Enum\HKS::from($colorSystem));
        }

        if($system) {
            $this->element->setColorSystem($colorSystem);

            foreach ($system->getAllColors() as $color) {
                $optColor = new \PSC\Library\Calc\Option\Type\Select\ColorOpt();
                $optColor->setId($color->getName());
                $optColor->setLabel(sprintf("%s %s %s", $system->getColorSystemPrefix(), $color->getValue("name"), $system->getColorSystemSuffix()));
                $optColor->setColorHex((string)$color->getHex());
                $optColor->setColorCMYK((string)$color->getCMYK());
                $optColor->setColorRGB((string)$color->getRGB());
                $optColor->setValue((string)$color->getHEX());
                $this->element->addOption($optColor);
            }
        }
    }

    private function parseModeNormal()
    {
        foreach ($this->node->opt as $opt) {
            $optParser = new Opt($opt);
            $this->element->addOption($optParser->parse());
        }
    }

    /**
     * @return PaperContainer
     */
    public function getPaperContainer()
    {
        return $this->paperContainer;
    }

    /**
     * @param PaperContainer $paperContainer
     */
    public function setPaperContainer($paperContainer)
    {
        $this->paperContainer = $paperContainer;
    }

    /**
     * @param ObjectRepository $repostory
     */
    public function setPaperRepository($repository)
    {
        $this->paperRepository = $repository;
    }

    /**
     * @return ObjectRepository
     */
    public function getPaperRepository()
    {
        return $this->paperRepository;
    }

}