<?php
namespace PSC\Library\Calc\Option\Parser;

class Base
{
    /** @var \PSC\Library\Calc\Option\Type\Base $element */
    protected $element;

    /** @var  \SimpleXMLElement $node */
    protected $node;

    public function __construct(\SimpleXMLElement $node)
    {
        $this->node = $node;
    }

    protected function parse()
    {
        $this->element->setId((string)$this->node['id']);
        $this->element->setName((string)$this->node['name']);

        if(isset($this->node['default'])) {
            $this->element->setDefault((string)$this->node['default']);
        }
        if(isset($this->node['require'])) {
            $this->element->setRequire($this->getBoolean($this->node['require']));
        }
        if(isset($this->node['required'])) {
            $this->element->setRequire($this->getBoolean($this->node['required']));
        }
        if(isset($this->node['help'])) {
            $this->element->setHelp((string)$this->node['help']);
        }
        if(isset($this->node['helplink'])) {
            $this->element->setHelpLink((string)$this->node['helplink']);
        }
        if(isset($this->node['exportAjax']) && (string)$this->node['exportAjax'] == 1) {
            $this->element->setIsAjaxExport(true);
        }
        if(isset($this->node['displayOnly']) && (string)$this->node['displayOnly'] == 1) {
            $this->element->setIsDisplayOnly(true);
        }
        if(isset($this->node['amount']) && (string)$this->node['amount'] == 0) {
            $this->element->setAmount(false);
        }
        if(isset($this->node['display_group']) && (string)$this->node['display_group'] != "") {
            $this->element->setDisplayGroup((string)$this->node['display_group']);
        }

    }

    private function getBoolean($value)
    {
        if((string)$value == 'true' || (string)$value == '1') {
            return true;
        }
        return false;
    }

}
