<?php
namespace PSC\Library\Calc\Option\Parser;

use PSC\Library\Calc\General\Parser\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Type\Input as PSCInput;
use SimpleXMLElement;

class Input extends Base
{

    public function __construct(\SimpleXMLElement $node)
    {
        $this->element = new \PSC\Library\Calc\Option\Type\Input();
        parent::__construct($node);
    }

    public function parse(): PSCInput
    {
        parent::parse();

        if(isset($this->node['min'])) {
            $this->element->setMinValue((int)$this->node['min']);
        }

        if(isset($this->node['max'])) {
            $this->element->setMaxValue((int)$this->node['max']);
        }

        if(isset($this->node['pattern'])) {
            $this->element->setPattern((string)$this->node['pattern']);
        }
        if(isset($this->node['placeholder'])) {
            $this->element->setPlaceHolder((string)$this->node['placeholder']);
        }

        if($this->node->children()) {
            $edgeCollectionContainerParser = new EdgeCollectionContainer($this->node);
            $this->element->setEdgesCollectionContainer($edgeCollectionContainerParser->parse());
        }

        return $this->element;
    }

}
