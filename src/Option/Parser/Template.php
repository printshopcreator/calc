<?php
namespace PSC\Library\Calc\Option\Parser;

class Template extends Base
{

    protected $element;

    public function __construct(\SimpleXMLElement $node)
    {
        $this->element = new \PSC\Library\Calc\Option\Type\Template();
        parent::__construct($node);
    }

    public function parse()
    {
        parent::parse();

        if($this->node['select']) {
            $this->element->setSelect((string)$this->node['select']);
        }
        return $this->element;
    }

}