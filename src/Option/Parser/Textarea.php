<?php
namespace PSC\Library\Calc\Option\Parser;

class Textarea extends Base
{

    protected $element;

    public function __construct(\SimpleXMLElement $node)
    {
        $this->element = new \PSC\Library\Calc\Option\Type\Textarea();
        parent::__construct($node);
    }

    public function parse()
    {
        parent::parse();

        return $this->element;
    }

}