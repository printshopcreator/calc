<?php
namespace PSC\Library\Calc\Option\Type;

use PSC\Library\Calc\Option\Type\Select\Opt;

class Radio extends Base
{

    public $type = 'radio';

    /** @var \ArrayIterator $options */
    protected $options;

    public function __construct()
    {
        parent::__construct();
        $this->options = new \ArrayIterator();
    }

    public function addOption($option)
    {
        $this->options->append($option);
    }

    /**
     * Verarbeitet das Value
     */
    public function processValue()
    {
        if($this->rawValue != "") {
            /** @var \PSC\Library\Calc\Option\Type\Radio\Opt $item */
            foreach($this->options as $item) {
                if($item->getId() == $this->rawValue) {
                    $item->setIsSelected(true);
                }
            }
        }
    }

    /**
     * @return \ArrayIterator
     */
    public function getOptions()
    {
        return $this->options;
    }

    public function getSelectedOption()
    {
        /** @var Opt $opt */
        foreach($this->getOptions() as $opt) {
            if($opt->isSelected()) return $opt;
        }
    }

    public function getValue()
    {
        return $this->getSelectedOption()->getLabel();
    }

}