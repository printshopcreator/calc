<?php
namespace PSC\Library\Calc\Option\Type;

class Input extends Base
{
    private ?int $minValue = null;
    
    private ?int $maxValue = null;

    private string $pattern = "";

    private string $placeHolder = "";

    public $type = 'input';

    public function setMinValue(?int $min): void
    {
        $this->minValue = $min;
    }

    public function getMinValue(): ?int
    {
        return $this->minValue;
    }

    public function setMaxValue(?int $max): void
    {
        $this->maxValue = $max;
    }
    
    public function getMaxValue(): ?int
    {
        return $this->maxValue;
    }

    public function getPattern(): string
    {
        return $this->pattern;
    }

    public function setPattern(string $pattern): void
    {
        $this->pattern = $pattern;
    }

    public function getPlaceHolder(): string
    {
        return $this->placeHolder;
    }

    public function setPlaceHolder(string $placeHolder): void
    {
        $this->placeHolder = $placeHolder;
    }
}
