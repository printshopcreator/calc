<?php
namespace PSC\Library\Calc\Option\Type\Radio;

use PSC\Library\Calc\General\Type\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Type\Base;

class Opt
{
    /** @var string $id */
    protected $id;

    /** @var string $label */
    protected $label;

    /** @var EdgeCollectionContainer */
    protected $edgesCollectionContainer;

    /** @var bool */
    protected $isValid = true;

    /** @var bool */
    protected $isSelected = false;

    /**
     * Opt constructor.
     */
    public function __construct()
    {
        $this->edgesCollectionContainer = new EdgeCollectionContainer();
    }


    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return EdgeCollectionContainer
     */
    public function getEdgesCollectionContainer()
    {
        return $this->edgesCollectionContainer;
    }

    /**
     * @param EdgeCollectionContainer $edgesCollectionContainer
     */
    public function setEdgesCollectionContainer($edgesCollectionContainer)
    {
        $this->edgesCollectionContainer = $edgesCollectionContainer;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->isValid;
    }

    /**
     * @param bool $isValid
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;
    }

    /**
     * @return bool
     */
    public function isSelected()
    {
        return $this->isSelected;
    }

    /**
     * @param bool $isSelected
     */
    public function setIsSelected($isSelected)
    {
        $this->isSelected = $isSelected;
    }
}