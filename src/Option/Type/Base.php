<?php
namespace PSC\Library\Calc\Option\Type;

use PSC\Library\Calc\Error\Validation\Base as PSCBase;
use PSC\Library\Calc\Error\Validation\Set;
use PSC\Library\Calc\General\Type\EdgeCollectionContainer;

class Base
{
    /** @var string $name */
    protected $name;

    /** @var string $id */
    protected $id;

    /** @var string $default */
    protected $default = null;

    /** @var string $help */
    protected ?string $help = null;

    protected ?string $helpLink = null;

    /** @var boolean $require */
    protected $require = false;

    /** @var string $type */
    public $type = 'base';

    /** @var string $rawValue */
    protected $rawValue = '';

    /** @var string $value */
    protected $value = '';

    protected string $displayGroup = '';

    /** @var EdgeCollectionContainer */
    protected $edgesCollectionContainer = null;

    /** @var bool */
    protected $isValid = true;

    /** @var bool */
    protected $isAjaxExport = false;

    /** @var bool */
    protected $isDisplayOnly = false;

    protected $savedCalcValues = [];
    private $amount = true;

    protected Set $validationErrors; 

    public function __construct()
    {
        $this->edgesCollectionContainer = new EdgeCollectionContainer();
        $this->validationErrors = new Set();
    }

    public function addValidationError(PSCBase $error)
    {
        $this->validationErrors->add($error);
    }

    public function getValidationErrors(): Set
    {
        return $this->validationErrors;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function setDisplayGroup(string $var): void
    {
        $this->displayGroup = $var;
    }

    public function getDisplayGroup(): string
    {
        return $this->displayGroup;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function isRequire()
    {
        return $this->require;
    }

    /**
     * @param boolean $require
     */
    public function setRequire($require)
    {
        $this->require = $require;
    }

    /**
     * @return string
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param string $default
     */
    public function setDefault($default)
    {
        $this->default = $default;
    }

    public function getHelp(): ?string
    {
        return $this->help;
    }

    public function setHelp(string $help): void
    {
        $this->help = $help;
    }

    public function setHelpLink(string $var): void
    {
        $this->helpLink = $var;
    }

    public function getHelpLink(): ?string
    {
        return $this->helpLink;
    }

    /**
     * @return string
     */
    public function getRawValue()
    {
        return $this->rawValue;
    }

    /**
     * @param string $rawValue
     */
    public function setRawValue($rawValue)
    {
        $this->rawValue = $rawValue;
    }

    /**
     * Verarbeitet das Value
     */
    public function processValue()
    {
        $this->setValue($this->getRawValue());
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->isValid;
    }

    /**
     * @param bool $isValid
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;
    }

    /**
     * @return EdgeCollectionContainer
     */
    public function getEdgesCollectionContainer()
    {
        return $this->edgesCollectionContainer;
    }

    /**
     * @param EdgeCollectionContainer $edgesCollectionContainer
     */
    public function setEdgesCollectionContainer($edgesCollectionContainer)
    {
        $this->edgesCollectionContainer = $edgesCollectionContainer;
    }

    /**
     * @return bool
     */
    public function isAjaxExport()
    {
        return $this->isAjaxExport;
    }

    /**
     * @param bool $isAjaxExport
     */
    public function setIsAjaxExport($isAjaxExport)
    {
        $this->isAjaxExport = $isAjaxExport;
    }

    public function parseAdditionalValues($variables)
    {
        return $variables;
    }

    /**
     * @return array
     */
    public function getSavedCalcValues()
    {
        return $this->savedCalcValues;
    }

    /**
     * @param array $savedCalcValues
     */
    public function setSavedCalcValues($savedCalcValues)
    {
        $this->savedCalcValues = $savedCalcValues;
    }

    /**
     * @return bool
     */
    public function isDisplayOnly()
    {
        return $this->isDisplayOnly;
    }

    /**
     * @param bool $isDisplayOnly
     */
    public function setIsDisplayOnly($isDisplayOnly)
    {
        $this->isDisplayOnly = $isDisplayOnly;
    }

    /**
     * @return bool
     */
    public function isAmount()
    {
        return $this->amount;
    }

    /**
     * @param bool $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function toArray() : array {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'type' => $this->getType(),
            'valid' => $this->isValid()
        ];
    }
}
