<?php
namespace PSC\Library\Calc\Option\Type;

use PSC\Library\Calc\Option\Type\Select\Opt;

class Checkbox extends Base
{

    public $type = 'checkbox';

    /** @var \ArrayIterator $options */
    protected $options;

    public function __construct()
    {
        parent::__construct();
        $this->options = new \ArrayIterator();
    }

    public function addOption($option)
    {
        $this->options->append($option);
    }

    /**
     * Verarbeitet das Value
     */
    public function processValue()
    {
        if(is_array($this->rawValue)) {
            /** @var \PSC\Library\Calc\Option\Type\Checkbox\Opt $item */
            foreach($this->options as $item) {
                if(in_array($item->getId(), $this->rawValue)) {
                    $item->setIsSelected(true);
                }
            }
        }elseif($this->rawValue != "") {
            /** @var \PSC\Library\Calc\Option\Type\Checkbox\Opt $item */
            foreach($this->options as $item) {
                if($item->getId() == $this->rawValue) {
                    $item->setIsSelected(true);
                }
            }
        }
    }

    /**
     * @return \ArrayIterator
     */
    public function getOptions()
    {
        return $this->options;
    }

    public function getSelectedOptions()
    {
        $tmp = [];

        /** @var Opt $opt */
        foreach($this->getOptions() as $opt) {
            if($opt->isSelected()) $tmp[] = $opt;
        }

        return $tmp;
    }

    public function getValue()
    {
        $tmp = array();
        foreach($this->getSelectedOptions() as $option) {
            $tmp[] = $option->getLabel();
        }

        return implode(", ", $tmp);
    }

}