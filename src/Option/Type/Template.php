<?php
namespace PSC\Library\Calc\Option\Type;

class Template extends Base
{

    public $type = 'template';

    /** @var String $select */
    protected $select = '';

    /**
     * @return String
     */
    public function getSelect()
    {
        return $this->select;
    }

    /**
     * @param String $select
     */
    public function setSelect($select)
    {
        $this->select = $select;
    }
}