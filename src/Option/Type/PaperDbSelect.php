<?php
namespace PSC\Library\Calc\Option\Type;

use PSC\Library\Calc\Option\Type\Select\Opt;
use PSC\Library\Calc\Option\Type\Select\PaperOpt;
use PSC\Library\Calc\Tests\Mock\Paper;

class PaperDbSelect extends Select
{
    protected $newPaperObject = null;

    public function getSelectedOption()
    {
        /** @var Opt $opt */
        foreach($this->getOptions() as $opt) {
            if($opt->isSelected()) return $opt;
        }

        if(isset($this->savedCalcValues[$this->getId()]) && $this->savedCalcValues[$this->getId()]['art_nr'] == $this->getRawValue()) {
            $opt = new PaperOpt();
            $opt->setIsSelected(true);
            $opt->setId($this->savedCalcValues[$this->getId()]['art_nr']);
            $opt->setLabel($this->savedCalcValues[$this->getId()]['description_1']);


            $paper = $this->newPaperObject;
            $paper->setId($this->savedCalcValues[$this->getId()]['id']);
            $paper->setArtNr($this->savedCalcValues[$this->getId()]['art_nr']);
            $paper->setDescription1($this->savedCalcValues[$this->getId()]['description_1']);
            $paper->setDescription2($this->savedCalcValues[$this->getId()]['description_2']);
            $paper->setPreis($this->savedCalcValues[$this->getId()]['preis']);
            $paper->setGrammatur($this->savedCalcValues[$this->getId()]['grammatur']);
            $paper->setOffsetFix($this->savedCalcValues[$this->getId()]['offset_fix']);
            $paper->setOffsetVar($this->savedCalcValues[$this->getId()]['offset_var']);
            $paper->setDigitalFix($this->savedCalcValues[$this->getId()]['digital_fix']);
            $paper->setDigitalVar($this->savedCalcValues[$this->getId()]['digital_var']);

            $paper->setVolume($this->savedCalcValues[$this->getId()]['volume']);

            $opt->setPaper($paper);

            $this->addOption($opt);

            return $opt;
        }
    }

    public function parseAdditionalValues($variables)
    {
        /** @var PaperOpt $option */
        $option = $this->getSelectedOption();

        if($option == null) {

            $variables[$this->getId() . '_grammatur'] = 0;
            $variables[$this->getId() . '_art_nr'] = 0;
            $variables[$this->getId() . '_volume'] = 0;
            $variables[$this->getId() . '_value'] = 0;
            $variables[$this->getId() . '_offset_fix'] = 0;
            $variables[$this->getId() . '_offset_var'] = 0;
            $variables[$this->getId() . '_digital_fix'] = 0;
            $variables[$this->getId() . '_digital_var'] = 0;

            $variables[$this->getId() . '_papiertyp1'] = 0;
            $variables[$this->getId() . '_papiertyp2'] = 0;
            $variables[$this->getId() . '_papiertyp3'] = 0;
            $variables[$this->getId() . '_papiertyp4'] = 0;
            $variables[$this->getId() . '_papiertyp5'] = 0;
            $variables[$this->getId() . '_papiertyp6'] = 0;
            $variables[$this->getId() . '_papiertyp7'] = 0;
            $variables[$this->getId() . '_papiertyp8'] = 0;
            $variables[$this->getId() . '_papiertyp9'] = 0;
            $variables[$this->getId() . '_papiertyp10'] = 0;
            $variables[$this->getId() . '_papiertyp11'] = 0;
            $variables[$this->getId() . '_papiertyp12'] = 0;
            $variables[$this->getId() . '_papiertyp13'] = 0;
            $variables[$this->getId() . '_papiertyp14'] = 0;
            $variables[$this->getId() . '_umschlagen'] = 0;

            $variables[$this->getId() . '_staffelmenge_1'] = 0;
            $variables[$this->getId() . '_staffelpreis_1'] = 0;
            $variables[$this->getId() . '_staffelmenge_2'] = 0;
            $variables[$this->getId() . '_staffelpreis_2'] = 0;
            $variables[$this->getId() . '_staffelmenge_3'] = 0;
            $variables[$this->getId() . '_staffelpreis_3'] = 0;
            $variables[$this->getId() . '_staffelmenge_4'] = 0;
            $variables[$this->getId() . '_staffelpreis_4'] = 0;
            $variables[$this->getId() . '_staffelmenge_5'] = 0;
            $variables[$this->getId() . '_staffelpreis_5'] = 0;
            $variables[$this->getId() . '_mengeneinheit'] = 0;
            $variables[$this->getId() . '_staerke'] = 0;
            $variables[$this->getId() . '_breite'] = 0;
            $variables[$this->getId() . '_laenge'] = 0;
            $variables[$this->getId() . '_etiketten_je_blatt'] = 0;
            $variables[$this->getId() . '_kleber'] = 0;

            $variables[$this->getId() . '_happy'] = 0;
            $variables[$this->getId() . '_eq'] = 0;
            $variables[$this->getId() . '_sense'] = 0;
            $variables[$this->getId() . '_sky'] = 0;
            $variables[$this->getId() . '_glam'] = 0;
            $variables[$this->getId() . '_post'] = 0;
            $variables[$this->getId() . '_sammelform'] = 0;

        }else {

            /** @var Paper $paper */
            $paper = $option->getPaper();

            $variables[$this->getId() . '_grammatur'] = $paper->getGrammatur();
            $variables[$this->getId() . '_art_nr'] = $paper->getArtNr();
            $variables[$this->getId() . '_volume'] = $paper->getVolume();
            $variables[$this->getId() . '_value'] = $paper->getPreis();
            if($option->getValue() > 0) {
                $variables[$this->getId() . '_value'] = $option->getValue();
            }
            $variables[$this->getId() . '_offset_fix'] = $paper->getOffsetFix();
            $variables[$this->getId() . '_offset_var'] = $paper->getOffsetVar();
            $variables[$this->getId() . '_digital_fix'] = $paper->getDigitalFix();
            $variables[$this->getId() . '_digital_var'] = $paper->getDigitalVar();

            $variables[$this->getId() . '_papiertyp1'] = $paper->getPapierTyp1();
            $variables[$this->getId() . '_papiertyp2'] = $paper->getPapierTyp2();
            $variables[$this->getId() . '_papiertyp3'] = $paper->getPapierTyp3();
            $variables[$this->getId() . '_papiertyp4'] = $paper->getPapierTyp4();
            $variables[$this->getId() . '_papiertyp5'] = $paper->getPapierTyp5();
            $variables[$this->getId() . '_papiertyp6'] = $paper->getPapierTyp6();
            $variables[$this->getId() . '_papiertyp7'] = $paper->getPapierTyp7();
            $variables[$this->getId() . '_papiertyp8'] = $paper->getPapierTyp8();
            $variables[$this->getId() . '_papiertyp9'] = $paper->getPapierTyp9();
            $variables[$this->getId() . '_papiertyp10'] = $paper->getPapierTyp10();
            $variables[$this->getId() . '_papiertyp11'] = $paper->getPapierTyp11();
            $variables[$this->getId() . '_papiertyp12'] = $paper->getPapierTyp12();
            $variables[$this->getId() . '_papiertyp13'] = $paper->getPapierTyp13();
            $variables[$this->getId() . '_papiertyp14'] = $paper->getPapierTyp14();
            $variables[$this->getId() . '_umschlagen'] = $paper->getUmschlagen();

            $variables[$this->getId() . '_staffelmenge_1'] = $paper->getStaffelmenge1();
            $variables[$this->getId() . '_staffelpreis_1'] = $paper->getStaffelpreis1();
            $variables[$this->getId() . '_staffelmenge_2'] = $paper->getStaffelmenge2();
            $variables[$this->getId() . '_staffelpreis_2'] = $paper->getStaffelpreis2();
            $variables[$this->getId() . '_staffelmenge_3'] = $paper->getStaffelmenge3();
            $variables[$this->getId() . '_staffelpreis_3'] = $paper->getStaffelpreis3();
            $variables[$this->getId() . '_staffelmenge_4'] = $paper->getStaffelmenge4();
            $variables[$this->getId() . '_staffelpreis_4'] = $paper->getStaffelpreis4();
            $variables[$this->getId() . '_staffelmenge_5'] = $paper->getStaffelmenge5();
            $variables[$this->getId() . '_staffelpreis_5'] = $paper->getStaffelpreis5();
            $variables[$this->getId() . '_mengeneinheit'] = $paper->getMengeneinheit();
            $variables[$this->getId() . '_staerke'] = $paper->getStaerke();
            $variables[$this->getId() . '_breite'] = $paper->getBreite();
            $variables[$this->getId() . '_laenge'] = $paper->getLaenge();
            $variables[$this->getId() . '_etiketten_je_blatt'] = $paper->getEtikettenJeBlatt();
            $variables[$this->getId() . '_kleber'] = $paper->getKleber();

            $variables[$this->getId() . '_happy'] = $paper->getHappy();
            $variables[$this->getId() . '_eq'] = $paper->getEq();
            $variables[$this->getId() . '_sense'] = $paper->getSense();
            $variables[$this->getId() . '_sky'] = $paper->getSky();
            $variables[$this->getId() . '_glam'] = $paper->getGlam();
            $variables[$this->getId() . '_post'] = $paper->getPost();
            $variables[$this->getId() . '_sammelform'] = $paper->getSammelform();

        }

        return $variables;
    }

    /**
     * @param null $newPaperObject
     */
    public function setNewPaperObject($newPaperObject)
    {
        $this->newPaperObject = $newPaperObject;
    }

}