<?php
namespace PSC\Library\Calc\Option\Type;

use PSC\Library\Calc\Option\Type\Select\Opt;
use PSC\Library\Calc\Option\Type\Select\PaperOpt;
use PSC\Library\Calc\Tests\Mock\Paper;

class DeliverySelect extends Select
{
    public function getSelectedOption()
    {
        /** @var Opt $opt */
        foreach($this->getOptions() as $opt) {
            if($opt->isSelected()) return $opt;
        }
    }

}