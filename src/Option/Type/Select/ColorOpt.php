<?php
namespace PSC\Library\Calc\Option\Type\Select;

use Color\Value\CMYK;
use Color\Value\HEX;
use Color\Value\RGB;
use Color\Value\ValueInterface;
use PSC\Library\Calc\General\Type\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Type\Base;

class ColorOpt extends Opt
{
    protected $colorHex = null;
    protected $colorCMYK = null;
    protected $colorRGB = null;
    private string $value = "";

    public function setValue(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getColorHex(): string|null
    {
        return $this->colorHex;
    }

    public function setColorHex(string $colorHex): void
    {
        $this->colorHex = $colorHex;
    }

    public function getColorCMYK(): string|null
    {
        return $this->colorCMYK;
    }

    public function setColorCMYK(string $colorCMYK): void
    {
        $this->colorCMYK = $colorCMYK;
    }

    public function getColorRGB(): string|null
    {
        return $this->colorRGB;
    }

    public function setColorRGB(string $colorRGB): void
    {
        $this->colorRGB = $colorRGB;
    }
}