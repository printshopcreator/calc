<?php
namespace PSC\Library\Calc\Option\Type\Select;

use PSC\Library\Calc\General\Type\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Type\Base;

class PaperOpt extends Opt
{
    protected $paper = null;
    private float $value = 0.0;

    public function setPaper($paper)
    {
        $this->paper = $paper;
    }

    /**
     * @return null
     */
    public function getPaper()
    {
        return $this->paper;
    }

    public function setValue(float $value)
    {
        $this->value = $value;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}