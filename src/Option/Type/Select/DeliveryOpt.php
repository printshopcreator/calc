<?php
namespace PSC\Library\Calc\Option\Type\Select;

use PSC\Library\Calc\General\Type\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Type\Base;
use Yasumi\Yasumi;

class DeliveryOpt extends Opt
{
    protected ?\DateTime $curDate = null;
    protected string $info = "";

    protected string $country = "Germany";

    protected string $dateFormat = "d.m.Y";

    protected int $workDays = 5;

    protected int $maxTime = 12;

    public function __construct()
    {
        $this->curDate = new \DateTime();
    }

    public function getInfo(): string
    {
        return $this->info;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getDateFormat(): string
    {
        return $this->dateFormat;
    }

    public function setDateFormat(string $dateFormat)
    {
        $this->dateFormat = $dateFormat;
    }

    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    public function setInfo(string $info)
    {
        $this->info = $info;
    }
    public function getWorkDays(): int
    {
        return $this->workDays;
    }

    public function setWorkDays(int $workDays)
    {
        $this->workDays = $workDays;
    }

    public function getMaxTime(): int
    {
        return $this->maxTime;
    }

    public function setMaxTime(int $maxTime)
    {
        $this->maxTime = $maxTime;
    }

    public function getDeliveryDate(): \DateTime
    {
        $holidays = Yasumi::create($this->country, (int) date('Y'));

        $currentHour = (int)$this->curDate->format('H');
        if($currentHour >= $this->maxTime) {
            $this->workDays++;
        }

        while($this->workDays >= 1) {
            $this->curDate->modify("+1 day");
            if($holidays->isWorkingDay($this->curDate)) {
                $this->workDays--;
            }
        }

        return $this->curDate;
    }

    public function getDeliveryDateAsString(): string
    {
        return $this->getDeliveryDate()->format($this->dateFormat);
    }

    public function setCurDate(\DateTime $curDate)
    {
        $this->curDate = $curDate;
    }
}