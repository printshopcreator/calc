<?php
namespace PSC\Library\Calc\PaperContainer;

use PSC\Library\Calc\General\Type\EdgeCollectionContainer;

class Item
{

    protected string $id;

    protected float $value = 0.0;
    protected EdgeCollectionContainer $edgesCollectionContainer;

    public function __construct()
    {
        $this->edgesCollectionContainer = new EdgeCollectionContainer();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getEdgesCollectionContainer(): EdgeCollectionContainer
    {
        return $this->edgesCollectionContainer;
    }

    public function setEdgesCollectionContainer(EdgeCollectionContainer $edgesCollectionContainer): void
    {
        $this->edgesCollectionContainer = $edgesCollectionContainer;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function setValue(float $value): void
    {
        $this->value = $value;
    }
}