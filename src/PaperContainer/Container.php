<?php
namespace PSC\Library\Calc\PaperContainer;

class Container
{
    /** @var String */
    protected $id;

    /** @var \ArrayIterator $items */
    protected $items;

    public function __construct()
    {
        $this->items = new \ArrayIterator();
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item) {
        $this->items->append($item);
    }

    /**
     * @return \ArrayIterator
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return String
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param String $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}