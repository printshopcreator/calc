<?php
namespace PSC\Library\Calc;

use ArrayIterator;
use PSC\Library\Calc\PreCalc\PreCalc;

class Article
{
    /** @var string $name */
    protected $name;

    protected string $comment = "";

    /** @var \ArrayIterator $options */
    protected $options;

    /** @var \ArrayIterator $displayGroups */
    protected $displayGroups;

    protected PreCalc $preCalc;

    public function __construct($name)
    {
        $this->options = new \ArrayIterator();
        $this->displayGroups = new \ArrayIterator();
        $this->preCalc = new PreCalc();
        $this->setName($name);
    }

    public function setPreCalc(PreCalc $preCalc)
    {
        $this->preCalc = $preCalc;
    }

    public function getPreCalc(): PreCalc
    {
        return $this->preCalc;
    }

    public function addOption($option)
    {
        $this->options->append($option);
    }

    public function addDisplayGroup(DisplayGroup $var): void
    {
        $this->displayGroups->append($var);
    }

    public function getDisplayGroups(): ArrayIterator
    {
        return $this->displayGroups;
    }

    /**
     * @param $params \Array
     */
    public function setParams($params)
    {
        /** @var Option\Type\Base $option */
        foreach($this->options as $option)
        {
            if(isset($params[$option->getId()])) {
                $option->setRawValue($params[$option->getId()]);
                $option->processValue();
            }
        }
    }

    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getOptionById($id)
    {
        if($id === false) {
            throw new \Exception('No id provided');
        }

        /** @var Option\Type\Base $option */
        foreach($this->options as $option)
        {
            if($option->getId() == $id) {
                return $option;
            }
        }

        if($id === false) {
            throw new \Exception('Option not found: ' . $id);
        }
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function clearOptions(): void
    {
        $this->options = new \ArrayIterator();
    }

    public function setOptions(array $options): void
    {
        $this->options = new \ArrayIterator($options);
    }

    public function getValidOptions()
    {
        $temp = array();

        foreach ($this->options as $option) {
            if($option->isValid()) {
                $temp[] = $option;
            }
        }
        return $temp;
    }

    public function getOptionsAsArray()
    {
        $temp = array();
        /** @var Option\Type\Base $option */
        foreach ($this->options as $option) {
            if(($option->getValue() == '' && $option->getRawValue() == '') || !$option->isValid()) {
                continue;
            }
            $temp[$option->getId()] = array(
                'name' => $option->getName(),
                'value' => $option->getValue(),
                'rawVaue' => $option->getRawValue()
            );
        }
        return $temp;
    }
}
