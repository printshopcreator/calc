<?php
namespace PSC\Library\Calc;

class DisplayGroup
{
    protected string $id;

    protected string $name;

    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId(): string
    {
        return $this->id;
    }
    
    public function getName(): string
    {
        return $this->name;
    }
}