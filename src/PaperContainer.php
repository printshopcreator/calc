<?php
namespace PSC\Library\Calc;

use PSC\Library\Calc\General\Parser\EdgeCollectionContainer;
use PSC\Library\Calc\PaperContainer\Container;
use PSC\Library\Calc\PaperContainer\Item;

class PaperContainer
{

    /** @var \ArrayIterator $container */
    protected $container;

    public function __construct()
    {
        $this->container = new \ArrayIterator();
    }

    public function parse(\SimpleXMLElement $node)
    {
        foreach($node->papiercontainer as $containerNode) {
            $container = new Container();
            $container->setId((string)$containerNode['id']);
            foreach($containerNode->papier as $paper) {
                $item = new Item();
                $item->setId((string)$paper['id']);
                if(isset($paper['value']) && (float)$paper['value'] > 0) {
                    $item->setValue((float)$paper['value']);
                }
                if(isset($paper->grenzen)) {
                    $edgeCollectionContainerParser = new EdgeCollectionContainer($paper->grenzen);
                    $item->setEdgesCollectionContainer($edgeCollectionContainerParser->parse());
                }

                $container->addItem($item);
            }
            $this->container->append($container);
        }
    }

    /**
     * @return \ArrayIterator
     */
    public function getContainer()
    {
        return $this->container;
    }

    public function getContainerById($id)
    {
        /** @var Container $article */
        foreach($this->container as $container)
        {
            if($container->getId() == $id) {
                return $container;
            }
        }
    }

}