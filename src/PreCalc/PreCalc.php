<?php

namespace PSC\Library\Calc\PreCalc;

use ArrayIterator;

class PreCalc {


    protected ArrayIterator $groups;

    public function __construct()
    {
        $this->groups = new ArrayIterator();
    }

    public function addGroup(Group $group): void
    {
        $this->groups->append($group);
    }
    
    public function getGroups(): ArrayIterator
    {
        return $this->groups;
    }
}
