<?php

namespace PSC\Library\Calc\PreCalc;

use ArrayIterator;

class Group {

    protected string $name = "";

    protected ArrayIterator $variants;

    public function __construct()
    {
        $this->variants = new ArrayIterator();
    } 

    public function setName(string $name):void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function addVariants(Variant $calc): void
    {
        $this->variants->append($calc);
    }

    public function getVariants(): ArrayIterator
    {
        return $this->variants;
    }
}
