<?php

namespace PSC\Library\Calc\PreCalc\Parser;

use PSC\Library\Calc\PreCalc\Value as PSCValue;
use SimpleXMLElement;

class Value {

    protected SimpleXMLElement $node;
    
    public function __construct(SimpleXMLElement $node)
    {
        $this->node = $node;
    }

    public function parse(): PSCValue
    {
        $obj = new PSCValue();
        $obj->setKey($this->node->getName());
        $obj->setValue((string)$this->node);

        return $obj;

    }

}
