<?php

namespace PSC\Library\Calc\PreCalc\Parser;

use PSC\Library\Calc\PreCalc\PreCalc as PSCPreCalc;
use SimpleXMLElement;

class PreCalc {

    protected SimpleXMLElement $node;
    
    public function __construct(SimpleXMLElement $node)
    {
        $this->node = $node;
    }

    public function parse(): PSCPreCalc
    {
        $obj = new PSCPreCalc();

        foreach($this->node->children() as $child) {
            $parser = new Group($child);
            $obj->addGroup($parser->parse());
        }

        return $obj;

    }

}
