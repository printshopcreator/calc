<?php

namespace PSC\Library\Calc\PreCalc\Parser;

use PSC\Library\Calc\PreCalc\Group as PSCGroup;
use SimpleXMLElement;

class Group {

    protected SimpleXMLElement $node;
    
    public function __construct(SimpleXMLElement $node)
    {
        $this->node = $node;
    }

    public function parse(): PSCGroup
    {
        $obj = new PSCGroup();
        if(isset($this->node['name'])) {
            $obj->setName((string)$this->node['name']);
        }

        foreach($this->node->children() as $child) {
            $parser = new Variant($child);
            $obj->addVariants($parser->parse());
        }

        return $obj;

    }

}
