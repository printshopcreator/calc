<?php

namespace PSC\Library\Calc\PreCalc\Parser;

use PSC\Library\Calc\PreCalc\Variant as PSCVariant;
use SimpleXMLElement;

class Variant {

    protected SimpleXMLElement $node;
    
    public function __construct(SimpleXMLElement $node)
    {
        $this->node = $node;
    }

    public function parse(): PSCVariant
    {
        $obj = new PSCVariant();
        if(isset($this->node['name'])) {
            $obj->setName((string)$this->node['name']);
        }

        foreach($this->node->children() as $child) {
            $valueParser = new Value($child);
            $obj->addValue($valueParser->parse());  
        } 

        return $obj;

    }

}
