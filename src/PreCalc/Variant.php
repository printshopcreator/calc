<?php

namespace PSC\Library\Calc\PreCalc;

use ArrayIterator;

class Variant {

    protected string $name = "";

    protected float $price = 0;

    protected ArrayIterator $values;

    public function __construct()
    {
        $this->values = new ArrayIterator();
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function addValue(Value $value):void
    {
        $this->values->append($value);
    }

    public function getValues(): ArrayIterator
    {
        return $this->values;
    }
}
