<?php

namespace PSC\Library\Calc\PreCalc;

class Value {

    protected string $key = "";

    protected string $value = "";

    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    public function getKey(): string
    {
        return $this->key;
    }
    
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
