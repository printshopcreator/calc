<?php

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 16.04.18
 * Time: 14:00
 */

namespace PSC\Library\Calc\Calc;

use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\General\Type\Edge;
use PSC\Library\Calc\General\Type\EdgeCollection;
use PSC\Library\Calc\General\Type\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Type\Base;
use PSC\Library\Calc\Option\Type\Checkbox;
use PSC\Library\Calc\Option\Type\PaperDbSelect;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\Tests\Mock\Paper;
use SebastianBergmann\CodeCoverage\Report\PHP;

class CalcValues
{
    /** @var Engine */
    protected $engine = null;

    /** @var Article */
    protected $article = null;

    protected $storageFormulas = [];

    protected $storageCalcValues = [];

    protected $formelCalc = null;

    /**
     * Calc constructor.
     * @param Engine $engine
     * @param Article $article
     */
    public function __construct($engine, $article)
    {
        $this->engine = $engine;
        $this->article = $article;
        $this->formelCalc = new Formel($engine, $article);
    }

    public function calc()
    {
        $price = 0;

        /** @var Base $option */
        foreach ($this->article->getOptions() as $option) {

            if ($option instanceof Select || $option instanceof Checkbox) {
                /** @var Select\Opt $opt */
                foreach ($option->getOptions() as $opt) {
                    if ($opt->isValid() && $opt->isSelected()) {
                        $price = $this->parseEdgeCollection($price, $option->getId(), $opt->getEdgesCollectionContainer());
                    }
                }
            }

            $this->parseEdgeCollection($price, $option->getId(), $option->getEdgesCollectionContainer());
        }

        return $price;
    }

    /**
     * @param $price
     * @param id
     * @param EdgeCollectionContainer $container
     * @return int
     */
    private function parseEdgeCollection($price, $id, EdgeCollectionContainer $container, $isSub = false)
    {

        $calcValue1 = 0;
        $calcValue2 = 0;
        $calcValueAccount1 = 0;
        $calcValueAccount2 = 0;

        try{
            @eval($this->engine->getParameters());
            @eval($this->engine->getFormulas());
        }catch (\Throwable $e) {

        }

        /** @var EdgeCollection $collection */
        foreach ($container as $collection) {
            $var = 'XXXXXXXX';
            $hasVar = true;
            $hasValidEdge = false;

            if ($collection->getName() == "opt") continue;
            if ($collection->getFormel() != "") {
                $formel = $this->formelCalc->parse($collection->getFormel());
                if (preg_match("/^[a-z](.*)/", $formel)) {
                    eval('$var = "' . $formel . '";');
                } else {
                    eval('$var = ' . $formel . ';');
                }
            } else {
                if (isset($this->engine->getVariables()[$collection->getName()])) {
                    $var = $this->engine->getVariables()[$collection->getName()];
                }
            }
            if($collection->getDefault() != "" && $var === "XXXXXXXXXXXX") {
                $var = $collection->getDefault();
                $hasVar = false;
            }

            foreach ($collection as $edge) {
                if ($edge->isValid($var)) {
                    $hasValidEdge = true;
                }
            }

            if(!$hasValidEdge && $hasVar && $collection->getDefault()) {
                $var = $collection->getDefault();
            }

            /** @var Edge $edge */
            foreach ($collection as $edge) {

                if ($var !== 'XXXXXXXX' && $edge->isValid($var)) {

                    if ($edge->getCalcValue() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id, $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id, $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }
                        
                        $this->engine->addCalcVariable($id, $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName(), $cv);
                    }
                    if ($edge->getCalcValue1() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue1());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_1', $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_1', $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }
                        
                        $this->engine->addCalcVariable($id . '_1', $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName() . '_1', $cv);
                    }
                    if ($edge->getCalcValue2() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue2());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_2', $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_2', $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }
                        
                        $this->engine->addCalcVariable($id . '_2', $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName() . '_2', $cv);
                    }
                    if ($edge->getCalcValue3() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue3());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_3', $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_3', $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }

                        $this->engine->addCalcVariable($id . '_3', $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName() . '_3', $cv);
                    }
                    if ($edge->getCalcValue4() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue4());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_4', $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_4', $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }

                        $this->engine->addCalcVariable($id . '_4', $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName() . '_4', $cv);
                    }
                    if ($edge->getCalcValue5() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue5());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_5', $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_5', $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }

                        $this->engine->addCalcVariable($id . '_5', $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName() . '_5', $cv);
                    }
                    if ($edge->getCalcValue6() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue6());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_6', $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_6', $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }

                        $this->engine->addCalcVariable($id . '_6', $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName() . '_6', $cv);
                    }
                    if ($edge->getCalcValue7() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue7());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_7', $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_7', $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }

                        $this->engine->addCalcVariable($id . '_7', $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName() . '_7', $cv);
                    }
                    if ($edge->getCalcValue8() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue8());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_8', $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_8', $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }

                        $this->engine->addCalcVariable($id . '_8', $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName() . '_8', $cv);
                    }
                    if ($edge->getCalcValue9() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue9());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_9', $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_9', $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }

                        $this->engine->addCalcVariable($id . '_9', $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName() . '_9', $cv);
                    }
                    if ($edge->getCalcValue10() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue10());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_10', $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        }catch (\Throwable $e) {
                            $this->engine->addDebugCalcVariables($id . '_' . $collection->getName() . '_10', $edge->getCalcValue(), $orgCv . ' = ERROR');
                            $cv = 0;
                        }

                        $this->engine->addCalcVariable($id . '_10', $cv);

                        $this->engine->addCalcVariable($id . '_' . $collection->getName() . '_10', $cv);
                    }
                    if ($edge->getEdgesCollectionContainer()->count() > 0) {
                        $this->parseEdgeCollection($price,  $id . '_' . $collection->getName(), $edge->getEdgesCollectionContainer(), true);
                    }
                }
            }
        }

        return $price;
    }
}
