<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 13.04.18
 * Time: 22:38
 */

namespace PSC\Library\Calc\Calc;


class EvalCommand
{
    public function calc($formel) {
        $result = 0;

        eval('$result += ' . $formel . ';');

        return $result;
    }

    private function parseVariables($formel)
    {
        preg_match_all('/\$V\w*\$V/', $formel, $founds);

        $variables = $this->engine->getVariables();

        if (!empty($founds [0])) {
            foreach ($founds [0] as $key => $found) {
                $foundvalue = str_replace('$V', '', $found);
                if (isset($variables [$foundvalue])) {
                    if ($variables [$foundvalue] == 'null') {
                        $formel = str_replace($found, 0, $formel);
                    } else {
                        if ($foundvalue == 'auflage') {
                            $formel = str_replace($found, str_replace(',', '.', $variables [$foundvalue]), $formel);
                        } else {
                            if ($variables [$foundvalue] == '') {
                                $formel = str_replace($found, 0, $formel);
                            } else {
                                $formel = str_replace($found, $variables [$foundvalue], $formel);
                            }
                        }
                    }
                } else {
                    $formel = str_replace($found, 0, $formel);
                }
            }
        }

        return $formel;
    }

}