<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 16.04.18
 * Time: 14:00
 */

namespace PSC\Library\Calc\Calc;

use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\General\Type\Edge;
use PSC\Library\Calc\General\Type\EdgeCollection;
use PSC\Library\Calc\General\Type\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Type\Base;
use PSC\Library\Calc\Option\Type\Checkbox;
use PSC\Library\Calc\Option\Type\PaperDbSelect;
use PSC\Library\Calc\Option\Type\Radio;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\Tests\Mock\Paper;

class Calc
{
    /** @var Engine */
    protected $engine = null;

    /** @var Article */
    protected $article = null;

    protected $storageFormulas = [];

    protected $storageCalcValues = [];

    protected $formelCalc = null;

    /**
     * Calc constructor.
     * @param Engine $engine
     * @param Article $article
     */
    public function __construct($engine, $article)
    {
        $this->engine = $engine;
        $this->article = $article;
        $this->formelCalc = new Formel($engine, $article);
    }

    public function calc()
    {
        $gesamt = 0;

        /** @var Base $option */
        foreach ($this->article->getOptions() as $option) {

            if ($option instanceof Select || $option instanceof Checkbox || $option instanceof Radio) {
                /** @var Select\Opt $opt */
                foreach ($option->getOptions() as $opt) {
                    if ($opt->isValid() && $opt->isSelected()) {
                        $gesamt = $this->parseEdgeCollection($gesamt, $option, $opt->getEdgesCollectionContainer(), [$option->getId()]);
                    }
                }
            }

            $gesamt = $this->parseEdgeCollection($gesamt, $option, $option->getEdgesCollectionContainer(), [$option->getId()]);
        }

        return $gesamt;
    }

    /**
     * @param $gesamt
     * @param Base $option
     * @param EdgeCollectionContainer $container
     * @return int
     */
    private function parseEdgeCollection($gesamt, $option, EdgeCollectionContainer $container, $calcValueId = [])
    {

        $calcValue1 = 0;
        $calcValue2 = 0;
        $calcValueAccount1 = 0;
        $calcValueAccount2 = 0;

        /** @var EdgeCollection $collection */
        foreach ($container as $collection) {
            $var = 'XXXXXXXXXXXX';
            $hasVar = true;
            $hasValidEdge = false;

            if ($collection->getName() == "opt") {
                continue;
            }
            if ($collection->getFormel() != "") {
                $formel = $this->formelCalc->parse($collection->getFormel());
                eval('$var = ' . $formel . ';');
            } else {
                if (isset($this->engine->getVariables()[$collection->getName()])) {
                    $var = $this->engine->getVariables()[$collection->getName()];
                }
            }
            if($collection->getDefault() != "" && $var === "XXXXXXXXXXXX") {
                $var = $collection->getDefault();
                $hasVar = false;
            }

            foreach ($collection as $edge) {
                if ($edge->isValid($var)) {
                    $hasValidEdge = true;
                }
            }

            if(!$hasValidEdge && $hasVar && $collection->getDefault()) {
                $var = $collection->getDefault();
            }

            /** @var Edge $edge */
            foreach ($collection as $edge) {

                if ($var !== "XXXXXXXXXXXX" && $edge->isValid($var)) {

                    if ($edge->getPauschale() != 0) {
                        eval('$gesamt += ' . $edge->getPauschale() . ';');
                        $this->engine->addDebugFlatPrice($collection->getName(), $edge->getPauschale());
                    }

                    if ($edge->getPreis() != 0) {
                        eval('$gesamt += ' . ($edge->getPreis() * $var) . ';');
                        $this->engine->addDebugPrice($collection->getName(), $edge->getPreis(), $var, $edge->getPreis() * $var);
                    }

                    if ($edge->getCalcValue() != "") {
                        $cv = $this->formelCalc->parse($edge->getCalcValue());
                        $orgCv = $cv;
                        try {
                            eval('@$cv = ' . $cv . ';');
                            $this->engine->addDebugCalcVariables($option->getId() . '_' . $collection->getName(), $edge->getCalcValue(), $orgCv . ' = ' . $cv);
                        } catch (\Throwable $e) {
                            $cv = 0;
                        }
                        $this->engine->addCalcVariable($option->getId() . '_' . $collection->getName(), $orgCv);
                        $this->engine->setCalcVaribleStack($orgCv, $calcValueId);
                    }

                    if ($edge->getFormel() != "") {
                        $formel = $this->formelCalc->parse($edge->getFormel());
                        if ($formel != "" && !in_array($option->getId(), ['weight', 'weight_single']) && !$option->isAjaxExport() && !$option->isDisplayOnly() && $option->isAmount()) {
                            $p = 0;
                            $formel = str_replace("tonumber", '$this->toNumber', $formel);

                            try {
                                eval('@$p = ' . $this->eval_func($gesamt, $formel) . ';');
                                $this->engine->addDebugCalcFormel($edge->getFormel(), $formel . ' = ' .  $p);
                            } catch (\Throwable $e) {
                                $this->engine->addDebugCalcFormel($edge->getFormel(), $formel . ' = error');
                                $p = 0;
                            }
                            if ($p > 0 || $p < 0) {
                                $gesamt += $p;
                            }

                            $this->engine->setVariable('price', $gesamt);
                        }
                        if ($formel != "" && !in_array($option->getId(), ['weight', 'weight_single']) && $option->isAjaxExport() && !$option->isDisplayOnly() && $option->isAmount()) {
                            $p = 0;
                            $formel = str_replace("tonumber", '$this->toNumber', $formel);
                            try {
                                eval('@$p = ' . $this->eval_func($gesamt, $formel) . ';');

                                $this->engine->addAjaxVariable($option->getId(), $p);
                            } catch (\Throwable $e) {
                                $this->engine->addDebugCalcFormel($edge->getFormel(), $formel . ' = error');
                                $p = 0;
                            }
                        }
                        if ($formel != "" && !in_array($option->getId(), ['weight', 'weight_single']) && !$option->isAjaxExport() && $option->isDisplayOnly() && $option->isAmount()) {
                            $p = 0;
                            $formel = str_replace("tonumber", '$this->toNumber', $formel);
                            eval('@$p = ' . $this->eval_func($gesamt, $formel) . ';');

                            $this->engine->addDisplayVariable($option->getId(), $p);
                        }
                        if ($formel != "" && $option->getId() == "weight_single") {
                            $p = 0;
                            $formel = str_replace("tonumber", '$this->toNumber', $formel);
                            eval('@$p = ' . $this->eval_func($gesamt, $formel) . ';');
                            $this->engine->setWeightSingle($p);
                        }
                        if ($formel != "" && $option->getId() == "weight") {
                            $p = 0;
                            $formel = str_replace("tonumber", '$this->toNumber', $formel);
                            eval('@$p = ' . $this->eval_func($gesamt, $formel) . ';');
                            $this->engine->setWeight($p);
                        }
                        if (!$option->isAmount()) {
                            $p = 0;
                            $formel = str_replace("tonumber", '$this->toNumber', $formel);

                            eval('@$p = ' . $this->eval_func($gesamt, $formel) . ';');
                            $this->engine->addCalcVariable($option->getId(), $p);
                        }
                    }

                    if ($edge->getEdgesCollectionContainer()->count() > 0) {
                        $calcValueId[] = $collection->getName();
                        $gesamt = $this->parseEdgeCollection($gesamt, $option, $edge->getEdgesCollectionContainer(), $calcValueId);
                    }
                }
            }
        }

        return $gesamt;
    }

    private function eval_func($gesamt, string $formel)
    {
        $p = 0;
        try {
            eval('@$p=' . $formel . ';');
        } catch (\Throwable $e) {

            if (str_contains($e->getMessage(), 'Undefined constant')) {
                preg_match('/Undefined constant "(.*)"/', $e->getMessage(), $output_array);
                if (isset($output_array[1])) {
                    $formel = str_replace($output_array[1], "'" . $output_array[1] ."'", $formel);
                    $p = $this->eval_func($gesamt, $formel);
                    return $p;
                }
            }
        }

        return $p;
    }

    private function toNumber($value)
    {
        if (strpos($value, ',') == (strlen($value) - 1)) {
            return str_replace(',', '', $value);
        }

        return str_replace(',', '.', $value);

    }
}
