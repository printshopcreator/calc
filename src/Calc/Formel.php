<?php
namespace PSC\Library\Calc\Calc;

use PSC\Library\Calc\Article;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\General\Type\Edge;
use PSC\Library\Calc\General\Type\EdgeCollection;
use PSC\Library\Calc\General\Type\EdgeCollectionContainer;
use PSC\Library\Calc\Option\Type\Base;

class Formel
{

    /** @var Engine */
    protected $engine = null;

    /** @var Article */
    protected $article = null;

    /**
     * Calc constructor.
     * @param Engine $engine
     * @param Article $article
     */
    public function __construct($engine, $article)
    {
        $this->engine = $engine;
        $this->article = $article;
    }

    public function parse($formel) {

        $i = 0;

        while((strpos($formel,'$F') !== false || strpos($formel,'$P') !== false || strpos($formel,'$V') !== false || strpos($formel,'$CV') !== false) && $i < 6) {
            $formel = $this->parseCalcVariables($formel);
            $formel = $this->parseFormulas($formel);
            $formel = $this->parseParameters($formel);
            $formel = $this->parseVariables($formel);

            $i++;
        }

        return $formel;

    }

    private function parseVariables($formel)
    {
        preg_match_all('/\$V\w*\$V/', $formel, $founds);

        $variables = $this->engine->getVariables();

        if (!empty($founds [0])) {
            foreach ($founds [0] as $key => $found) {
                $foundvalue = str_replace('$V', '', $found);
                if (isset($variables [$foundvalue])) {
                    if ($variables [$foundvalue] == 'null') {
                        $formel = str_replace($found, 0, $formel);
                    } else {
                        if ($foundvalue == 'auflage') {
                            $formel = str_replace($found, str_replace(',', '.', $variables [$foundvalue]), $formel);
                        } else {
                            if ($variables [$foundvalue] == '') {
                                $formel = str_replace($found, 0, $formel);
                            } else {
                                $formel = str_replace($found, $variables [$foundvalue], $formel);
                            }
                        }
                    }
                } else {
                    $formel = str_replace($found, 0, $formel);
                }
            }
        }

        return $formel;
    }

    private function parseParameters($formel)
    {
        preg_match_all('/\$P\w*\$P/', $formel, $founds);

        if (!empty($founds [0])) {
            try{
                @eval($this->engine->getParameters());
                @eval($this->engine->getFormulas());
            }catch (\Throwable $e) {

            }
            foreach ($founds [0] as $key => $found) {
                $foundvalue = str_replace('$P', '', $found);
                if (isset($$foundvalue)) {
                    $formel = str_replace($found, $$foundvalue, $formel);
                } else {
                    $formel = str_replace($found, 0, $formel);
                }
            }
        }

        return $formel;
    }

    private function parseFormulas($formel)
    {
        preg_match_all('/\$F\w*\$F/', $formel, $founds);

        if (!empty($founds [0])) {
            try{
                @eval($this->engine->getParameters());
                @eval($this->engine->getFormulas());
            }catch (\Throwable $e) {

            }

            foreach ($founds [0] as $key => $found) {
                $foundvalue = str_replace('$F', '', $found);
                if(isset($$foundvalue)) {
                    $formel = str_replace($found, $$foundvalue, $formel);
                }else{
                    $formel = str_replace($found, 0, $formel);
                }
            }
        }

        return $formel;
    }

    private function parseCalcVariables($formel)
    {
        preg_match_all('/\$CV[\w\.]*\$CV/', $formel, $founds);
        $variables = $this->engine->getCalcVariables();

        if (!empty($founds [0])) {
            foreach ($founds [0] as $key => $found) {
                $foundvalue = str_replace('$CV', '', $found);
                if (isset($variables [$foundvalue])) {
                    if ($variables [$foundvalue] == 'null') {
                        $formel = str_replace($found, 0, $formel);
                    } else {
                        if ($foundvalue == 'auflage') {
                            $formel = str_replace($found, str_replace(',', '.', $variables [$foundvalue]), $formel);
                        } else {
                            if ($variables [$foundvalue] == '') {
                                $formel = str_replace($found, 0, $formel);
                            } else {
                                if(is_array($variables [$foundvalue])) {
                                    $formel = str_replace($found, 0, $formel);
                                }else{
                                    $formel = str_replace($found, $variables [$foundvalue], $formel);
                                }
                            }
                        }
                    }
                } else {
                    $formel = str_replace($found, 0, $formel);
                }
            }
        }

        return $formel;
    }
}