<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 09.04.18
 * Time: 15:58
 */

namespace PSC\Library\Calc\General\Parser;

class EdgeCollectionContainer
{
    /** @var  \SimpleXMLElement $node */
    protected $node;

    public function __construct($node)
    {
        $this->node = $node;
    }

    public function parse()
    {
        $container = new \PSC\Library\Calc\General\Type\EdgeCollectionContainer();

        foreach($this->node->children() as $key => $row) {
            $collectionParser = new EdgeCollection($row);

            $collection = $collectionParser->parse();
            $collection->setName($key);

            $container->append($collection);
        }

        return $container;
    }
}