<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 09.04.18
 * Time: 15:58
 */

namespace PSC\Library\Calc\General\Parser;


class EdgeCollection
{
    /** @var  \SimpleXMLElement $node */
    protected $node;

    public function __construct($node)
    {
        $this->node = $node;
    }

    /**
     *
     * @return \PSC\Library\Calc\General\Type\EdgeCollection
     */
    public function parse()
    {
        $collection = new \PSC\Library\Calc\General\Type\EdgeCollection();

        if(isset($this->node->attributes()->formel)) {
            $collection->setFormel((string)$this->node->attributes()->formel);
        }

        if(isset($this->node->attributes()->default)) {
            $collection->setDefault((string)$this->node->attributes()->default);
        }

        foreach($this->node->grenze as $row) {
            $edgeParser = new Edge($row);
            $edge = $edgeParser->parse();

            $collection->append($edge);
        }

        return $collection;
    }
}