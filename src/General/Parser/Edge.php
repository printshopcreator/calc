<?php

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 09.04.18
 * Time: 15:58
 */

namespace PSC\Library\Calc\General\Parser;


class Edge
{
    /** @var  \SimpleXMLElement $node */
    protected $node;

    public function __construct($node)
    {
        $this->node = $node;
    }

    /**
     * @return \PSC\Library\Calc\General\Type\Edge
     */
    public function parse()
    {
        $edge = new \PSC\Library\Calc\General\Type\Edge();
        if (isset($this->node->attributes()->formel)) {
            $edge->setFormel((string) $this->node->attributes()->formel);
        }
        if (isset($this->node->attributes()->calc_value)) {
            $edge->setCalcValue((string) $this->node->attributes()->calc_value);
        }
        if (isset($this->node->attributes()->calc_value_1)) {
            $edge->setCalcValue1((string) $this->node->attributes()->calc_value_1);
        }
        if (isset($this->node->attributes()->calc_value_2)) {
            $edge->setCalcValue2((string) $this->node->attributes()->calc_value_2);
        }
        if (isset($this->node->attributes()->calc_value_3)) {
            $edge->setCalcValue3((string) $this->node->attributes()->calc_value_3);
        }
        if (isset($this->node->attributes()->calc_value_4)) {
            $edge->setCalcValue4((string) $this->node->attributes()->calc_value_4);
        }
        if (isset($this->node->attributes()->calc_value_5)) {
            $edge->setCalcValue5((string) $this->node->attributes()->calc_value_5);
        }
        if (isset($this->node->attributes()->calc_value_6)) {
            $edge->setCalcValue6((string) $this->node->attributes()->calc_value_6);
        }
        if (isset($this->node->attributes()->calc_value_7)) {
            $edge->setCalcValue7((string) $this->node->attributes()->calc_value_7);
        }
        if (isset($this->node->attributes()->calc_value_8)) {
            $edge->setCalcValue8((string) $this->node->attributes()->calc_value_8);
        }
        if (isset($this->node->attributes()->calc_value_9)) {
            $edge->setCalcValue9((string) $this->node->attributes()->calc_value_9);
        }
        if (isset($this->node->attributes()->calc_value_10)) {
            $edge->setCalcValue10((string) $this->node->attributes()->calc_value_10);
        }
        if (isset($this->node->attributes()->preis)) {
            $edge->setPreis(floatval($this->node->attributes()->preis));
        }
        if (isset($this->node->attributes()->pauschale)) {
            $edge->setPauschale(floatval($this->node->attributes()->pauschale));
        }

        if (isset($this->node->attributes()->value) && $this->node->children()) {
            $this->parseCondition($edge, trim((string) $this->node->attributes()->value));
            if ($this->node->children()) {
                $edgeCollectionContainerParser = new EdgeCollectionContainer($this->node);
                $edge->setEdgesCollectionContainer($edgeCollectionContainerParser->parse());
            }
        } else {
            $this->parseCondition($edge, trim((string) $this->node));
        }

        return $edge;
    }

    private function parseCondition(\PSC\Library\Calc\General\Type\Edge $edge, string $condition): void
    {
        if (preg_match("/^([0-9a-zA-Z_]+)$/", trim($condition), $regs)) {
            $edge->setValues([$regs[1]]);
        } elseif (preg_match("/^([0-9]+)-([0-9]+)$/", trim($condition), $regs)) {
            $edge->setRegion(true);
            $edge->setFrom(intval($regs[1]));
            $edge->setTo(intval($regs[2]));
        } elseif (preg_match("/^([0-9]+)-$/", trim($condition), $regs)) {
            $edge->setRegion(true);
            $edge->setFrom(intval($regs[1]));
        } elseif (strpos(trim($condition), ",") !== false) {
            $values = explode(",", trim($condition));
            $edge->setValues($values);
        }
    }
}
