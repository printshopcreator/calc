<?php

namespace PSC\Library\Calc\General\Type;

class EdgeCollectionContainer extends \ArrayIterator
{
    public function getCollectionByName($string)
    {
        while($this->valid()) {
            if($string == $this->current()->getName()) {
                return $this->current();
            }

            $this->next();
        }

        return null;
    }
}