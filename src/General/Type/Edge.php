<?php

namespace PSC\Library\Calc\General\Type;

class Edge
{
    protected $formel = '';
    protected $pauschale = 0;
    protected $preis = 0;
    protected $calcValue = '';

    protected $calcValue1 = '';
    protected $calcValue2 = '';
    protected $calcValue3 = '';
    protected $calcValue4 = '';
    protected $calcValue5 = '';
    protected $calcValue6 = '';
    protected $calcValue7 = '';
    protected $calcValue8 = '';
    protected $calcValue9 = '';
    protected $calcValue10 = '';

    protected $from = 0;
    protected $to = 0;
    protected $values = [];

    protected $region = false;

    /** @var EdgeCollectionContainer */
    protected $edgesCollectionContainer = null;

    public function __construct()
    {
        $this->edgesCollectionContainer = new EdgeCollectionContainer();
    }

    /**
     * @return string
     */
    public function getFormel()
    {
        return $this->formel;
    }

    /**
     * @param string $formel
     */
    public function setFormel($formel)
    {
        $this->formel = $formel;
    }

    /**
     * @return int
     */
    public function getPauschale()
    {
        return $this->pauschale;
    }

    /**
     * @param int $pauschale
     */
    public function setPauschale($pauschale)
    {
        $this->pauschale = $pauschale;
    }

    /**
     * @return int
     */
    public function getPreis()
    {
        return $this->preis;
    }

    /**
     * @param int $preis
     */
    public function setPreis($preis)
    {
        $this->preis = $preis;
    }

    /**
     * @return string
     */
    public function getCalcValue()
    {
        return $this->calcValue;
    }

    /**
     * @param string $calcValue
     */
    public function setCalcValue($calcValue)
    {
        $this->calcValue = $calcValue;
    }

    /**
     * @return string
     */
    public function getCalcValue1()
    {
        return $this->calcValue1;
    }

    /**
     * @param string $calcValue1
     */
    public function setCalcValue1($calcValue1)
    {
        $this->calcValue1 = $calcValue1;
    }

    /**
     * @return string
     */
    public function getCalcValue2()
    {
        return $this->calcValue2;
    }

    /**
     * @param string $calcValue2
     */
    public function setCalcValue2($calcValue2)
    {
        $this->calcValue2 = $calcValue2;
    }

    /**
     * @return string
     */
    public function getCalcValue3()
    {
        return $this->calcValue3;
    }

    /**
     * @param string $calcValue3
     */
    public function setCalcValue3($calcValue3)
    {
        $this->calcValue3 = $calcValue3;
    }

    /**
     * @return string
     */
    public function getCalcValue4()
    {
        return $this->calcValue4;
    }

    /**
     * @param string $calcValue4
     */
    public function setCalcValue4($calcValue4)
    {
        $this->calcValue4 = $calcValue4;
    }

    /**
     * @return string
     */
    public function getCalcValue5()
    {
        return $this->calcValue5;
    }

    /**
     * @param string $calcValue5
     */
    public function setCalcValue5($calcValue5)
    {
        $this->calcValue5 = $calcValue5;
    }

    /**
     * @return string
     */
    public function getCalcValue6()
    {
        return $this->calcValue6;
    }

    /**
     * @param string $calcValue6
     */
    public function setCalcValue6($calcValue6)
    {
        $this->calcValue6 = $calcValue6;
    }

    /**
     * @return string
     */
    public function getCalcValue7()
    {
        return $this->calcValue7;
    }

    /**
     * @param string $calcValue7
     */
    public function setCalcValue7($calcValue7)
    {
        $this->calcValue7 = $calcValue7;
    }

    /**
     * @return string
     */
    public function getCalcValue8()
    {
        return $this->calcValue8;
    }

    /**
     * @param string $calcValue8
     */
    public function setCalcValue8($calcValue8)
    {
        $this->calcValue8 = $calcValue8;
    }

    /**
     * @return string
     */
    public function getCalcValue9()
    {
        return $this->calcValue9;
    }

    /**
     * @param string $calcValue9
     */
    public function setCalcValue9($calcValue9)
    {
        $this->calcValue9 = $calcValue9;
    }

    /**
     * @return string
     */
    public function getCalcValue10()
    {
        return $this->calcValue10;
    }

    /**
     * @param string $calcValue10
     */
    public function setCalcValue10($calcValue10)
    {
        $this->calcValue10 = $calcValue10;
    }

    /**
     * @return int
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param int $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return int
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param int $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @return bool
     */
    public function isRegion()
    {
        return $this->region;
    }

    /**
     * @param bool $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }


    /**
     * @param $value
     * @return bool
     */
    public function isValid($value)
    {

        if (
            $this->isRegion() &&
            $this->getFrom() <= $value &&
            $this->getTo() >= $value
        ) {
            return true;
        }

        if (
            $this->isRegion() &&
            $this->getFrom() <= $value &&
            $this->getTo() == 0
        ) {
            return true;
        }

        if (!$this->isRegion() && in_array($value, $this->getValues())) {
            return true;
        }

        $values = $this->getValues();

        if (is_array($values)) {
            foreach ($values as $val) {
                if (is_numeric($value) && (
                    (preg_match("/^([0-9]+)-([0-9]+)$/", trim($val), $regs) && ($value >= $regs[1] && $value <= $regs[2])) ||
                    (preg_match("/^([0-9]+)-$/", trim($val), $regs) && $value >= $regs[1]))) {
                    return true;
                }
                if(is_array($value) && in_array($val, $value)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return EdgeCollectionContainer
     */
    public function getEdgesCollectionContainer()
    {
        return $this->edgesCollectionContainer;
    }

    /**
     * @param EdgeCollectionContainer $edgesCollectionContainer
     */
    public function setEdgesCollectionContainer($edgesCollectionContainer)
    {
        $this->edgesCollectionContainer = $edgesCollectionContainer;
    }
}

