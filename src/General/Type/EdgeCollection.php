<?php

namespace PSC\Library\Calc\General\Type;

class EdgeCollection extends \ArrayIterator
{

    protected string $name = '';

    protected string $formel = '';

    protected string $default = '';

    public function getName(): string
    {
        return (string)$this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getFormel(): string
    {
        return (string)$this->formel;
    }

    public function setFormel(string $formel): void
    {
        $this->formel = $formel;
    }

    public function getDefault(): string
    {
        return (string)$this->default;
    }

    public function setDefault(string $default): void
    {
        $this->default = $default;
    }


}